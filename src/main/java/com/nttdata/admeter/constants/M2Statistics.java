package com.nttdata.admeter.constants;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Statistics;

public class M2Statistics {

	/**
	 * Number of buffers written to output file.
	 */
	public static final String BUFFER_WRITE_ADSMART = "bufferWriteProcessed.adsmart";
	public static final String BUFFER_WRITE_ETHAN = "bufferWriteProcessed.ethan";
	public static final String BUFFER_READ_ADSMART = "bufferReadProcessed.adsmart";
	public static final String BUFFER_READ_ETHAN = "bufferReadProcessed.ethan";

	/**
	 * Number of messages from input correctly processed.
	 */
	public static final String MESSAGES_PROCESSED_ADSMART = "msgProcessed.adsmart";
	public static final String MESSAGES_PROCESSED_ETHAN = "msgProcessed.ethan";

	/**
	 * Number of messages from input discarded for any reasons.
	 */
	public static final String MESSAGES_DISCARDED_ADSMART = "msgDiscarded.adsmart";
	public static final String MESSAGES_DISCARDED_ETHAN = "msgDiscarded.ethan";

	/**
	 * Total number of events processed.
	 */
	public static final String EVENTS_PROCESSED_ADSMART = "totalEventProcessed.adsmart";
	public static final String EVENTS_PROCESSED_ETHAN = "totalEventProcessed.ethan";

	/**
	 * Total number of events discarded.
	 */
	public static final String EVENTS_DISCARDED_ADSMART = "totalEventDiscarded.adsmart";
	public static final String EVENTS_DISCARDED_ETHAN = "totalEventDiscarded.ethan";

	/**
	 * Number of events with unknown speed
	 */
	public static final String UNKNOWN_SPEED_ADSMART = "unknownSpeed.adsmart";
	public static final String UNKNOWN_SPEED_ETHAN = "unknownSpeed.ethan";

	/**
	 * Number of events with unknown channel
	 */
	public static final String UNKNOWN_CHANNEL_ADSMART = "unknownChannel.adsmart";
	public static final String UNKNOWN_CHANNEL_ETHAN = "unknownChannel.ethan";

	/**
	 * Number of events with unknown campaign
	 */
	public static final String UNKNOWN_CAMPAIGN_ADSMART = "unknownCampaign.adsmart";
	public static final String UNKNOWN_CAMPAIGN_ETHAN = "unknownCampaign.ethan";

	/**
	 * Total number of generic program errors.
	 */
	public static final String GENERIC_ERRORS_ADSMART = Statistics.GENERIC_ERRORS + ErrorCode.Source.ADSMART;
	public static final String GENERIC_ERRORS_ETHAN = Statistics.GENERIC_ERRORS + ErrorCode.Source.ETHAN;
	public static final String GENERIC_ERRORS_COMMON = Statistics.GENERIC_ERRORS + ErrorCode.Source.COMMON;

	/**
	 * minim processing time
	 */
	public static final String MIN_DELAY_PROCESSING_MILLIS_ADSMART = "minDelayProcessingMillis.adsmart";
	public static final String MIN_DELAY_PROCESSING_MILLIS_ETHAN = "minDelayProcessingMillis.ethan";

	/**
	 * max processing time
	 */
	public static final String MAX_DELAY_PROCESSING_MILLIS_ADSMART = "maxDelayProcessingMillis.adsmart";
	public static final String MAX_DELAY_PROCESSING_MILLIS_ETHAN = "maxDelayProcessingMillis.ethan";

}
