package com.nttdata.admeter.config.check;

public class ConfigKeys {
	static final public String YES = "Y";
	static final public String NO = "N";
	
	public static final String ADSMART = "ADSMART";
	public static final String ETHAN = "ETHAN";

	public static class FIXED {
		public static final String SECTION = "$.fixed";

		public static final String INSTANCE_NAME = "$.fixed.instanceName";
		public static final String MODULE_NAME = "$.fixedModule.moduleName";
		public static final String MODULE_VERSION = "$.fixedModule.moduleVersion";

		public static class COMMON_CFG {
			public static final String OBJECT = "$.fixed.commonCfg";

			public static final String RUN_FILE_DIR = OBJECT + ".runFileDir";

			public static final String STAT_FILE_DIR = OBJECT + ".statFileDir";
			public static final String STAT_FILE_NAME_TS_FORMAT = OBJECT + ".statFileNameTsFormat";

			public static final String ERROR_FILE_DIR = OBJECT + ".errorFileDir";
			public static final String ERROR_FILE_NAME_TS_FORMAT = OBJECT + ".errorFileNameTsFormat";

			public static final String ERROR_DICTIONARY_FILE_DIR = OBJECT + ".errorDictionaryFileDir";
			public static final String ERROR_DICTIONARY_FILE_NAME = OBJECT + ".errorDictionaryFileName";

			public static class DATABASE_IP {
				public static final String ARRAY = "$.fixed.commonCfg.databaseIp";
			}

			public static final String COUNT_FILE_SYSTEM_FAIL = OBJECT + ".countFileSystemFail";
			public static final String COUNT_OFFSET_FAIL = OBJECT + ".countOffsetFail";
			
            public static final String DATABASE_READ_RETRY_NUM  = OBJECT + "databaseReadRetryNum";
            public static final String DATABASE_READ_WAIT_MSEC  = OBJECT + "databaseReadWaitMsec";
            public static final String DATABASE_WRITE_RETRY_NUM = OBJECT + "databaseWriteRetryNum";
            public static final String DATABASE_WRITE_WAIT_MSEC = OBJECT + "databaseWriteWaitMsec";
		}

		public static class ADSMART_CFG {
			public static final String OBJECT = "$.fixed.adsmartCfg";

			public static final String PARALLEL_THREADS_NUM = OBJECT + ".parallelThreadsNum";
			public static final String TIMESTAMP_HEADER_TYPE = OBJECT + ".timestampHeaderType";
			public static final String HEADER_IDENTIFIER = OBJECT + ".headerIdentifier";

			public static final String INPUT_FILES_DIR = OBJECT + ".inputFilesDir";

			public static class INPUT_FILES_PATTERN {
				public static final String ARRAY = OBJECT + ".inputFilesPattern";
			}

			public static final String OUTPUT_EVENTS_FILE_DIR = OBJECT + ".outputEventsFileDir";
			public static final String OUTPUT_EVENTS_FILE_NAME = OBJECT + ".outputEventsFileName";
			public static final String OUTPUT_FILE_NAME_TS_FORMAT = OBJECT + ".outputFileNameTsFormat";

			public static final String OVERRIDE_CHANNEL_FILE_DIR = OBJECT + ".overrideChannelFileDir";
			public static final String OVERRIDE_CHANNEL_FILE_NAME = OBJECT + ".overrideChannelFileName";

			public static final String OVERRIDE_CAMPAIGN_FILE_DIR = OBJECT + ".overrideCampaignFileDir";
			public static final String OVERRIDE_CAMPAIGN_FILE_NAME = OBJECT + ".overrideCampaignFileName";

			public static final String TBL_SPEED_FILE_DIR = OBJECT + ".tblSpeedFileDir";
			public static final String TBL_SPEED_FILE_NAME = OBJECT + ".tblSpeedFileName";

			public static final String XML_SCHEMA_FILE_DIR = OBJECT + ".xmlSchemaFileDir";
			public static final String XML_SCHEMA_FILE_NAME = OBJECT + ".xmlSchemaFileName";

			public static final String OFS_FILE_DIR = OBJECT + ".ofsFileDir";
			public static final String OFS_FILE_NAME = OBJECT + ".ofsFileName";

			public static class DB_TABLE_CAMPAIGN_LIST {
				public static final String OBJECT = "$.fixed.adsmartCfg.dbTableCampaignList";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CAMPAIGNS_VIEW = OBJECT + ".allCampaignsView";
				public static final String ALL_CAMPAIGNS_VIEW_DD = OBJECT + ".allCampaignsViewDD";
			}

			public static class DB_TABLE_CAMPAIGN_HISTORY {
				public static final String OBJECT = "$.fixed.adsmartCfg.dbTableCampaignHistory";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CAMPAIGNS_HISTORY_VIEW = OBJECT + ".allCampaignsHistoryView";
				public static final String ALL_CAMPAIGNS_HISTORY_VIEW_DD = OBJECT + ".allCampaignsHistoryViewDD";
			}

			public static class DB_TABLE_CHANNEL_LIST {
				public static final String OBJECT = "$.fixed.adsmartCfg.dbTableChannelList";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CHANNELS_VIEW = OBJECT + ".allChannelsView";
				public static final String ALL_CHANNELS_VIEW_DD = OBJECT + ".allChannelsViewDD";
			}

			public static class DB_TABLE_STATE_MACHINE {
				public static final String OBJECT = "$.fixed.adsmartCfg.dbTableStateMachine";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
			}
		}

		public static class ETHAN_CFG {
			public static final String OBJECT = "$.fixed.ethanCfg";

			public static final String PARALLEL_THREADS_NUM = OBJECT + ".parallelThreadsNum";
			public static final String TIMESTAMP_HEADER_TYPE = OBJECT + ".timestampHeaderType";
			public static final String HEADER_IDENTIFIER = OBJECT + ".headerIdentifier";

			public static final String INPUT_FILES_DIR = OBJECT + ".inputFilesDir";

			public static class INPUT_FILES_PATTERN {
				public static final String ARRAY = OBJECT + ".inputFilesPattern";
			}

			public static final String OUTPUT_EVENTS_FILE_DIR = OBJECT + ".outputEventsFileDir";
			public static final String OUTPUT_EVENTS_FILE_NAME = OBJECT + ".outputEventsFileName";
			public static final String OUTPUT_FILE_NAME_TS_FORMAT = OBJECT + ".outputFileNameTsFormat";

			public static final String OVERRIDE_CHANNEL_FILE_DIR = OBJECT + ".overrideChannelFileDir";
			public static final String OVERRIDE_CHANNEL_FILE_NAME = OBJECT + ".overrideChannelFileName";

			public static final String OVERRIDE_CAMPAIGN_FILE_DIR = OBJECT + ".overrideCampaignFileDir";
			public static final String OVERRIDE_CAMPAIGN_FILE_NAME = OBJECT + ".overrideCampaignFileName";

			public static final String TBL_SPEED_FILE_DIR = OBJECT + ".tblSpeedFileDir";
			public static final String TBL_SPEED_FILE_NAME = OBJECT + ".tblSpeedFileName";

			public static final String XML_SCHEMA_FILE_DIR = OBJECT + ".xmlSchemaFileDir";
			public static final String XML_SCHEMA_FILE_NAME = OBJECT + ".xmlSchemaFileName";

			public static final String OFS_FILE_DIR = OBJECT + ".ofsFileDir";
			public static final String OFS_FILE_NAME = OBJECT + ".ofsFileName";

			public static class DB_TABLE_CAMPAIGN_LIST {
				public static final String OBJECT = "$.fixed.ethanCfg.dbTableCampaignList";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CAMPAIGNS_VIEW = OBJECT + ".allCampaignsView";
				public static final String ALL_CAMPAIGNS_VIEW_DD = OBJECT + ".allCampaignsViewDD";
			}

			public static class DB_TABLE_CAMPAIGN_HISTORY {
				public static final String OBJECT = "$.fixed.ethanCfg.dbTableCampaignHistory";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CAMPAIGNS_HISTORY_VIEW = OBJECT + ".allCampaignsHistoryView";
				public static final String ALL_CAMPAIGNS_HISTORY_VIEW_DD = OBJECT + ".allCampaignsHistoryViewDD";
			}

			public static class DB_TABLE_CHANNEL_LIST {
				public static final String OBJECT = "$.fixed.ethanCfg.dbTableChannelList";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
				public static final String ALL_CHANNELS_VIEW = OBJECT + ".allChannelsView";
				public static final String ALL_CHANNELS_VIEW_DD = OBJECT + ".allChannelsViewDD";
			}

			public static class DB_TABLE_STATE_MACHINE {
				public static final String OBJECT = "$.fixed.ethanCfg.dbTableStateMachine";

				public static final String BUCKET_NAME = OBJECT + ".bucketName";
				public static final String BUCKET_PASS = OBJECT + ".bucketPass";
			}
		}
	}

	public static class RUNTIME {
		public static final String SECTION = "$.runtime";

		public static final String STATUS_RECOVERY = SECTION + ".statusRecovery";

		public static final String MONITOR_STAT_INTERVAL_SEC = SECTION + ".monitorStatIntervalSec";
		public static final String MONITOR_RUN_INTERVAL_SEC = SECTION + ".monitorRunIntervalSec";

		public static final String OFS_READ_CYCLE_INTERVAL_MSEC = SECTION + ".ofsReadCycleIntervalMsec";

		public static final String TBL_CAMPAIGN_FREQ_READ_MIN = SECTION + ".tblCampaignFreqReadMin";
		public static final String TBL_CAMPAIGN_RETRY_NUM = SECTION + ".tblCampaignRetryNum";
		public static final String TBL_CAMPAIGN_HISTORY_READ_SINCE_DAY = SECTION + ".tblCampaignHistoryReadSinceDay";
		public static final String TBL_CAMPAIGN_LOG = SECTION + ".tblCampaignLog";

		public static class TBL_CHANNEL_TIME_READ {
			public static final String ARRAY = "$.runtime.tblChannelTimeRead";
		}

		public static final String TBL_CHANNEL_READ_SINCE_DAY = SECTION + ".tblChannelReadSinceDay";
		public static final String TBL_CHANNEL_RETRY_NUM = SECTION + ".tblChannelRetryNum";
		public static final String TBL_CHANNEL_LOG = SECTION + ".tblChannelLog";

		public static class BUFFER_WRITE_CFG {
			public static final String OBJECT = "$.runtime.bufferWriteCfg";

			public static final String BUFFER_SIZE_MB = OBJECT + ".ioBufferSizeMb";
			public static final String BUFFER_TIME_SEC = OBJECT + ".ioBufferTimeMsec";
		}

	}
}
