package com.nttdata.admeter.config.check;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.utils.FileUtils;

public class ConfigParserCheck extends ConfigCheck {

	private static JSONArray defaultDbIp = new JSONArray()
			.put("localhost");

	/**
	 * Checks the validity of the parameters in the "fixed" section. If a parameter is not found or it has a value not valid, it is replaced (when possible)..
	 * If not possible, returns the error.
	 * @param config the module's configuration object.
	 * @param logger the Log4j2 logger.
	 * @param programName the program name.
	 * @param counter the container of the correct, warning, and error parameters.
	 */
	public static void checkParametersFixed(IConfig config, Logger logger, String programName, ConfigurationCounters counter) {

		counter.correct++;
		checkStringDefault(config, logger, ConfigKeys.FIXED.INSTANCE_NAME, "M2_" + System.currentTimeMillis(), counter);

		// START commonConf object
		JSONObject tempCommonConf = config.getJSONObject(ConfigKeys.FIXED.COMMON_CFG.OBJECT);
		if (tempCommonConf == null) {
			logger.warn("Parameter " + ConfigKeys.FIXED.COMMON_CFG.OBJECT + " not found in configuration. Execution will not work.");
			counter.error += 10;
		} else {
			counter.correct += 10;

			checkStringDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR, defaultDirStatus, counter);
			checkDirExist(config, ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR, logger);

			checkStringDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR, defaultDirStatistics, counter);
			checkDirExist(config, ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR, logger);

			checkStringDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_DIR, defaultDirError, counter);
			checkDirExist(config, ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_DIR, logger);

			checkStringDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_NAME_TS_FORMAT, defaultTimestampFilePrefix, counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_NAME_TS_FORMAT, defaultTimestampFilePrefix, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR, counter);
			checkDirExist(config, ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR, logger);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_NAME, counter);
			checkDirExist(config, ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR, logger);
			String fileName = config.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR);
			fileName += config.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_NAME);
			FileUtils.checkFileMandatory(fileName, logger, counter);

			JSONArray databaseIp = config.getArray(ConfigKeys.FIXED.COMMON_CFG.DATABASE_IP.ARRAY);
			if (databaseIp == null || databaseIp.length() == 0) {
				logger.error("Parameter " + ConfigKeys.FIXED.COMMON_CFG.DATABASE_IP.ARRAY
						+ " not found in configuration. Program will not be able to start.");
				counter.correct--;
				counter.error++;
			}

			checkIntDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.COUNT_FILE_SYSTEM_FAIL, 50, 1, counter);

			checkIntDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.COUNT_OFFSET_FAIL, 50, 1, counter);
			
			checkLongDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_RETRY_NUM, 3, 1, counter);
			checkLongDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_WAIT_MSEC, 30000, 1, counter);
			checkLongDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_RETRY_NUM, 3, 1, counter);
			checkLongDefault(config, logger, ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_WAIT_MSEC, 30000, 1, counter);

		}
		// END commonConf object

		List<String> expectedStrings = new ArrayList<>();
		// START adsmartCfg object
		JSONObject tempadSmartConf = config.getJSONObject(ConfigKeys.FIXED.ADSMART_CFG.OBJECT);
		if (tempadSmartConf == null) {
			logger.warn("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.OBJECT + " not found in configuration. Execution will not work.");
			counter.error += 32;
		} else {
			counter.correct += 32;

			checkIntDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.PARALLEL_THREADS_NUM, 2, 1, counter);
			
			expectedStrings.add("MSG");
			expectedStrings.add("WR");
			checkExpectedString(config, logger, ConfigKeys.FIXED.ADSMART_CFG.TIMESTAMP_HEADER_TYPE, "MSG", counter, expectedStrings);
			
			expectedStrings.clear();
			expectedStrings.add("SUBSCRIBER");
			expectedStrings.add("SMARTCARD");
			expectedStrings.add("DEVICEID");
			checkExpectedString(config, logger, ConfigKeys.FIXED.ADSMART_CFG.HEADER_IDENTIFIER, "SUBSCRIBER", counter, expectedStrings);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_DIR, counter);
			FileUtils.checkDirectoryMandatory(config.getString(ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_DIR), logger, counter);

			JSONArray inputFilePattern = config.getArray(ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_PATTERN.ARRAY);
			if (inputFilePattern == null || inputFilePattern.length() == 0) {
				logger.error("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_PATTERN.ARRAY
						+ " not found in configuration. Program will not be able to start.");
				counter.correct--;
				counter.error++;
			}

			String defaultOutDir = defaultDirOutput + programName + "-" + config.getString(ConfigKeys.FIXED.INSTANCE_NAME)
					+ File.separatorChar;
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_EVENTS_FILE_DIR, defaultOutDir, counter);
			checkDirExist(config, ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_EVENTS_FILE_DIR, logger);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_EVENTS_FILE_NAME, "ADSMART", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_FILE_NAME_TS_FORMAT, defaultTimestampFilePrefix, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_NAME, counter);
			String fileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_DIR);
			fileName += config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_NAME);
			FileUtils.checkFileMandatory(fileName, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME, counter);
			String fileName2 = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_DIR);
			fileName2 += config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
			FileUtils.checkFileMandatory(fileName2, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_NAME, counter);
			String fileName3 = config.getString(ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_DIR);
			fileName3 += config.getString(ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_NAME);
			FileUtils.checkFileMandatory(fileName3, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_NAME, counter);
			String fileName4 = config.getString(ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_DIR);
			fileName4 += config.getString(ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_NAME);
			FileUtils.checkFileMandatory(fileName4, logger, counter);

			String defaultOfsDir = defaultDirOffset + File.separatorChar + programName + "-" + config.getString(ConfigKeys.FIXED.INSTANCE_NAME)
					+ File.separatorChar;
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OFS_FILE_NAME, defaultDirOffset, counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.OFS_FILE_DIR, defaultOfsDir, counter);
			checkDirExist(config, ConfigKeys.FIXED.ADSMART_CFG.OFS_FILE_DIR, logger);

			// couchbase config object dbTableCampaignList
			JSONObject tempTableCampaign = config.getJSONObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT);
			if (tempTableCampaign == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_NAME, "ad_CampaignList", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW, "allCampaignsView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW_DD, "adsmart", counter);

			// couchbase config object dbTableCampaignHistory
			JSONObject tempTableCampaignHistory = config.getJSONObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT);
			if (tempTableCampaignHistory == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_NAME, "ad_CampaignHistory", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW,
					"allCampaignsHistoryView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW_DD, "adsmart",
					counter);

			// couchbase config object dbTableChannelList
			JSONObject tempTableChannel = config.getJSONObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.OBJECT);
			if (tempTableChannel == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_NAME, "ad_ChannelList", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW, "allChannelsView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW_DD, "adsmart", counter);

			// couchbase config object dbTableStateMachine
			JSONObject tempTableStateMachine = config.getJSONObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.OBJECT);
			if (tempTableStateMachine == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.BUCKET_NAME, "ad_StateMachine", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.BUCKET_PASS, "", counter);

		}
		// END adsmartCfg object

		// START ethanCfg object
		JSONObject tempEthanConf = config.getJSONObject(ConfigKeys.FIXED.ETHAN_CFG.OBJECT);
		if (tempEthanConf == null) {
			logger.warn("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.OBJECT + " not found in configuration. Execution will not work.");
			counter.error += 32;
		} else {
			counter.correct += 32;

			checkIntDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.PARALLEL_THREADS_NUM, 2, 1, counter);
			
			expectedStrings.clear();
			expectedStrings.add("MSG");
			expectedStrings.add("WR");
			checkExpectedString(config, logger, ConfigKeys.FIXED.ETHAN_CFG.TIMESTAMP_HEADER_TYPE, "MSG", counter, expectedStrings);
			
			expectedStrings.clear();
			expectedStrings.add("SUBSCRIBER");
			expectedStrings.add("SMARTCARD");
			expectedStrings.add("DEVICEID");
			checkExpectedString(config, logger, ConfigKeys.FIXED.ETHAN_CFG.HEADER_IDENTIFIER, "DEVICEID", counter, expectedStrings);
			
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_DIR, counter);
			FileUtils.checkDirectoryMandatory(config.getString(ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_DIR), logger, counter);

			JSONArray inputFilePattern = config.getArray(ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_PATTERN.ARRAY);
			if (inputFilePattern == null || inputFilePattern.length() == 0) {
				logger.error("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_PATTERN.ARRAY
						+ " not found in configuration. Program will not be able to start.");
				counter.correct--;
				counter.error++;
			}

			String defaultOutDir = defaultDirOutput + programName + "-" + config.getString(ConfigKeys.FIXED.INSTANCE_NAME)
					+ File.separatorChar;
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_EVENTS_FILE_DIR, defaultOutDir, counter);
			checkDirExist(config, ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_EVENTS_FILE_DIR, logger);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_EVENTS_FILE_NAME, "ETHAN", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_FILE_NAME_TS_FORMAT, defaultTimestampFilePrefix, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_NAME, counter);
			String fileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_DIR);
			fileName += config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_NAME);
			FileUtils.checkFileMandatory(fileName, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME, counter);
			String fileName2 = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_DIR);
			fileName2 += config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
			FileUtils.checkFileMandatory(fileName2, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_NAME, counter);
			String fileName3 = config.getString(ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_DIR);
			fileName3 += config.getString(ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_NAME);
			FileUtils.checkFileMandatory(fileName3, logger, counter);

			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.XML_SCHEMA_FILE_DIR, counter);
			checkStringNoDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.XML_SCHEMA_FILE_NAME, counter);
			String fileName4 = config.getString(ConfigKeys.FIXED.ETHAN_CFG.XML_SCHEMA_FILE_DIR);
			fileName4 += config.getString(ConfigKeys.FIXED.ETHAN_CFG.XML_SCHEMA_FILE_NAME);
			FileUtils.checkFileMandatory(fileName4, logger, counter);

			String defaultOfsDir = defaultDirOffset + File.separatorChar + programName + "-" + config.getString(ConfigKeys.FIXED.INSTANCE_NAME)
					+ File.separatorChar;
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OFS_FILE_NAME, defaultDirOffset, counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.OFS_FILE_DIR, defaultOfsDir, counter);
			checkDirExist(config, ConfigKeys.FIXED.ETHAN_CFG.OFS_FILE_DIR, logger);

			// couchbase config object dbTableCampaignList
			JSONObject tempTableCampaign = config.getJSONObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT);
			if (tempTableCampaign == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_NAME, "ad_CampaignList", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW, "allCampaignsView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW_DD, "adsmart", counter);

			// couchbase config object dbTableCampaignHistory
			JSONObject tempTableCampaignHistory = config.getJSONObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT);
			if (tempTableCampaignHistory == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_NAME, "ad_CampaignHistory", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW,	"allCampaignsHistoryView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW_DD, "adsmart", counter);

			// couchbase config object dbTableChannelList
			JSONObject tempTableChannel = config.getJSONObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.OBJECT);
			if (tempTableChannel == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_NAME, "ad_ChannelList", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_PASS, "", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW, "allChannelsView", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW_DD, "adsmart", counter);

			// couchbase config object dbTableStateMachine
			JSONObject tempTableStateMachine = config.getJSONObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.OBJECT);
			if (tempTableStateMachine == null) {
				logger.warn("Parameter " + ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.OBJECT
						+ " not found in configuration. Default value will be created.");
				counter.error += 4;
				config.setObject(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.OBJECT, new JSONObject());
			}
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.BUCKET_NAME, "ethan_StateMachine", counter);
			checkStringDefault(config, logger, ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.BUCKET_PASS, "", counter);

		}
		// END ethanCfg object
	}

	/**
	 * Checks the validity of the parameters in the "runtime" section. If a parameter is not found or it has a value not valid, it is replaced (when possible).
	 * If not possible, returns the error.
	 * @param config the module's configuration object.
	 * @param logger the Log4j2 logger.
	 * @param counter the container of the correct, warning, and error parameters.
	 */
	public static void checkParametersRuntime(IConfig config, Logger logger, ConfigurationCounters counter) {

		counter.correct += 12;

		checkBooleanDefault(config, logger, ConfigKeys.RUNTIME.STATUS_RECOVERY, ConfigKeys.YES, counter);

		checkIntDefault(config, logger, ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC, 60, 1000, counter);
		checkIntDefault(config, logger, ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC, 1, 1000, counter);

		checkIntDefault(config, logger, ConfigKeys.RUNTIME.OFS_READ_CYCLE_INTERVAL_MSEC, 75, 1, counter);

		checkIntDefault(config, logger, ConfigKeys.RUNTIME.TBL_CAMPAIGN_FREQ_READ_MIN, 60, 60 * 1000, counter);
		checkIntDefault(config, logger, ConfigKeys.RUNTIME.TBL_CAMPAIGN_RETRY_NUM, 3, 1, counter);
		checkLongDefault(config, logger, ConfigKeys.RUNTIME.TBL_CAMPAIGN_HISTORY_READ_SINCE_DAY, 7, 24 * 60 * 60 * 1000, counter);
		checkBooleanDefault(config, logger, ConfigKeys.RUNTIME.TBL_CAMPAIGN_LOG, ConfigKeys.YES, counter);

		/*  Domains for tblChannelTimeRead array */
		JSONArray channelTimeRead = config.getArray(ConfigKeys.RUNTIME.TBL_CHANNEL_TIME_READ.ARRAY);
		try {
			if (channelTimeRead == null || channelTimeRead.length() == 0)
				throw new Exception();
			for (int i = 0; i < channelTimeRead.length(); i++) {
				if (channelTimeRead.getString(i).matches("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$") == false)
					throw new Exception();
			}
		} catch (Exception e1) {
			logger.warn("Parameter " + ConfigKeys.RUNTIME.TBL_CHANNEL_TIME_READ.ARRAY
					+ " not found or incorrect in config. Setting default value: ['07:00','14:00','21:00']");
			JSONArray defaultType = new JSONArray();
			defaultType.put("07:00");
			defaultType.put("14:00");
			defaultType.put("21:00");
			config.setArray(ConfigKeys.RUNTIME.TBL_CHANNEL_TIME_READ.ARRAY, defaultType);
			counter.correct--;
			counter.warning++;
		}

		checkIntDefault(config, logger, ConfigKeys.RUNTIME.TBL_CHANNEL_READ_SINCE_DAY, 7, 24 * 60 * 60 * 1000, counter);
		checkIntDefault(config, logger, ConfigKeys.RUNTIME.TBL_CHANNEL_RETRY_NUM, 3, 1, counter);
		checkBooleanDefault(config, logger, ConfigKeys.RUNTIME.TBL_CHANNEL_LOG, ConfigKeys.YES, counter);
		
		/* START write buffer configuration */
		JSONObject tempBuffer = config.getJSONObject(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.OBJECT);
		if (tempBuffer == null) {
			logger.warn("Parameter " + ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.OBJECT
					+ " not found in configuration. Setting default value: size 64 mb refreshInterval 1000 msec");
			config.setObject(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.OBJECT, new JSONObject());
			config.setInt(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_SIZE_MB, 64 * 1024 * 1024);
			config.setInt(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_TIME_SEC, 1000);
			counter.warning += 2;
		} else {
			counter.correct += 2;
			checkIntDefault(config, logger, ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_SIZE_MB, 64, 1024 * 1024, counter);
			checkIntDefault(config, logger, ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_TIME_SEC, 1, 1, counter);
		}
		/* END write buffer configuration */
	}

	/**
	 * Loads the default minimum fixed configuration, in order to keep the program working.
	 * @param config the module's configuration object.
	 * @param logger the Log4j2 logger.
	 */
	public static void loadDefaultMinimumConfigFixed(IConfig config, Logger logger) {
		String value = config.getString(ConfigKeys.FIXED.INSTANCE_NAME);
		if (value == null || value.isEmpty()) {
			config.setString(ConfigKeys.FIXED.INSTANCE_NAME, "M2_" + System.currentTimeMillis());
		}

		value = config.getString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR);
		if (value == null || value.isEmpty()) {
			config.setString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR, defaultDirStatus);
			FileUtils.checkDirectory(config.getString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR), logger);
		}

		value = config.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR);
		if (value == null || value.isEmpty()) {
			config.setString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR, defaultDirStatistics);
			FileUtils.checkDirectory(config.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR), logger);
		}

		JSONArray array = config.getArray(ConfigKeys.FIXED.COMMON_CFG.DATABASE_IP.ARRAY);
		if (array == null || array.length() == 0) {
			config.setArray(ConfigKeys.FIXED.COMMON_CFG.DATABASE_IP.ARRAY, defaultDbIp);
		}

	}

	/**
	 * Loads the default minimum runtime configuration, in order to keep the program working.
	 * @param config the module's configuration object.
	 * @param logger the Log4j2 logger.
	 */
	public static void loadDefaultMinimumConfigRuntime(IConfig config, Logger logger) {

		int runInt = config.getInteger(ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC);
		if (runInt <= 0) {
			config.setInt(ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC, 1000);
		}

		int statInt = config.getInteger(ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC);
		if (statInt <= 0) {
			config.setInt(ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC, 60000);
		}

		String value = config.getString(ConfigKeys.RUNTIME.STATUS_RECOVERY);
		if (value == null || value.isEmpty()) {
			config.setString(ConfigKeys.RUNTIME.STATUS_RECOVERY, ConfigKeys.YES);
		}
	}

}
