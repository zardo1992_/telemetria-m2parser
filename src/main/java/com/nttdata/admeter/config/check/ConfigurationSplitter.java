package com.nttdata.admeter.config.check;

import com.nttdata.admeter.config.JsonConfig;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.utils.LOG;

public class ConfigurationSplitter {

	private static IConfig oldAdsmartConfig;
	private static IConfig oldEthanConfig;
	private static IConfig newFileConfig;

	public static void createConfig(IConfig fileConfig) {
		if (newFileConfig == null)
			newFileConfig = fileConfig;
		else
			newFileConfig.loadParameters(fileConfig);

		createAdSmartConfig(fileConfig);
		createEthanConfig(fileConfig);
	}

	static private IConfig createAdSmartConfig(IConfig fileConfig) {

		IConfig adSmartConfig = new JsonConfig(LOG.getSlog());

		adSmartConfig.setString(Parameter.SOURCE, ErrorCode.Source.ADSMART);

		/* Fixed */
		adSmartConfig.setString(
				Parameter.INSTANCE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME));

		adSmartConfig.setString(
				Parameter.MODULE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.MODULE_NAME));

		adSmartConfig.setString(
				Parameter.MODULE_VERSION,
				fileConfig.getString(ConfigKeys.FIXED.MODULE_VERSION));

		adSmartConfig.setString(
				Parameter.DIR_STATUS,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR));

		adSmartConfig.setString(
				Parameter.DIR_STATISTICS,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR));

		adSmartConfig.setString(
				Parameter.ERROR_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_DIR));

		adSmartConfig.setString(
				Parameter.ERROR_DICTIONARY_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR));

		adSmartConfig.setString(
				Parameter.ERROR_DICTIONARY_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_NAME));

		adSmartConfig.setString(
				Parameter.STATUS_TS_FORMAT,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_NAME_TS_FORMAT));

		adSmartConfig.setString(
				Parameter.ERROR_TS_FORMAT,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_NAME_TS_FORMAT));
		
		adSmartConfig.setString(
                Parameter.DATABASE_READ_RETRY_NUM,
                fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_RETRY_NUM));
		
		adSmartConfig.setString(
                Parameter.DATABASE_READ_WAIT_MSEC,
                fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_WAIT_MSEC));
		
		adSmartConfig.setString(
                Parameter.DATABASE_WRITE_RETRY_NUM,
                fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_RETRY_NUM));
		
		adSmartConfig.setString(
                Parameter.DATABASE_WRITE_WAIT_MSEC,
                fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_WAIT_MSEC));

		adSmartConfig.setString(
                Parameter.OUTPUT_TS_FORMAT,
                fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_FILE_NAME_TS_FORMAT));

		adSmartConfig.setString(
				Parameter.OVERRIDE_CAMPAIGN_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_DIR));

		adSmartConfig.setString(
				Parameter.OVERRIDE_CAMPAIGN_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME));

		adSmartConfig.setString(
				Parameter.OVERRIDE_CHANNEL_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_DIR));

		adSmartConfig.setString(
				Parameter.OVERRIDE_CHANNEL_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_NAME));

		/* Runtime */
		adSmartConfig.setString(
				Parameter.MONITOR_STAT_INTERVAL_SEC,
				fileConfig.getString(ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC));
		adSmartConfig.setString(
				Parameter.MONITOR_STATUS_INTERVAL_SEC,
				fileConfig.getString(ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC));

		adSmartConfig.setString(
				Parameter.BUFFER_SIZE,
				fileConfig.getString(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_SIZE_MB));

		adSmartConfig.setString(
				Parameter.BUFFER_REFRESH,
				fileConfig.getString(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_TIME_SEC));

		// Carico le nuove configurazioni nel vecchio oggetto o ne creo uno nuovo
		if (oldAdsmartConfig == null)
			oldAdsmartConfig = adSmartConfig;
		else
			oldAdsmartConfig.loadParameters(adSmartConfig);

		return oldAdsmartConfig;
	}

	static private IConfig createEthanConfig(IConfig fileConfig) {

		IConfig ethanConfig = new JsonConfig(LOG.getSlog());

		ethanConfig.setString(Parameter.SOURCE, ErrorCode.Source.ETHAN);

		/* Fixed */
		ethanConfig.setString(
				Parameter.INSTANCE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME));

		ethanConfig.setString(
				Parameter.MODULE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.MODULE_NAME));

		ethanConfig.setString(
				Parameter.MODULE_VERSION,
				fileConfig.getString(ConfigKeys.FIXED.MODULE_VERSION));

		ethanConfig.setString(
				Parameter.DIR_STATUS,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR));

		ethanConfig.setString(
				Parameter.DIR_STATISTICS,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_DIR));

		ethanConfig.setString(
				Parameter.ERROR_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_DIR));

		ethanConfig.setString(
				Parameter.ERROR_DICTIONARY_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR));

		ethanConfig.setString(
				Parameter.ERROR_DICTIONARY_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_NAME));

		ethanConfig.setString(
				Parameter.STATUS_TS_FORMAT,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.STAT_FILE_NAME_TS_FORMAT));

		ethanConfig.setString(
				Parameter.OUTPUT_TS_FORMAT,
				fileConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OUTPUT_FILE_NAME_TS_FORMAT));

		ethanConfig.setString(
				Parameter.ERROR_TS_FORMAT,
				fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_FILE_NAME_TS_FORMAT));
		
	      ethanConfig.setString(
                Parameter.DATABASE_READ_RETRY_NUM,
                fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_RETRY_NUM));
          ethanConfig.setString(
              Parameter.DATABASE_READ_WAIT_MSEC,
              fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_WAIT_MSEC));
          ethanConfig.setString(
              Parameter.DATABASE_WRITE_RETRY_NUM,
              fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_RETRY_NUM));
          ethanConfig.setString(
              Parameter.DATABASE_WRITE_WAIT_MSEC,
              fileConfig.getString(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_WAIT_MSEC));

		ethanConfig.setString(
				Parameter.OVERRIDE_CAMPAIGN_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_DIR));

		ethanConfig.setString(
				Parameter.OVERRIDE_CAMPAIGN_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME));

		ethanConfig.setString(
				Parameter.OVERRIDE_CHANNEL_FILE_DIR,
				fileConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_DIR));

		ethanConfig.setString(
				Parameter.OVERRIDE_CHANNEL_FILE_NAME,
				fileConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_NAME));

		/* Runtime */
		ethanConfig.setString(
				Parameter.MONITOR_STAT_INTERVAL_SEC,
				fileConfig.getString(ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC));
		ethanConfig.setString(
				Parameter.MONITOR_STATUS_INTERVAL_SEC,
				fileConfig.getString(ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC));

		ethanConfig.setString(
				Parameter.BUFFER_SIZE,
				fileConfig.getString(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_SIZE_MB));

		ethanConfig.setString(
				Parameter.BUFFER_REFRESH,
				fileConfig.getString(ConfigKeys.RUNTIME.BUFFER_WRITE_CFG.BUFFER_TIME_SEC));

		// Carico le nuove configurazioni nel vecchio oggetto o ne creo uno nuovo
		if (oldEthanConfig == null)
			oldEthanConfig = ethanConfig;
		else
			oldEthanConfig.loadParameters(ethanConfig);

		return oldEthanConfig;
	}

	public static IConfig getOldAdsmartConfig() {
		return oldAdsmartConfig;
	}

	public static IConfig getOldEthanConfig() {
		return oldEthanConfig;
	}

	public static IConfig getNewFileConfig() {
		return newFileConfig;
	}

}
