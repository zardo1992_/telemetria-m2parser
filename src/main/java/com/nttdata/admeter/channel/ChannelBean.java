package com.nttdata.admeter.channel;

/**
 * Container of channel informations
 * @author MCortesi
 *
 */
public class ChannelBean {

	private String originalNetworkID;
	private String transportStreamID;
	private String siServiceID;
	private String serviceKey;
	private String name;
	private String definition;
	private String type;
	private long fromDate;
	private long toDate;
	
	public ChannelBean() {
		super();
	}

	public ChannelBean(String serviceKey, String type) {
		super();
		this.serviceKey = serviceKey;
		this.type = type;
	}
	
	public ChannelBean(String originalNetworkID, String transportStreamID, String siServiceID, String type) {
		super();
		this.originalNetworkID = originalNetworkID;
		this.transportStreamID = transportStreamID;
		this.siServiceID = siServiceID;
		this.type = type;
	}



	public String getOriginalNetworkID() {
		return originalNetworkID;
	}

	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}

	public String getTransportStreamID() {
		return transportStreamID;
	}

	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}

	public String getSiServiceID() {
		return siServiceID;
	}

	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	public String getServiceKey() {
		return serviceKey;
	}
	
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public long getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	
	public long getToDate() {
		return toDate;
	}
	
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	
	@Override
	public String toString() {
		return "OriginalNetworkID: " + originalNetworkID + ", TransportStreamID: "
				+ transportStreamID + ", SIServiceID: " + siServiceID + ", ServiceKey: " + serviceKey
				+ ", name: " + name + ", definition: " + definition + ", from: " + fromDate
				+ ", to: " + toDate;
	}
}
