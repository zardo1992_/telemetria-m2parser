package com.nttdata.admeter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.json.JSONException;

import com.nttdata.admeter.channel.ChannelBean;
import com.nttdata.admeter.config.ConfigManager;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.config.check.ConfigParserCheck;
import com.nttdata.admeter.config.check.ConfigurationSplitter;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.couchbase.CampaignsConfigUpdate;
import com.nttdata.admeter.couchbase.ChannelsConfigUpdate;
import com.nttdata.admeter.couchbase.DButils;
import com.nttdata.admeter.couchbase.Database;
import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IConfigUpdate;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.runnable.CBCampaignsReloaderRunner;
import com.nttdata.admeter.runnable.CBChannelsReloaderRunner;
import com.nttdata.admeter.runnable.CouchbaseConnectionRunnable;
import com.nttdata.admeter.runnable.ParserRunnableAdSmartMT;
import com.nttdata.admeter.runnable.ParserRunnableEthanMT;
import com.nttdata.admeter.runnable.StatParserRunnable;
import com.nttdata.admeter.runnable.StatusRunnable;
import com.nttdata.admeter.utils.LOG;
import com.nttdata.admeter.utils.TimeUtils;
import com.nttdata.admeter.watchdir.WatchDir;

import net.bull.javamelody.EmbeddedServer;

/**
 * Main class of Data Parser module:
 *  - Loads the Log4j configuration and creates the logger;
 *  - Loads the application configuration properties;
 *  - Loads the error dictionary and creates the error logger;
 *  - Creates the watchers for the application config and error dictionary files;
 *  - schedules the status and statistics runnables for the ModuleParser;
 *  - Instantiates the Parser that will read the input messages and write the output events.
 *  
 * @author BurrafatoMa
 * @version 1.0.0
 */
public class ModuleParserMain {

	private static String programName = "ModuleParser";
	private static String programVersion = "0.0.1";

	//protected static Logger logger = null;
	protected static ErrorLogger errorLogger = null;

	protected static IConfig jsonConfig;
	private static String configFileName;

	private static StatusRunnable statusRunnable;
	private static StatParserRunnable statisticsRunnable;
	private static Thread statusThread;
	private static Thread statisticsThread;

	private static WatchDir wdErrorLogger;
	private static WatchDir wdConfiguration;
	private static WatchDir wdChannelCacheAdSmart;
	private static WatchDir wdChannelCacheEthan;
	private static WatchDir wdCampaignCacheAdSmart;
	private static WatchDir wdCampaignCacheEthan;

	private static Thread parserThread;
	private static Thread parserEthanThread;

	private static Thread channelReloaderThread;
	private static CBChannelsReloaderRunner channelReloaderRunnable;
	private static Thread campaignReloaderThread;
	private static CBCampaignsReloaderRunner campaignReloaderRunnable;

	private static ParserRunnableAdSmartMT parserRunnableMT;
	private static ParserRunnableEthanMT parserRunnableEthanMT;

	private static CouchbaseConnectionRunnable couchbaseRunnable;
	private static Thread couchbaseThread;

	/**
	* Shows how to use this program and what kind of parameters it supports/requires.
	* <p>Usage: <i>program name</i> -l <i>log4j2 configuration file</i> -c <i> application configuration file</i></p>
	* <p>Usage: <i>program name</i> -v <i>(returns program version and usage)</i></p>
	* <p>Usage: <i>program name</i> -i <i>(enables shutting down by pressing return in the console as input)</i></p>
	* <p>Usage: <i>program name</i> -jm &lt;port&gt;<i>(enables javamelody and jetty server for performance testing. The results will be available at http://localhost:&lt;the port specified&gt;/)</i></p>
	*/
	private static void showUsage() {
		System.out.println(programName + " Version: " + programVersion);
		System.out.println("Usage:");
		System.out.println(programName + " -l <log4j2 configuration file> -c <application configuration file> (these parameters are mandatory)");
		System.out.println(programName + " -v (returns program version and usage)");
		System.out.println(programName + " -i (enables shutting down by pressing return in the console as input)");
		System.out.println(programName
				+ " -jm <port> (enables javamelody and jetty server for performance testing. The results will be available at http://localhost:<the port specified>/)");
		System.exit(0);
	}

	/**
	* Main class.
	* @param args log4j2 configuration file, and application configuration file.
	*/
	public static void main(String[] args) {

		getVersion();

		boolean enableJavaMelody = false;
		int javaMelodyPort = 0;
		String logFileName = "";
		configFileName = "";

		/* Load the command line parameters */

		if (args == null || args.length == 0) {
			showUsage();
		}

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-v")) {
				// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
				showUsage();
			} else if (args[i].equals("-jm")) {
				if (i + 1 >= args.length) {
					showUsage();
				} else {
					try {
						javaMelodyPort = Integer.parseInt(args[i + 1]);
					} catch (NumberFormatException e) {
						// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
						showUsage();
					}
					enableJavaMelody = true;
					i++;
				}
			} else if (args[i].equals("-l")) {
				if (i + 1 >= args.length) {
					// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
					showUsage();
				} else {
					logFileName = args[i + 1];
					i++;
				}
			} else if (args[i].equals("-c")) {
				if (i + 1 >= args.length) {
					// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
					showUsage();
				} else {
					configFileName = args[i + 1];
					i++;
				}
			} else {
				// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
				showUsage();
			}
		}

		// These parameters are mandatory
		if (logFileName.length() == 0 || configFileName.length() == 0) {
			// Note that this will also stop the program's execution and invalidate all the other parameters in the command line
			showUsage();
		}

		/* Log4j initialization */

		File file = new File(logFileName);
		if (file.exists()) {
			try {
				System.setProperty("log4j.configurationFile", file.toURI().toURL().toString());
			} catch (MalformedURLException e) {
				System.out.println(MessageFormat.format("LogFile configuration {0} NOT found; using root Logger.", file.getAbsolutePath()));
				e.printStackTrace();
			}
		}

		LOG.info("Log4J correctly initialized");

		/* Adding shutdown hook to correctly close the line when the program ends */

		Runtime.getRuntime().addShutdownHook(new Thread("shut") {
			public void run() {
				shutDown();
			}
		});

		/* Program's configuration initialization */

		jsonConfig = new ConfigManager(LOG.getSlog());

		jsonConfig.setString(ConfigKeys.FIXED.MODULE_NAME, programName);
		jsonConfig.setString(ConfigKeys.FIXED.MODULE_VERSION, programVersion);
		ConfigurationCounters counterFixed = loadConfigFile(jsonConfig, configFileName, ConfigKeys.FIXED.SECTION, true);
		ConfigurationCounters counterRuntime = loadConfigFile(jsonConfig, configFileName, ConfigKeys.RUNTIME.SECTION, true);
		ConfigurationSplitter.createConfig(jsonConfig);
		IConfig oldConfig = ConfigurationSplitter.getOldAdsmartConfig();

		boolean instantSetKo = false;

		if (counterRuntime.correct == 0) {
			// StatusRunnable not created yet. We're saving the keepOut for later.
			instantSetKo = true;
			// Not writing to error logger because it is not created yet.
		}

		if (counterFixed.error > 0) {
			LOG.error("Could not load section \"" + ConfigKeys.FIXED.SECTION + "\" from the config file! " + counterFixed.correct + " correct, "
					+ counterFixed.warning + " warnings, " + counterFixed.error + " errors");
			writeKoAndExit();
		}

		if (counterRuntime.error > 0) {
			LOG.error("Could not load section \"" + ConfigKeys.RUNTIME.SECTION + "\" from the config file! " + counterRuntime.correct
					+ " correct, " + counterRuntime.warning + " warnings, " + counterRuntime.error + " errors");
			// StatusRunnable not created yet. We're saving the keepOut for later.
			instantSetKo = true;
			// Not writing to error logger because it is not created yet.
		}

		LOG.info("Configuration loaded. Found " + (counterFixed.correct + counterRuntime.correct) + " parameters; "
				+ (counterFixed.warning + counterRuntime.warning) + " have been corrected with default values.");

		/* Monitoring threads initialization */

		statusRunnable = new StatusRunnable(oldConfig, LOG.getSlog(), errorLogger,
				Long.valueOf(jsonConfig.getString(ConfigKeys.RUNTIME.MONITOR_RUN_INTERVAL_SEC)));
		statisticsRunnable = new StatParserRunnable(oldConfig, errorLogger, statusRunnable,
				Long.valueOf(jsonConfig.getString(ConfigKeys.RUNTIME.MONITOR_STAT_INTERVAL_SEC)));
		statusThread = new Thread(statusRunnable, "status");
		statusThread.start();
		statisticsThread = new Thread(statisticsRunnable, "stat");
		statisticsThread.start();

		// WARNING: this is a PERMANENT keepOut situation. 
		// It only occurs if there is a severe error in the runtime configuration (i.e. invalid input) 
		// and the application could not work anyway.
		// If this ever happen, you have to correct the config file, and the runtime parameters will be reloaded.
		if (instantSetKo) {
			LOG.error("ERROR: severe errors found. Program will not be able to work. Program will be put in constant KeepOut mode.");
			statusRunnable.setStatus(Status.INDEX_PARSERMAIN, -1);
		}

		/* Error Logger initialization */

		errorLogger = new ErrorLogger(statusRunnable, statisticsRunnable, oldConfig, LOG.getSlog());

		/* WatchFile threads initialization */

		Path dir = Paths.get((jsonConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_DIR) +
				jsonConfig.getString(ConfigKeys.FIXED.COMMON_CFG.ERROR_DICTIONARY_FILE_NAME)));
		if (dir.getParent() != null) {
			try {
				wdErrorLogger = new WatchDir(dir.getParent(), false, errorLogger, LOG.getSlog(), errorLogger, "wdErr");
				wdErrorLogger.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to error dictionary: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		dir = Paths.get(configFileName);
		if (dir.getParent() != null) {
			try {
				wdConfiguration = new WatchDir(dir.getParent(), false, new ConfigUpdater(), LOG.getSlog(), errorLogger, "wdConf");
				wdConfiguration.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to configuration file: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		dir = Paths.get(jsonConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_DIR) +
				jsonConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_NAME));
		if (dir.getParent() != null) {
			try {
				IConfigUpdate fileReloader = new ChannelsConfigUpdate(errorLogger, jsonConfig, ConfigKeys.ADSMART);
				fileReloader.update();
				wdChannelCacheAdSmart = new WatchDir(dir.getParent(), false, fileReloader, LOG.getSlog(), errorLogger, "wdChannelCacheAdSmart");
				wdChannelCacheAdSmart.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to channels list file: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		dir = Paths.get(jsonConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_DIR) +
				jsonConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_NAME));
		if (dir.getParent() != null) {
			try {
				IConfigUpdate fileReloader = new ChannelsConfigUpdate( errorLogger, jsonConfig, ConfigKeys.ETHAN);
				fileReloader.update();
				wdChannelCacheEthan = new WatchDir(dir.getParent(), false, fileReloader, LOG.getSlog(), errorLogger, "wdChannelCacheEthan");
				wdChannelCacheEthan.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to channels list file: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		dir = Paths.get(jsonConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_DIR) +
				jsonConfig.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME));
		if (dir.getParent() != null) {
			try {
				IConfigUpdate fileReloader = new CampaignsConfigUpdate(errorLogger, jsonConfig, ConfigKeys.ADSMART);
				fileReloader.update();
				wdCampaignCacheAdSmart = new WatchDir(dir.getParent(), false, fileReloader, LOG.getSlog(), errorLogger, "wdCampaignCacheAdSmart");
				wdCampaignCacheAdSmart.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to campaigns list file: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		dir = Paths.get(jsonConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_DIR) +
				jsonConfig.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME));
		if (dir.getParent() != null) {
			try {
				IConfigUpdate fileReloader = new CampaignsConfigUpdate(errorLogger, jsonConfig, ConfigKeys.ETHAN);
				fileReloader.update();
				wdCampaignCacheEthan = new WatchDir(dir.getParent(), false, fileReloader, LOG.getSlog(), errorLogger, "wdCampaignCacheEthan");
				wdCampaignCacheEthan.start();
			} catch (IOException e) {
				LOG.error("Error setting watch to campaigns list file: ", e);
				errorLogger.write(ErrorCode.WATCHFILE_NOT_SET, "[module:Parser,method:Main,exception:" + e.getMessage() + "]");
			}
		}

		/* Parameters for javamelody and jetty server for performance testing */
		if (enableJavaMelody && javaMelodyPort > 0) {
			LOG.info("Enabling javamelody for performance testing.");

			final Map<net.bull.javamelody.Parameter, String> parameters = new HashMap<>();
			parameters.put(net.bull.javamelody.Parameter.AUTHORIZED_USERS, "admin:admin");
			parameters.put(net.bull.javamelody.Parameter.STORAGE_DIRECTORY, "/tmp/javamelody");
			parameters.put(net.bull.javamelody.Parameter.SAMPLING_SECONDS, "1.0");
			parameters.put(net.bull.javamelody.Parameter.MONITORING_PATH, "/");

			try {
				EmbeddedServer.start(javaMelodyPort, parameters);
			} catch (Exception e) {
				LOG.error("Unable to start Embedded server for performance evaluation!", e);
			}
		}

		/* Start actual functionality of this program (parsing the incoming messages) */
		errorLogger.write(ErrorCode.PARSER_READY, "[instance:" + jsonConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME) + "]");

		Database.setConfig(jsonConfig);
		// I connect to the DB, if error I log the error and shut down the service
		try {
			Database.connect();
		} catch (Exception e) {
			errorLogger.write(ErrorCode.DB_CONNECTION,
					"[module:ExternalDataInterface,method:ContextListener.contextInitialized,error:" + e.getMessage() + "]");
			LOG.error("Error connecting to couchbase: " + e);
			statusRunnable.setStatus(Status.INDEX_CONTEXTLISTENER_FIXED, -1);
			System.exit(0);
		}

		// I start the couchbase monitor runnable
		couchbaseRunnable = new CouchbaseConnectionRunnable(statusRunnable);
		couchbaseThread = new Thread(couchbaseRunnable, "couchbaseMonitor");
		couchbaseThread.start();

		// Loading channels from DB for the first time
		try {
			DButils.reloadChannelsCouchbase(errorLogger, jsonConfig, ConfigKeys.ADSMART);			
			DButils.reloadChannelsCouchbase(errorLogger, jsonConfig, ConfigKeys.ETHAN);

			if (jsonConfig.getString(ConfigKeys.RUNTIME.TBL_CHANNEL_LOG).equalsIgnoreCase(ConfigKeys.YES)) {
				for (ChannelBean entry : DButils.getChannelsCouchbase(ConfigKeys.ADSMART)) {
					LOG.info(Source.ADSMART, "CHANNELRELOAD serviceKey: " + entry.getServiceKey() + " DVB:[" + entry.getOriginalNetworkID() + ","
							+ entry.getTransportStreamID() + "," + entry.getSiServiceID() + "]");
				}
				for (ChannelBean entry : DButils.getChannelsCouchbase(ConfigKeys.ETHAN)) {
					LOG.info(Source.ETHAN, "CHANNELRELOAD serviceKey: " + entry.getServiceKey() + " DVB:[" + entry.getOriginalNetworkID() + ","
							+ entry.getTransportStreamID() + "," + entry.getSiServiceID() + "]");
				}
			}
		} catch (Exception e) {
			LOG.error("Error loading from Couchbase: ", e);
		}
		
		// Loading channels from DB for the first time
		try {
			DButils.reloadCampaignsCouchbase(errorLogger, jsonConfig, ConfigKeys.ADSMART);			
			DButils.reloadCampaignsCouchbase(errorLogger, jsonConfig, ConfigKeys.ETHAN);

			if (jsonConfig.getString(ConfigKeys.RUNTIME.TBL_CHANNEL_LOG).equalsIgnoreCase(ConfigKeys.YES)) {
				for (Map.Entry<String, Integer> entry : DButils.getCampaignsCouchbase(ConfigKeys.ADSMART).entrySet()) {
					LOG.info(Source.ADSMART, "CAMPAIGNRELOAD | campaignID: " + entry.getKey() + ", duration: " + entry.getValue());
				}
				for (Map.Entry<String, Integer> entry : DButils.getCampaignsCouchbase(ConfigKeys.ETHAN).entrySet()) {
					LOG.info(Source.ETHAN, "CAMPAIGNRELOAD | campaignID: " + entry.getKey() + ", duration: " + entry.getValue());
				}
			}
		} catch (Exception e) {
			LOG.error("Error loading from Couchbase: ", e);
		}
		
		LOG.debug("Available processors:" + Runtime.getRuntime().availableProcessors());
		parserRunnableMT = new ParserRunnableAdSmartMT(errorLogger, jsonConfig, statisticsRunnable, statusRunnable);
		parserThread = new Thread(parserRunnableMT, "parserRunnableMT");
		parserThread.start();

		parserRunnableEthanMT = new ParserRunnableEthanMT(errorLogger, jsonConfig, statisticsRunnable, statusRunnable);
		parserEthanThread = new Thread(parserRunnableEthanMT, "parserRunnableEthanMT");
		parserEthanThread.start();
		
		/*  runnable to reload from couchbase campaigns and channels every time based on config  */
		channelReloaderRunnable = new CBChannelsReloaderRunner(errorLogger, jsonConfig, statusRunnable);
		channelReloaderThread = new Thread(channelReloaderRunnable, "channelReloaderRunnable");
		channelReloaderThread.start();

		campaignReloaderRunnable = new CBCampaignsReloaderRunner(errorLogger, jsonConfig, statusRunnable);
		campaignReloaderThread = new Thread(campaignReloaderRunnable, "campaignReloaderRunnable");
		campaignReloaderThread.start();
	}

	/**
	* Method called if the configuration is incorrect and the module can't work.
	* Creates a run file with "KO" status in the default directory and then exits.
	*/
	private static void writeKoAndExit() {
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			String fileDir = jsonConfig.getString(ConfigKeys.FIXED.COMMON_CFG.RUN_FILE_DIR);
			File directory = new File(fileDir);
			if (!directory.exists())
				directory.mkdirs();
			File file = new File(
					directory +
							File.separator +
							jsonConfig.getString(ConfigKeys.FIXED.MODULE_NAME) +
							"-" +
							jsonConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME) +
							".run");
			if (!file.exists())
				file.createNewFile();

			String line = "{\"objlist\":[{\"timestamp\":\"" +
					TimeUtils.long2String(errorLogger) +
					"\",\"module\":\"" +
					jsonConfig.getString(ConfigKeys.FIXED.MODULE_NAME) +
					"\",\"instance\":\"" +
					jsonConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME) +
					"\"}],\"run\":\"KO\"}";

			fw = new FileWriter(file, false);
			bw = new BufferedWriter(fw);
			bw.write(line);
			bw.newLine();
		} catch (IOException e) {
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException e) {
			}
		}

		System.exit(0);
	}

	/**
	* Stop all application's threads when System.exit() is called (or the application is stopped in some other ways).
	*/
	private static void shutDown() {
		System.out.println(programName + (jsonConfig != null ? "-" + jsonConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME) : "")
				+ " is shutting down - closing application");
		LOG.info("Module is shutting down - closing application");

		if (parserThread != null) {
			LOG.debug("Stopping adsmart parser thread");
			parserRunnableMT.stopThreads();
		}
		
		if (parserEthanThread != null) {
			LOG.debug("Stopping ethan parser thread");
			parserRunnableEthanMT.stopThreads();
		}

		stopThread(statusThread);
		stopThread(statisticsThread);

		stopThread(wdErrorLogger);
		stopThread(wdConfiguration);

		stopThread(wdChannelCacheAdSmart);
		stopThread(wdChannelCacheEthan);

		stopThread(wdCampaignCacheAdSmart);
		stopThread(wdCampaignCacheEthan);

		stopThread(channelReloaderThread);
		stopThread(campaignReloaderThread);

		stopThread(couchbaseThread);

		if (errorLogger != null) {
			LOG.debug("Stopping error logger");
			errorLogger.stop();
		}

		if (LogManager.getContext() instanceof LoggerContext) {
			LOG.debug("Shutting down log4j2");
			Configurator.shutdown((LoggerContext) LogManager.getContext());
		} else
			LOG.warn("Unable to shutdown log4j2");

		System.out.println(programName + (jsonConfig != null ? "-" + jsonConfig.getString(ConfigKeys.FIXED.INSTANCE_NAME) : "") + " Done.");
	}

	static private void stopThread(Thread value) {
		if (value != null) {
			LOG.debug("Stopping configuration " + value.getName());
			value.interrupt();
			try {
				value.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	* Loads a section of the json config file in the servlet context.
	* @param config the module configuration object, where to store the parameters.
	* @param filename the full path to the json file.
	* @param section the section to be loaded (fixed or runtime).
	* @param firstLoad if the function is called for the first time (i.e. at the start of the program) or in a second moment, when the configuration file has been modified.
	* @return the counters of the parameters loaded (number of correct loads, number of warnings, number of errors).
	*/
	private static ConfigurationCounters loadConfigFile(IConfig config, String filename, String section, boolean firstLoad) {

		ConfigurationCounters result = new ConfigurationCounters();

		File file = new File(filename);
		if (!file.exists()) {
			LOG.error("Configuration file " + file.getAbsolutePath() + " not found");
			if (errorLogger != null)
				errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:Main.loadConfigFile,file:" + file.getAbsolutePath() + "]");
		} else {

			try {
				if (config == null)
					config = new ConfigManager(LOG.getSlog());

				result = config.load(file.getAbsolutePath(), section);

				if (result.error == 0) {

					// We load the config in the servlet context only if the loading was successful; otherwise we leave the old values
					if (section.equals(ConfigKeys.FIXED.SECTION))
						ConfigParserCheck.checkParametersFixed(config, LOG.getSlog(), programName, result);
					else
						ConfigParserCheck.checkParametersRuntime(config, LOG.getSlog(), result);

					jsonConfig.loadParameters(config);
					if (statusRunnable != null)
						statusRunnable.removeStatus(Status.INDEX_PARSERMAIN);
					if (!firstLoad)
						ConfigurationSplitter.createConfig(jsonConfig);
				} else if (firstLoad) {
					// But if this is the first load, we load it anyway (to avoid problems later)
					jsonConfig.loadParameters(config);
				}
				if (result.error > 0) {
					LOG.error("Configuration file " + file.getAbsolutePath() + " not correctly loaded. " + result.correct + " correct, "
							+ result.warning + " warnings, " + result.error + " errors");
					if (errorLogger != null)
						errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:Main.loadConfigFile,file:" + file.getAbsolutePath() + "]");
				} else if (result.warning > 0) {
					LOG.warn("Configuration file " + file.getAbsolutePath() + " not correctly loaded. " + result.correct + " correct, "
							+ result.warning + " warnings, " + result.error + " errors");
					if (errorLogger != null)
						errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:Main.loadConfigFile,file:" + file.getAbsolutePath() + "]");
				}
			} catch (IOException | JSONException e) {
				LOG.error("Error loading configuration file " + file.getAbsolutePath() + ": ", e);
				if (errorLogger != null)
					errorLogger.write(ErrorCode.FILE_NOT_FOUND,
							"[module:Parser,method:Main.loadConfigFile,file:" + file.getAbsolutePath() + ",message:" + e.getMessage() + "]");
			}
		}

		if (section.equals(ConfigKeys.FIXED.SECTION))
			ConfigParserCheck.loadDefaultMinimumConfigFixed(jsonConfig, LOG.getSlog());
		else
			ConfigParserCheck.loadDefaultMinimumConfigRuntime(jsonConfig, LOG.getSlog());

		return result;
	}

	/**
	* Private class for the update of the runtime configuration.
	* It also reloads the configuration in the monitor thread, if it has changed.
	* @author BurrafatoMa
	* @version 1.0.1
	*/
	private static class ConfigUpdater implements IConfigUpdate {

		public boolean update() {
			LOG.debug("Refreshing runtime config from file...");
			IConfig tempConfig = null;

			ConfigurationCounters result = loadConfigFile(tempConfig, configFileName, ConfigKeys.RUNTIME.SECTION, false);
			if (result.error == 0) {
				LOG.info("Configuration runtime loaded. Found " + (result.correct) + " parameters; " + (result.warning)
						+ " have been corrected with default values.");
				statusRunnable.updateTimeout();
				statisticsRunnable.updateTimeout();

				return true;
			} else {
				LOG.error("Configuration file not correctly loaded. Keeping the old configuration. " + result.correct + " correct, "
						+ result.warning + " warnings, " + result.error + " errors");
				return false;
			}
		}

		@Override
		public String getConfigFileName() {
			File file = new File(configFileName);
			return file.getName();
		}
	}

	/**
	* Gets the program version from the module properties file.
	*/
	private static void getVersion() {
		try {
			java.io.InputStream is = ModuleParserMain.class.getResourceAsStream("/module.properties");
			if (is != null) {
				java.util.Properties p = new Properties();
				p.load(is);
				if (p != null) {
					programName = p.getProperty("name");
					programVersion = p.getProperty("version");
					System.out.println("Running " + p.getProperty("name") + " version " + p.getProperty("version"));
				}
			}
		} catch (Exception e) {
			System.out.println("Error getting jar properties: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static IConfig getJsonConfig() {
		return jsonConfig;
	}

}
