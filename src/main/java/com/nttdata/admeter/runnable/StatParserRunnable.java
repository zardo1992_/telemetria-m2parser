package com.nttdata.admeter.runnable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constants.M2Statistics;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.LOG;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * A runnable class used to write the statistics for this module
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class StatParserRunnable extends StatRunnable {

	/**
	 * Minimum difference between the time when the message is received by the collector, and the time when the message is computed by the parser.
	 */
	private long minDelayProcessingMillisAdsmart;
	private long minDelayProcessingMillisEthan;
	/**
	 * Maximum difference between the time when the message is received by the collector, and the time when the message is computed by the parser.
	 */
	private long maxDelayProcessingMillisAdsmart;
	private long maxDelayProcessingMillisEthan;

	/**
	 * Constructor for the runnable that writes this module's statistics.
	 * @param config the configuration container object.
	 * @param logger the Log4j2 logger.
	 * @param errorLogger the error logger.
	 * @param statusRunnable the Runnable monitoring the status of all the program's threads.
	 * @param timeout the maximum timeout for this thread.
	 */
	public StatParserRunnable(IConfig config, ErrorLogger errorLogger, StatusRunnable statusRunnable, long timeout) {
		super(config, LOG.getSlog(), errorLogger, statusRunnable, timeout);
	}

	/**
	 * Write the statistics file (appends another line if already existing).
	 * @param file a reference to the file to write.
	 */
	@Override
	protected synchronized void writeStatistics(File file) {
		Runtime runtime = Runtime.getRuntime();

		String line = "{\"timestamp\":\"" +
				TimeUtils.long2String(errorLogger) +
				"\",\"module\":\"" +
				moduleName +
				"\",\"version\":\"" +
				moduleVersion +
				"\",\"instance\":\"" +
				instanceName + "\"," +

				"\"adsmart\":{" +
				"\"bufferWriteProcessed\":\"" +
				statistics.get(M2Statistics.BUFFER_WRITE_ADSMART) +
				"\",\"minDelayProcessingMillis\":\"" +
				minDelayProcessingMillisAdsmart +
				"\",\"maxDelayProcessingMillis\":\"" +
				maxDelayProcessingMillisAdsmart +
				"\",\"msgProcessed\":\"" +
				statistics.get(M2Statistics.MESSAGES_PROCESSED_ADSMART) +
				"\",\"msgDiscarded\":\"" +
				statistics.get(M2Statistics.MESSAGES_DISCARDED_ADSMART) +
				"\",\"totalEventProcessed\":\"" +
				statistics.get(M2Statistics.EVENTS_PROCESSED_ADSMART) +
				"\",\"totalEventDiscarded\":\"" +
				statistics.get(M2Statistics.EVENTS_DISCARDED_ADSMART) +
				"\",\"unknownSpeed\":\"" +
				statistics.get(M2Statistics.UNKNOWN_SPEED_ADSMART) +
				"\",\"unknownChannel\":\"" +
				statistics.get(M2Statistics.UNKNOWN_CHANNEL_ADSMART) +
				"\",\"unknownCampaign\":\"" +
				statistics.get(M2Statistics.UNKNOWN_CAMPAIGN_ADSMART) +
				"\",\"numGenericError\":\"" +
				statistics.get(M2Statistics.GENERIC_ERRORS_ADSMART) + "\"}," +

				"\"ethan\":{" +
				"\"bufferWriteProcessed\":\"" +
				statistics.get(M2Statistics.BUFFER_WRITE_ETHAN) +
				"\",\"minDelayProcessingMillis\":\"" +
				minDelayProcessingMillisEthan +
				"\",\"maxDelayProcessingMillis\":\"" +
				maxDelayProcessingMillisEthan +
				"\",\"msgProcessed\":\"" +
				statistics.get(M2Statistics.MESSAGES_PROCESSED_ETHAN) +
				"\",\"msgDiscarded\":\"" +
				statistics.get(M2Statistics.MESSAGES_DISCARDED_ETHAN) +
				"\",\"totalEventProcessed\":\"" +
				statistics.get(M2Statistics.EVENTS_PROCESSED_ETHAN) +
				"\",\"totalEventDiscarded\":\"" +
				statistics.get(M2Statistics.EVENTS_DISCARDED_ETHAN) +
				"\",\"unknownSpeed\":\"" +
				statistics.get(M2Statistics.UNKNOWN_SPEED_ETHAN) +
				"\",\"unknownChannel\":\"" +
				statistics.get(M2Statistics.UNKNOWN_CHANNEL_ETHAN) +
				"\",\"unknownCampaign\":\"" +
				statistics.get(M2Statistics.UNKNOWN_CAMPAIGN_ETHAN) +
				"\",\"numGenericError\":\"" +
				statistics.get(M2Statistics.GENERIC_ERRORS_ETHAN) + "\"}," +

				"\"numGenericError\":\"" +
				statistics.get(M2Statistics.GENERIC_ERRORS_COMMON) +
				"\",\"memUsedKB\":\"" +
				runtime.totalMemory() / 1024 +
				"\",\"memFreeKB\":\"" +
				runtime.freeMemory() / 1024 +
				"\",\"memMaxKB\":\"" +
				runtime.maxMemory() / 1024 +
				"\",\"numThreads\":\"" +
				Thread.activeCount() +
				"\"}";
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			fw = new FileWriter(file, true);
			bw = new BufferedWriter(fw);
			bw.write(line);
			bw.newLine();
		} catch (IOException e) {
			logger.error("Exception writing statistics: ", e);
			errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:StatRunnable.writeStatistics,exception:" + e.getMessage() + "]");
			statusRunnable.setStatus(Status.INDEX_STATISTICS, -1L);
		} finally {
			
	        resetStatistics();
	        
			try {
				if (bw != null) {
					bw.close();
					bw = null;
				}
			} catch (IOException e) {
				logger.error("Exception closing buffer for statistics: ", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:StatRunnable.writeStatistics,exception:" + e.getMessage() + "]");
				statusRunnable.setStatus(Status.INDEX_STATISTICS, -1L);
			}
			try {
				if (fw != null) {
					fw.close();
					fw = null;
				}
			} catch (IOException e) {
				logger.error("Exception closing buffer for statistics: ", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:StatRunnable.writeStatistics,exception:" + e.getMessage() + "]");
				statusRunnable.setStatus(Status.INDEX_STATISTICS, -1L);
			}
		}
	}

	/**
	 * Resets the statistics before writing them again.
	 */
	@Override
	protected synchronized void resetStatistics() {
		statistics.put(M2Statistics.BUFFER_WRITE_ADSMART, 0);
		statistics.put(M2Statistics.MESSAGES_PROCESSED_ADSMART, 0);
		statistics.put(M2Statistics.MESSAGES_DISCARDED_ADSMART, 0);
		statistics.put(M2Statistics.EVENTS_PROCESSED_ADSMART, 0);
		statistics.put(M2Statistics.EVENTS_DISCARDED_ADSMART, 0);
		statistics.put(M2Statistics.UNKNOWN_SPEED_ADSMART, 0);
		statistics.put(M2Statistics.UNKNOWN_CHANNEL_ADSMART, 0);
		statistics.put(M2Statistics.UNKNOWN_CAMPAIGN_ADSMART, 0);
		statistics.put(M2Statistics.GENERIC_ERRORS_ADSMART, 0);

		statistics.put(M2Statistics.BUFFER_WRITE_ETHAN, 0);
		statistics.put(M2Statistics.MESSAGES_PROCESSED_ETHAN, 0);
		statistics.put(M2Statistics.MESSAGES_DISCARDED_ETHAN, 0);
		statistics.put(M2Statistics.EVENTS_PROCESSED_ETHAN, 0);
		statistics.put(M2Statistics.EVENTS_DISCARDED_ETHAN, 0);
		statistics.put(M2Statistics.UNKNOWN_SPEED_ETHAN, 0);
		statistics.put(M2Statistics.UNKNOWN_CHANNEL_ETHAN, 0);
		statistics.put(M2Statistics.UNKNOWN_CAMPAIGN_ETHAN, 0);
		statistics.put(M2Statistics.GENERIC_ERRORS_ETHAN, 0);

		statistics.put(M2Statistics.GENERIC_ERRORS_COMMON, 0);
		statistics.put(M2Statistics.MIN_DELAY_PROCESSING_MILLIS_ADSMART, 0);
		statistics.put(M2Statistics.MIN_DELAY_PROCESSING_MILLIS_ETHAN, 0);
		statistics.put(M2Statistics.MAX_DELAY_PROCESSING_MILLIS_ADSMART, 0);
		statistics.put(M2Statistics.MAX_DELAY_PROCESSING_MILLIS_ETHAN, 0);

		minDelayProcessingMillisAdsmart = -1l;
		maxDelayProcessingMillisAdsmart = -1l;
		minDelayProcessingMillisEthan = -1l;
		maxDelayProcessingMillisEthan = -1l;

		loadFactorSum = 0;
		loadFactorNum = 0;
	}

	/**
	 * Handling minimum and maximum values of processing delay, in milliseconds.
	 * @param timestart timestamp of the message from the input (i.e. when the collector wrote this message in its output file.
	 * @param timeend   timestamp of this very moment (i.e. when I am parsing the message).
	 */
	public synchronized void handleDelayProcessingMillis(long timestart, long timeend, String source) {
		long timediff = timeend - timestart;

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			// AdSmart
			if (minDelayProcessingMillisAdsmart < 0L)
				minDelayProcessingMillisAdsmart = timediff;
			else
				minDelayProcessingMillisAdsmart = Math.min(timediff, minDelayProcessingMillisAdsmart);

			if (maxDelayProcessingMillisAdsmart < 0l)
				maxDelayProcessingMillisAdsmart = timediff;
			else
				maxDelayProcessingMillisAdsmart = Math.max(timediff, maxDelayProcessingMillisAdsmart);
		} else {
			// Ethan
			if (minDelayProcessingMillisEthan < 0L)
				minDelayProcessingMillisEthan = timediff;
			else
				minDelayProcessingMillisEthan = Math.min(timediff, minDelayProcessingMillisEthan);

			if (maxDelayProcessingMillisEthan < 0l)
				maxDelayProcessingMillisEthan = timediff;
			else
				maxDelayProcessingMillisEthan = Math.max(timediff, maxDelayProcessingMillisEthan);
		}
	}
}
