package com.nttdata.admeter.runnable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.config.check.ConfigurationSplitter;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.output.FileOffsetElement;
import com.nttdata.admeter.utils.LOG;

/**
 * Multithreaded version of FileOffsetReader
 * @author MCortesi
 *
 */
public class FileOffsetReaderMT {

	private ErrorLogger errorLogger;
	private IConfig config;
	private StatRunnable statisticsRunnable;

	private File offsetFile;
	private File inputFilesDir;
	private Map<String, FileOffsetElement> fileOffsets;

	private int loadFactor = 0;

	// Used to check if the offset was not updated
	private long lastOffsetUpdate = 0;

	final
	private String source;

	public synchronized long getLastOffsetUpdate() {
		return lastOffsetUpdate;
	}

	public Map<String, FileOffsetElement> getFileOffsets() {
		return fileOffsets;
	}

	public FileOffsetReaderMT(IConfig config, ErrorLogger errorLogger, StatRunnable statisticsRunnable, String source) {
		this.config = config;
		this.errorLogger = errorLogger;
		this.statisticsRunnable = statisticsRunnable;
		this.source = source;

		String inputFileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_DIR);
		if (this.source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			inputFileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_DIR);
		}
		this.inputFilesDir = new File(inputFileName);

		String offsetFileDir = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OFS_FILE_DIR);
		String offsetFileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OFS_FILE_NAME);
		if (this.source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			offsetFileDir = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OFS_FILE_DIR);
			offsetFileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OFS_FILE_NAME);
		}
		this.offsetFile = new File(offsetFileDir, offsetFileName);

		this.fileOffsets = Collections.synchronizedSortedMap(new TreeMap<String, FileOffsetElement>());
		loadOffsets();
	}

	/**
	 * Called at object creation. Reads the offset file, parses it, and stores all the references to to files in a treeMap.
	 */
	private void loadOffsets() {
		FileReader fReader = null;
		BufferedReader bReader = null;
		String line;
		String[] foel;

		if (offsetFile.exists()) {
			LOG.info(source, "File offset " + offsetFile.getAbsolutePath() + " found with size " + offsetFile.length());
			try {
				fReader = new FileReader(offsetFile);
				bReader = new BufferedReader(fReader);
				// Read offset file and initialize entries
				while ((line = bReader.readLine()) != null) {
					foel = line.split(Output.SEPARATOR_REGEX);
					// If an entry is malformed, discard it
					if (foel.length == 2) {
						try {
							String fileName = foel[0];
							long offset = Long.parseLong(foel[1]);
							// If fileOffsets does not already contains some reference to the file, add it with an offset of 0
							if (!fileOffsets.containsKey(fileName)) {
								File file = new File(inputFilesDir, fileName);
								
								if(source.equalsIgnoreCase(ConfigKeys.ADSMART)){
									fileOffsets.put(fileName, new FileOffsetElement(LOG.getSlog(), errorLogger, ConfigurationSplitter.getOldAdsmartConfig(), file, 0L, offset));
									LOG.debug(Source.ADSMART, "Loaded file " + fileName + " with offset " + offset + " and size " + file.length());
								}
								else{
									fileOffsets.put(fileName, new FileOffsetElement(LOG.getSlog(), errorLogger, ConfigurationSplitter.getOldEthanConfig(), file, 0L, offset));
									LOG.debug(Source.ETHAN, "Loaded file " + fileName + " with offset " + offset + " and size " + file.length());
								}
								
							}
						} catch (NumberFormatException e) {
							LOG.error(source, "Error reading offset file " + offsetFile.getAbsolutePath() + "; ", e);
							errorLogger.write(ErrorCode.FILE_NOT_FOUND,
									"[module:Parser,method:FileOffsetReader.loadOffsets,exception:" + e.getMessage() + "]");
						}
					}
				}
			} catch (Exception e) {
				LOG.error(source, "Error reading offset file " + offsetFile.getAbsolutePath() + "; ", e);
				errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:FileOffsetReader.loadOffsets,exception:" + e.getMessage() + "]");
			} finally {
				try {
					if (bReader != null)
						bReader.close();
					if (fReader != null)
						fReader.close();
				} catch (Exception e) {
					LOG.error(source, "Error closing offset file reader for " + offsetFile.getAbsolutePath() + "; ", e);
					errorLogger.write(ErrorCode.FILE_NOT_FOUND,
							"[module:Parser,method:FileOffsetReader.loadOffsets,exception:" + e.getMessage() + "]");
				}
			}
		} else {
			LOG.warn(source, "File offset " + offsetFile.getAbsolutePath() + " was not found. Creating a new one...");
			try {
				offsetFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Check the input directory and update the offsets treeMap.
	 * If the treeMap reference a file that has not been updated for more than a minute, close the reference.
	 * If the treeMap reference a file not existing anymore, close and delete the reference.
	 * If there is a file in the directory which is not present in the treeMap, add the reference.
	 * @return true if the treeMap has been correctly updated, false if the directory is not accessible or not present, or if an error occurred.
	 */
	public boolean syncFileOffsets(List<String> launchedThreadsList) {
		boolean fileChanged = false;
		boolean result = true;
		try {
			File[] dirFiles = inputFilesDir.listFiles();
			if (dirFiles == null) {
				// Directory removed or otherwise inaccessible, no action
				result = false;
			} else {
				// Saving directory in hashMap
				Map<String, Long> fileMap = new HashMap<String, Long>();
				for (int i = 0; i < dirFiles.length; i++) {
					if (checkFile(dirFiles[i].getName()))
						fileMap.put(dirFiles[i].getName(), dirFiles[i].length());
				}
				
				synchronized (fileOffsets) {
					// Check fileOffsets entries
					Iterator<Map.Entry<String, FileOffsetElement>> iterator = fileOffsets.entrySet().iterator();
					while (iterator.hasNext()) {
						Map.Entry<String, FileOffsetElement> foEntry = iterator.next();
						String fileName = foEntry.getKey();

						// Delete from fileOffsets entries without an existing file
						if (!fileMap.containsKey(fileName)) {
							foEntry.getValue().closeFile();
							iterator.remove();
							fileChanged = true;
							for (int i = 0; i < launchedThreadsList.size(); i++) {
								if (launchedThreadsList.get(i).equals(fileName)) {
									LOG.debug(source, "Removing item: " + i + " from launched threads list. Filename removed: " + fileName);
									launchedThreadsList.remove(i);
								}
							}
						}
					}
				}
				// Add an entry in fileOffsets for each file, or update its size if already existing
				for (Map.Entry<String, Long> fileEntry : fileMap.entrySet()) {
					String fileName = fileEntry.getKey();
					Long size = fileEntry.getValue();
					FileOffsetElement foel = fileOffsets.get(fileName);
					if (foel == null) {
						
						if(source.equalsIgnoreCase(ConfigKeys.ADSMART)){
							foel = new FileOffsetElement(LOG.getSlog(), errorLogger, ConfigurationSplitter.getOldAdsmartConfig(), new File(inputFilesDir, fileName), size, 0L);
							LOG.debug(Source.ADSMART, "Loaded offset file " + fileName + " with offset " + 0 + " and size " + size);
						}
						else{
							foel = new FileOffsetElement(LOG.getSlog(), errorLogger, ConfigurationSplitter.getOldEthanConfig(), new File(inputFilesDir, fileName), size, 0L);
							LOG.debug(Source.ETHAN, "Loaded offset file " + fileName + " with offset " + 0 + " and size " + size);
						}
						fileChanged = true;
						
					} else {
						if (foel.bytesToRead() == 0) {
							foel.closeFile();
							for (int i = 0; i < launchedThreadsList.size(); i++) {
								if (launchedThreadsList.get(i).equals(fileName)) {
									LOG.debug(source, "Removing item: " + i + " from launched threads list. Filename removed: " + fileName);
									launchedThreadsList.remove(i);
                                }
							}
						}
						foel.setSize(size);
					}
					fileOffsets.put(fileName, foel);
				}
				result = true;
				
				if (fileChanged) {
					// scrivo su fileOffset tutta la nuova lista dove ho rimosso
					// le righe relative ai file rimossi
					List<String> fileContent = new ArrayList<>();
					synchronized (fileOffsets) {
						for (Map.Entry<String, FileOffsetElement> entry : fileOffsets.entrySet()) {

							FileOffsetElement foel = entry.getValue();
							fileContent.add(entry.getKey() + "|" + foel.getOffset());

						}
					}
					if(fileContent!=null)
						Files.write(offsetFile.toPath(), fileContent, StandardCharsets.UTF_8);
				}
			}
		} catch (Exception e) {
			LOG.error("Error syncing offset file " + offsetFile.getAbsolutePath() + "; ", e);
			errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:FileOffsetReader.syncFileOffsets,exception:" + e.getMessage() + "]");
			result = false;
		}
		return result;
	}

	/**
	 * Check if a file present in the input directory matches the criteria in the configuration file.
	 * if no "input file pattern" is found in config, we assume it is a "pass all" filter.
	 * Otherwise, we expect to find a pattern as a string, a regex, an array of strings, or an array of regexes.
	 * @param filename the file to check.
	 * @return true if the file name matches, false otherwise.
	 */
	private boolean checkFile(String filename) {
		Object pattern = config.getObject(ConfigKeys.FIXED.ADSMART_CFG.INPUT_FILES_PATTERN.ARRAY);
		if (this.source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			pattern = config.getObject(ConfigKeys.FIXED.ETHAN_CFG.INPUT_FILES_PATTERN.ARRAY);
		}

		if (pattern == null) {
			// No pattern parameter found: we assume it is a "pass all" filter
			return true;
		} else if (pattern instanceof String) {
			return checkFilePattern(filename, (String) pattern);
		} else if (pattern instanceof JSONArray) {
			for (int i = 0; i < ((JSONArray) pattern).length(); i++) {
				if (checkFilePattern(filename, (String) ((JSONArray) pattern).get(i)))
					return true;
			}
			return false;
		} else {
			LOG.error("Error reading input file pattern from config: returned object of type " + pattern.getClass().getSimpleName() + ": "
					+ pattern.toString());
			return false;
		}
	}

	/**
	 * Check if a file present in the input directory matches the selection patterns in the configuration.
	 * @param filename the file to check.
	 * @param pattern the pattern that the file has to match.
	 * @return true if the file name matches, false otherwise.
	 */
	private boolean checkFilePattern(String filename, String pattern) {
		// First, check if pattern is a normal filename, and if it is equal to filename
		if (filename != null && pattern != null && filename.contains(pattern)) {
			return true;
		} else {
			// Else, check with regex
			try {
				Pattern regex = Pattern.compile(pattern);
				Matcher matcher = regex.matcher(filename);
				if (matcher.find()) {
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
	}

	/**
	 * Overwrites the offset files with the values currently present in the treemap
	 * @return true if the file is correctly written, false otherwise.
	 */
    public synchronized boolean saveOffsets(String key) {
        FileOffsetElement foel;
        boolean written = false;
        LOG.debug("Saving offsets...");
        List<String> fileContent = null;
        try {
            foel = fileOffsets.get(key);
            fileContent = new ArrayList<>(Files.readAllLines(offsetFile.toPath(), StandardCharsets.UTF_8));
            for (int i = 0; i < fileContent.size(); i++) {
                if (fileContent.get(i).contains(key)) {
                    fileContent.set(i, foel.getFileName() + "|" + foel.getOffset());
                    if (LOG.isDebugEnabled())
                        LOG.debug("Updating offset: file = " + foel.getFileName() + " offset = " + foel.getOffset());
                    written = true;
                    break;
                }
            }

            if (!written) {
                fileContent.add(foel.getFileName() + "|" + foel.getOffset());
                if (LOG.isDebugEnabled())
                    LOG.debug("Saving offset: file = " + foel.getFileName() + " offset = " + foel.getOffset());
            }

            return overwriteBinary(offsetFile.toPath(), fileContent, StandardCharsets.UTF_8);

        } catch (ClosedByInterruptException e) {
            LOG.warn("Interrupted while updating offset file " + offsetFile.getAbsolutePath() + ", KEY: " + key);
            return false;
        } catch (Exception e) {
            LOG.error("Error updating offset file " + offsetFile.getAbsolutePath() + ";", e);
            errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:FileOffsetReader.saveOffsets,exception:" + e.getMessage() + "]");
            return false;
        }
    }

	/**
	 * Sleep some time between a input file reading and another. Update the status of the calling thread, in order to avoid false positives in run thread.
	 * @param statusRunnable the thread checking the status of all application's threads.
	 * @param index the thread's index to update every second.
	 * @throws InterruptedException 
	 */
	public void sleep(StatusRunnable statusRunnable, String index) throws InterruptedException {
		// vecchio valore
		//int sleepTime = (100 - loadFactor) * config.getInteger(ConfigKeys.RUNTIME.OFS_READ_CYCLE_INTERVAL_MSEC) / 100;
		
		int sleepTime = config.getInteger(ConfigKeys.RUNTIME.OFS_READ_CYCLE_INTERVAL_MSEC);
		
		LOG.debug("Thread " + Thread.currentThread().getName() + " sleeping for " + sleepTime + " milliseconds");
		for (int i = 0; i < sleepTime; i = i + 1000) {
			Thread.sleep(Math.min(1000, sleepTime - i));
			statusRunnable.setStatus(index, System.currentTimeMillis());
		}
	}

	/**
	 * Updates the load factor of the module, based on the size and the offset of the input files.
	 * This will affect the sleep time during the thread's inactivity.
	 */
	public void updateLoadFactor() {
		long totalRead = 0;
		long totalSize = 0;
		
		synchronized (fileOffsets) {
			for (Map.Entry<String, FileOffsetElement> foEntry : fileOffsets.entrySet()) {
				FileOffsetElement foel = foEntry.getValue();
				totalRead += foel.getOffset();
				totalSize += foel.getSize();
			}
		}
		
		loadFactor = (int) ((totalSize > 0) ? 100 - ((double) totalRead / totalSize) * 100.0f : 0);
		statisticsRunnable.updateLoadFactor(loadFactor);
	}

	public StatRunnable getStatisticsRunnable() {
		return statisticsRunnable;
	}

	public ErrorLogger getErrorLogger() {
		return errorLogger;
	}

	public void setLastOffsetUpdate(long lastOffsetUpdate) {
		this.lastOffsetUpdate = lastOffsetUpdate;
	}
	
    /**
     * Sovrascrivo un file partendo da zero scrivendolo in binario, metodo molto veloce ma se il vecchio file era più
     * lungo resterà la parte rimanente.
     * @param path
     * @param lines
     * @param cs
     */
    private static boolean overwriteBinary(Path path, List<String> lines, Charset cs) {
        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile(path.toString(), "rw");
            for (String line : lines) {
                file.write((line + "\n").getBytes(cs));
            }
            return true;
        } catch (IOException e) {
            LOG.error("Error writing offset file " + path.toString() + ";", e);
            return false;
        } finally {
            try {
                if (file != null)
                    file.close();
            } catch (IOException e) {
            }
        }
    }
}
