package com.nttdata.admeter.runnable;

import java.util.Date;
import java.util.Map;

import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.couchbase.DButils;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.LOG;

public class CBCampaignsReloaderRunner implements Runnable {

	private ErrorLogger errorLogger;
	private IConfig config;
	private StatusRunnable statusRunnable;
	private int intervalTimeMins = 60;
	private long lastReload = 0;
	private int retryLeft = 3;
	private final long tickTime = 60000;

	public CBCampaignsReloaderRunner(ErrorLogger errorLogger, IConfig config, StatusRunnable statusRunnable) {
		this.config = config;
		this.errorLogger = errorLogger;
		this.statusRunnable = statusRunnable;
		this.intervalTimeMins = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CAMPAIGN_FREQ_READ_MIN);
		this.retryLeft = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CAMPAIGN_RETRY_NUM);
	}

	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {

			try {
				if (isTime()) {
					LOG.info("Triggered campaigns reload from couchbase.");
					DButils.reloadCampaignsCouchbase(this.errorLogger, this.config, ConfigKeys.ADSMART);
					DButils.reloadCampaignsHistoryCouchbase(this.errorLogger, this.config, ConfigKeys.ADSMART);

					DButils.reloadCampaignsCouchbase(this.errorLogger, this.config, ConfigKeys.ETHAN);
					DButils.reloadCampaignsHistoryCouchbase(this.errorLogger, this.config, ConfigKeys.ETHAN);

					if (config.getString(ConfigKeys.RUNTIME.TBL_CAMPAIGN_LOG).equalsIgnoreCase(ConfigKeys.YES)) {
						for (Map.Entry<String, Integer> entry : DButils.getCampaignsCouchbase(ConfigKeys.ADSMART).entrySet()) {
							LOG.info(Source.ADSMART, "CAMPAIGNRELOAD | campaignID: " + entry.getKey() + ", duration: " + entry.getValue());
						}
						for (Map.Entry<String, Integer> entry : DButils.getCampaignsCouchbase(ConfigKeys.ETHAN).entrySet()) {
							LOG.info(Source.ETHAN, "CAMPAIGNRELOAD | campaignID: " + entry.getKey() + ", duration: " + entry.getValue());
						}
					}

					lastReload = new Date().getTime();
					this.retryLeft = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CAMPAIGN_RETRY_NUM);
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error("Error reloading campaigns from couchbase, attempt left: " + (retryLeft - 1));
				errorLogger.write(ErrorCode.GENERIC, "[module:Parser,method:CBCampaignsReloaderRunner,exception:" + e.getMessage() + "]");
				this.retryLeft--;
				if (this.retryLeft == 0)
					statusRunnable.setStatus(Status.INDEX_PARSERMAIN, -1);
			}

			try {
				Thread.sleep(this.tickTime);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

	}

	private boolean isTime() {
		intervalTimeMins = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CAMPAIGN_FREQ_READ_MIN);

		long nowTime = (new Date()).getTime();
		if (Math.abs(lastReload - nowTime) >= (intervalTimeMins))
			return true;

		return false;
	}
}
