package com.nttdata.admeter.runnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.config.check.ConfigurationSplitter;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.output.FileBufferWriter;
import com.nttdata.admeter.output.FileOffsetElement;
import com.nttdata.admeter.utils.FileUtils;
import com.nttdata.admeter.utils.LOG;

/**
 * Father runnable containing a thread pool able to handle 
 * threads that will effectively parse the input XMLs.
 * @author MCortesi
 *
 */
public class ParserRunnableEthanMT implements Runnable {

	private ErrorLogger errorLogger;
	private IConfig config;
	private IConfig oldEthanConfig;
	private StatParserRunnable statisticsRunnable;
	private StatusRunnable statusRunnable;
	private FileOffsetReaderMT ofsReader;
	private FileBufferWriter fbw;
	private int numThreads;
	protected static List<String> workingThreadsKeys = Collections.synchronizedList(new ArrayList<String>());
	private ExecutorService executor;

	public ParserRunnableEthanMT(ErrorLogger errorLogger,
			IConfig config, StatParserRunnable statisticsRunnable,
			StatusRunnable statusRunnable) {
		super();
		this.errorLogger = errorLogger;
		this.config = config;
		this.oldEthanConfig = ConfigurationSplitter.getOldEthanConfig();
		this.statisticsRunnable = statisticsRunnable;
		this.statusRunnable = statusRunnable;
		ofsReader = new FileOffsetReaderMT(config, errorLogger, statisticsRunnable, ConfigKeys.ETHAN);
		fbw = new FileBufferWriter(LOG.getSlog(), errorLogger,
				oldEthanConfig, statusRunnable, statisticsRunnable,
				config.getString(ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_EVENTS_FILE_DIR),
				FileUtils.getFileName(config.getString(ConfigKeys.FIXED.ETHAN_CFG.OUTPUT_EVENTS_FILE_NAME), oldEthanConfig),
				true, Parameter.OUTPUT_TS_FORMAT);
		numThreads = config.getInteger(ConfigKeys.FIXED.ETHAN_CFG.PARALLEL_THREADS_NUM);
	}

	@Override
	public void run() {
		executor = Executors.newFixedThreadPool(numThreads);
		Map<String, FileOffsetElement> ofsTree = ofsReader.getFileOffsets();
		while (!isStopped()) {
			ofsReader.syncFileOffsets(workingThreadsKeys);
			
			synchronized (ofsTree) {
				for (Map.Entry<String, FileOffsetElement> entry : ofsTree.entrySet()) {
					//Checking if hashmap already contains the tree entry
					if ((!workingThreadsKeys.contains(entry.getKey())) && (entry.getValue().bytesToRead() > 0)) {
						Runnable worker = new WorkerThreadEthanMT(errorLogger, config, statisticsRunnable, statusRunnable, ofsReader, fbw, entry.getKey(), entry.getValue());
						executor.execute(worker);
						workingThreadsKeys.add(entry.getKey());
					}
				}
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ofsReader.updateLoadFactor();
			try {
				ofsReader.sleep(statusRunnable, Status.INDEX_MESSAGEPARSER + "-ethan");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean isStopped() {
		return Thread.currentThread().isInterrupted();
	}
	
	public void stopThreads(){

		executor.shutdownNow();
		while (!executor.isTerminated())
			;
		LOG.debug(Source.ETHAN, "all parser threads stopped");
		
		if(fbw != null){
			LOG.debug(Source.ETHAN, "Stopping output file writer.");
        	fbw.flush();
        	fbw.terminate();	
    	}
    }
}
