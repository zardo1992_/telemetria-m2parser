package com.nttdata.admeter.runnable;

import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constant.StatusTimestamp;
import com.nttdata.admeter.couchbase.Database;

/**
 * A runnable class used to write the status of this module.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class CouchbaseConnectionRunnable implements Runnable {

	private StatusRunnable statusRunnable;
	private int couchbaseMonitoriInterval = 5000;

	public CouchbaseConnectionRunnable(StatusRunnable statusRunnable) {
		this.statusRunnable = statusRunnable;
		statusRunnable.setStatus(Status.INDEX_CONTEXTLISTENER_FIXED, new StatusTimestamp(System.currentTimeMillis(), couchbaseMonitoriInterval));
	}

	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {

			if (Database.isConnectionOK()) {
				statusRunnable.setStatus(Status.INDEX_CONTEXTLISTENER_FIXED, System.currentTimeMillis());
			} else {
				statusRunnable.setStatus(Status.INDEX_CONTEXTLISTENER_FIXED, -1);
			}

			try {
				Thread.sleep(couchbaseMonitoriInterval);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

	}
}