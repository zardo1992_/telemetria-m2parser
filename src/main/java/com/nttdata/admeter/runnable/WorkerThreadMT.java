package com.nttdata.admeter.runnable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constants.M2Statistics;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.output.FileBufferWriter;
import com.nttdata.admeter.output.FileOffsetElement;
import com.nttdata.admeter.output.OutputMessage;
import com.nttdata.admeter.parser.SAXEventParser;
import com.nttdata.admeter.parser.SAXParserWrapper;
import com.nttdata.admeter.utils.LOG;

/**
 * Single runnable able to parse a single file from output of ModuleCollector
 * @author MCortesi
 *
 */
public class WorkerThreadMT implements Runnable {

	private ErrorLogger errorLogger;
	private IConfig config;
	private StatParserRunnable statisticsRunnable;
	private StatusRunnable statusRunnable;
	private FileOffsetReaderMT ofsReader;
	private FileBufferWriter fbw;
	private String key;
	private FileOffsetElement foel;


	public WorkerThreadMT(ErrorLogger errorLogger,
			IConfig config, StatParserRunnable statisticsRunnable, StatusRunnable statusRunnable, FileOffsetReaderMT ofsReader, FileBufferWriter fbw,
			String key, FileOffsetElement foel) {
		super();
		this.errorLogger = errorLogger;
		this.config = config;
		this.statisticsRunnable = statisticsRunnable;
		this.statusRunnable = statusRunnable;
		this.ofsReader = ofsReader;
		this.fbw = fbw;
		this.key = key;
		this.foel = foel;
	}

	private String extractStringAtPosition(String str, int position) {

		List<String> fieldsList = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(str, "|");
		while (tokenizer.hasMoreTokens()) {
			String stringTmp = (String) tokenizer.nextToken();
			fieldsList.add(stringTmp);
		}
		return fieldsList.get(position);
	}

	@Override
	public void run() {

        LOG.debug(Source.ADSMART, "Started parsing: " + key);
        byte[] lineToParse = null;
		
		try {
			File fileSchema = new File(
					config.getString(ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_DIR) + config
							.getString(ConfigKeys.FIXED.ADSMART_CFG.XML_SCHEMA_FILE_NAME));

			SAXParserWrapper parser = new SAXParserWrapper();
			parser.setSchemaFile(fileSchema);

			lineToParse = getNextMessage();
			while (lineToParse != null && !isStopped()) {
				LOG.debug(Source.ADSMART, "lineToParse: " + new String(lineToParse));
				OutputMessage msg = new OutputMessage(lineToParse, LOG.getSlog(), errorLogger, com.nttdata.admeter.constant.ErrorCode.Source.ADSMART);
				try {
					Long collectorTime = Long.parseLong(msg.getTimestamp());
					String decoded = new String(lineToParse, "UTF-8");
					String xmlString = extractStringAtPosition(decoded, 5);
					
					if (parser.isValidXMLString(xmlString)) {
						SAXEventParser eventParser = new SAXEventParser(ConfigKeys.ADSMART, errorLogger, config, fbw,
								statisticsRunnable,
								statusRunnable, Long.parseLong(extractStringAtPosition(decoded, 4)));

						Long parserTime = System.currentTimeMillis();
						statisticsRunnable.handleDelayProcessingMillis(collectorTime, parserTime, ConfigKeys.ADSMART);
						parser.parse(xmlString, eventParser);
						LOG.info(Source.ADSMART, "SmartcardID: " + eventParser.getCardId() + " | Valid XML parsed. CollectorTime: " + collectorTime + ", ParserTime: " + parserTime);
						statisticsRunnable.incrementInt(M2Statistics.MESSAGES_PROCESSED_ADSMART);
					} else {
						LOG.error(Source.ADSMART, "Invalid XML!");
						errorLogger.write(ConfigKeys.ADSMART, ErrorCode.XML_NOT_VALID,
								"[instance: "
										+ config.getString(
												ConfigKeys.FIXED.INSTANCE_NAME)
										+ ", input line discarded: " + decoded
										+ "]");
						statisticsRunnable.incrementInt(M2Statistics.MESSAGES_DISCARDED_ADSMART);
					}
				} catch (NumberFormatException e) {
					LOG.info(Source.ADSMART, "Exception thrown: " + e);
					statisticsRunnable.incrementInt(M2Statistics.MESSAGES_DISCARDED_ADSMART);
				}
				
				// Salvo il progesso nell'offset, se fallisco ci riprovo
                for(int i=0; i<1000; i++){
                    if(ofsReader.saveOffsets(key))
                        break;
                    try {
                        LOG.warn("Error writing offset, waiting to retry.");
                        Thread.sleep(200);
                    } catch (Exception e) {
                    }
                }
                
                if(isStopped())
                    break;
                
                lineToParse = getNextMessage();
                
                if (lineToParse == null) {
                    foel.closeFile();
                }
			}

		} catch (Exception e) {
			LOG.error(Source.ADSMART, "Exit from thread, releasing file: "+ key, e);
			ParserRunnableAdSmartMT.workingThreadsKeys.remove(key);
			foel.putBackMessage(lineToParse);
		}
	}

	private boolean isStopped() {
		return Thread.currentThread().isInterrupted();
	}

	private byte[] getNextMessage() {
		byte[] message = null;
		try {
			while (message == null) {
				if (foel.bytesToRead() > 0) {
					LOG.debug(Source.ADSMART, "Getting new message. File name: " + foel.getFileName() + ", size: " + foel.getSize() + " bytes to read: "
							+ foel.bytesToRead());
					message = foel.read();
					if (message != null) {
						statisticsRunnable.incrementInt(M2Statistics.BUFFER_READ_ADSMART);
					}
					//						lastOffsetUpdate = fileOffset.lastOffsetUpdate();
				} else if (foel.bytesToRead() < 0) { // Should never happen
					foel.setOffset(foel.getSize());
				} else if (foel.bytesToRead() == 0) {
					break;
				}
			}
			//			updateLoadFactor();
		} catch (Exception e) {
			LOG.error(Source.ADSMART, "Error getting new buffer: " + e.getMessage());
			errorLogger.write(ErrorCode.BUFFER_READ, "[module:Parser,method:FileOffsetReader.getNextBuffer,exception:" + e.getMessage() + "]");
		}
		return message;
	}

	public String getKey() {
		return key;
	}
}
