package com.nttdata.admeter.runnable;

import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;

import com.nttdata.admeter.channel.ChannelBean;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.couchbase.DButils;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.LOG;

public class CBChannelsReloaderRunner implements Runnable {

	private ErrorLogger errorLogger;
	private IConfig config;
	private StatusRunnable statusRunnable;
	private JSONArray jsonTimes = new JSONArray();
	private int retryLeft = 3;
	private final long tickTime = 30000;
	private long nextTickTime = 30000;

	public CBChannelsReloaderRunner(ErrorLogger errorLogger, IConfig config, StatusRunnable statusRunnable) {
		this.config = config;
		this.errorLogger = errorLogger;
		this.statusRunnable = statusRunnable;
		this.jsonTimes = this.config.getArray(ConfigKeys.RUNTIME.TBL_CHANNEL_TIME_READ.ARRAY);
		this.retryLeft = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CAMPAIGN_RETRY_NUM);
	}

	@Override
	public void run() {

		while (!Thread.currentThread().isInterrupted()) {

			try {
				if (isTime()) {
					//                    logger.debug("--TRUE-- nextTickTime: " + nextTickTime / 1000.0);
					LOG.info("Triggered channels reload from couchbase.");
					DButils.reloadChannelsCouchbase(this.errorLogger, this.config, ConfigKeys.ADSMART);
					DButils.reloadChannelsCouchbase(this.errorLogger, this.config, ConfigKeys.ETHAN);

					if (config.getString(ConfigKeys.RUNTIME.TBL_CHANNEL_LOG).equalsIgnoreCase(ConfigKeys.YES)) {
						for (ChannelBean entry : DButils.getChannelsCouchbase(ConfigKeys.ADSMART)) {
							LOG.info(Source.ADSMART, "CHANNELRELOAD | serviceKey: " + entry.getServiceKey() + " DVB:[" + entry.getOriginalNetworkID() + ","
									+ entry.getTransportStreamID() + "," + entry.getSiServiceID() + "]");
						}
						for (ChannelBean entry : DButils.getChannelsCouchbase(ConfigKeys.ETHAN)) {
							LOG.info(Source.ETHAN, "CHANNELRELOAD | serviceKey: " + entry.getServiceKey() + " DVB:[" + entry.getOriginalNetworkID() + ","
									+ entry.getTransportStreamID() + "," + entry.getSiServiceID() + "]");
						}
					}

					this.retryLeft = this.config.getInteger(ConfigKeys.RUNTIME.TBL_CHANNEL_RETRY_NUM);
				} else {
					//                    logger.debug("--FALSE-- nextTickTime: " + nextTickTime / 1000.0);
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error("Error reloading channels from couchbase, attempt left: " + (retryLeft - 1));
				errorLogger.write(ErrorCode.GENERIC, "[module:Parser,method:CBChannelsReloaderRunner,exception:" + e.getMessage() + "]");
				this.retryLeft--;
				if (this.retryLeft == 0)
					statusRunnable.setStatus(Status.INDEX_PARSERMAIN, -1);
			}

			try {
				Thread.sleep(this.nextTickTime);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

	}

	/**
	 * Tells if it is time to reload data, also calculate the next tick, it will be <= tickTime
	 * 
	 * @return
	 */
	private boolean isTime() {

		long nowTime = (new Date()).getTime();
		long nextReloadTime = Long.MAX_VALUE; // to generate nextTickTime
		long closerReloadTime = Long.MAX_VALUE; // to generate return value

		//        logger.debug("Now time    : " + TimeUtils.long2String(nowTime, null));

		// This is an array with day time to fire reload with format "HH:mm" "18:15"
		jsonTimes = this.config.getArray(ConfigKeys.RUNTIME.TBL_CHANNEL_TIME_READ.ARRAY);

		// I lock for the nextReloadTime and closerReloadTime in the config times list
		for (int i = 0; i < jsonTimes.length(); i++) {

			long auxTime = time2long(jsonTimes.getString(i));
			//            logger.debug("Reload time : " + TimeUtils.long2String(auxTime, null));

			// if this time is in the future and before current reload time, I declare it as next reload time
			if (auxTime >= nowTime && auxTime < nextReloadTime)
				nextReloadTime = auxTime;

			// I also lock for the nearest reload time to know if it is "now"
			if (Math.abs(nowTime - auxTime) < Math.abs(nowTime - closerReloadTime))
				closerReloadTime = auxTime;
		}

		//        logger.debug("nextReloadTime  : " + (nextReloadTime - nowTime) / 1000.0);
		//        logger.debug("closerReloadTime: " + (closerReloadTime - nowTime) / 1000.0);

		// I calculate the nextTickTime as the lower value between the time to the next tick and to the next reload
		// time
		if (tickTime <= (nextReloadTime - nowTime))
			nextTickTime = tickTime;
		else
			nextTickTime = (nextReloadTime - nowTime);

		// if the closer reload time is very "close" to now it is time to reload the data
		if (Math.abs(nowTime - closerReloadTime) < 1000)
			return true;
		else
			return false;
	}

	/**
	 * Convert a time format HH:mm in long (milliseconds) of today
	 * 
	 * @param time
	 *        to convert with format HH:mm
	 * @return time in milliseconds
	 */
	private long time2long(String time) {

		// I take hours and minutes from current time
		String[] timeSplit = time.trim().split(":");
		int ora = Integer.parseInt(timeSplit[0]);
		int minuti = Integer.parseInt(timeSplit[1]);

		// I calculate current fire time in millis
		Calendar fireTime = Calendar.getInstance();
		fireTime.setTime(new Date());
		fireTime.set(Calendar.HOUR_OF_DAY, ora);
		fireTime.set(Calendar.MINUTE, minuti);
		fireTime.set(Calendar.SECOND, 0);
		fireTime.set(Calendar.MILLISECOND, 0);
		return fireTime.getTimeInMillis();
	}
}
