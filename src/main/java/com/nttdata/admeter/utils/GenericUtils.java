package com.nttdata.admeter.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GenericUtils {

	/**
	 * Computes the MD5 hash value on the passed string
	 * @param toHash
	 * 			the string that has to be hashed
	 * @return the hashed string
	 */
	public static String computeMD5Hash(String toHash) {
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Handle exception
			e.printStackTrace();
		}
		m.update(toHash.getBytes(),0,toHash.length());
		BigInteger hashValue = new BigInteger(1, m.digest());
		return hashValue.toString(16);
	}
}
