package com.nttdata.admeter.utils;

import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;


@Plugin(name="SourcePatternConverter", category="Converter")
@ConverterKeys({"source"})
public class SourcePatternConverter extends LogEventPatternConverter {

	protected SourcePatternConverter(String name, String style) {
		super(name, style);
	}

	
	public static SourcePatternConverter newInstance(final String[] options) {
        return new SourcePatternConverter("source", "source");
    }

    @Override
    public void format(LogEvent event, StringBuilder toAppendTo) {
        Marker marker = event.getMarker();
        if (marker != null) {
            // MarkerPatternConverter appends Marker.toString()
            // which includes the parents of the marker.  We just
            // want the marker's name
            toAppendTo.append(marker.getName());
        }
    }

}
