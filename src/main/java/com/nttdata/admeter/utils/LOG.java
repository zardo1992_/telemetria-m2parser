package com.nttdata.admeter.utils;

import java.util.Locale;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class LOG {

	static final private Logger sLog = LogManager.getLogger("ModuleParser");

	static public boolean isDebugEnabled() {
		return sLog.isDebugEnabled();
	}
	
	static public void info(String message) {
		log(Level.INFO, null, message, null);
	}

	static public void info(String source, String message) {
		log(Level.INFO, source, message, null);
	}

	static public void debug(String message) {
		log(Level.DEBUG, null, message, null);
	}

	static public void debug(String source, String message) {
		log(Level.DEBUG, source, message, null);
	}

	static public void error(String message) {
		log(Level.ERROR, null, message, null);
	}
	
	static public void error(String message, Throwable error) {
		log(Level.ERROR, null, message, error);
	}

	static public void error(String source, String message) {
		log(Level.ERROR, source, message, null);
	}
	
	static public void error(String source, String message, Throwable error) {
		log(Level.ERROR, source, message, error);
	}

	static public void warn(String message) {
		log(Level.WARN, null, message, null);
	}

	static public void warn(String source, String message) {
		log(Level.WARN, source, message, null);
	}

	static public void trace(String message) {
		log(Level.TRACE, null, message, null);
	}

	static public void trace(String source, String message) {
		log(Level.TRACE, source, message, null);
	}

	static private void log(Level level, String source, String message, Throwable t) {
		//String msg = (message != null) ? 
		//		String.format(Locale.getDefault(), "[%s] %s", getDefaultTag(), message) : null;
		
		Marker marker = (source != null) ? MarkerManager.getMarker(source) : 
			MarkerManager.getMarker("COMMON");
		if (marker != null)
			sLog.log(level, marker, message, t);
		else
			sLog.log(level, MarkerManager.getMarker("COMMON"), message, t);
	}

	public static Logger getSlog() {
		return sLog;
	}
	
	 /**
     * Returns a string representing the default tag. The value of the tag will be the name of the
     * class from there the log was called.
     *
     * @return the default tag
     */
    static
    private String getDefaultTag() {
        try {
            StackTraceElement[] stackTrace = new Throwable().getStackTrace();
            String className = stackTrace[3].getClassName();

            return className.substring(className.lastIndexOf('.') + 1);
        }  catch (Exception e) {
            return LOG.class.getName();
        }
    }
    
    static
    private  String format(String message, Object[] args) {
        if (message == null) {
            return "null";
        } else {
            return String.format(Locale.getDefault(), message, args);
        }
    }

    private LOG() {  }

}
