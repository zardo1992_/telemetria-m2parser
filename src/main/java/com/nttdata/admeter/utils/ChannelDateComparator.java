package com.nttdata.admeter.utils;

import java.util.Comparator;

import com.nttdata.admeter.channel.ChannelBean;

public class ChannelDateComparator implements Comparator<ChannelBean> {

	@Override
	public int compare(ChannelBean o1, ChannelBean o2) {
		return Long.compare(o2.getToDate(), o1.getToDate());
	}
}
