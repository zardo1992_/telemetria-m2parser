package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;

public class EvStandbyOutBean extends EventBean {

	public EvStandbyOutBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}
}
