package com.nttdata.admeter.event.input;

import org.json.JSONArray;
import org.json.JSONObject;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.parser.interfaces.IPrinter;

/**
 * Container of specific informations for events of type evAdSmartNoSubstitution. 
 * Implementing {@link com.nttdata.admeter.parser.interfaces.IPrinter} interface, the class supplies a specific
 * implementation for method {@link com.nttdata.admeter.parser.interfaces.IPrinter#printEvent()}
 * used to print this output event on intermediate file.
 * @author MCortesi
 *
 */
public class EvAdSmartNoSubsBean extends EventBean implements IPrinter, IChannelable {

	public static final String INPUT_EVENT_NAME = "evAdSmartNoSubstitution";
	public static final String OUTPUT_EVENT_NAME = "NOSUBS";
	private String availID;
	private String reason;
	private String originalNetworkID = "NA";
	private String transportStreamID = "NA";
	private String siServiceID = "NA";
	private String serviceKey = "NA";
	private String chType;

	public EvAdSmartNoSubsBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}

	public String getAvailID() {
		return availID;
	}

	public void setAvailID(String availID) {
		this.availID = availID;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}
	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}
	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}
	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}
	@Override
	public String getSiServiceID() {
		return siServiceID;
	}
	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}

	@Override 
	public String getServiceKey() {
		return serviceKey;
	}
	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	@Override
	public String getChType() {
		return chType;
	}
	@Override
	public void setChType(String chType) {
		this.chType = chType;
	}
	
	@Override
	public String toString() {
		return super.toString() + "originalNetworkID: " + originalNetworkID
				+ " transportStreamID: " + transportStreamID
				+ " siServiceID: " + siServiceID
				+ " serviceKey: " + serviceKey
				+ " availID: " + availID
				+ " reason: " + reason
				+ " channelType: " + chType;
	}
	
	@Override
	public JSONObject toJSONObject() {
		JSONObject eventObj = new JSONObject();
		
		eventObj.put("ts", getRefDate());
		eventObj.put("event", OUTPUT_EVENT_NAME);
		
		/* user_info */
		eventObj.put("user_info", printUserInfo());
		
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		JSONArray tdvb = new JSONArray();
	    
		eventInfoObj.put("rep_id", getReportID());
		
		tdvb.put(originalNetworkID);
		tdvb.put(transportStreamID);
		tdvb.put(siServiceID);
		
		eventInfoObj.put("tdvb", tdvb);
		
		eventInfoObj.put("skey", serviceKey);
	
		eventInfoObj.put("slot_id", availID);
		eventInfoObj.put("reason", reason);
		eventObj.put("event_info", eventInfoObj);
		
		return eventObj;
	}
}
