package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;

/**
 * Container of specific informations for events of type evVODPlayback 
 * @author MCortesi
 *
 */
public class EvVODPlaybackBean extends EventBean {

	public static final String EVENT_NAME = "evVODPlayback";
	private String vodAssetID;
	private int startOffset;
	private int playbackSpeed;

	public EvVODPlaybackBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}
	
	public String getVodAssetID() {
		return vodAssetID;
	}

	public void setVodAssetID(String vodAssetID) {
		this.vodAssetID = vodAssetID;
	}

	public int getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(int startOffset) {
		this.startOffset = startOffset;
	}

	public int getPlaybackSpeed() {
		return playbackSpeed;
	}

	public void setPlaybackSpeed(int playbackSpeed) {
		this.playbackSpeed = playbackSpeed;
	}
	
	@Override
	public String toString() {
		return super.toString() + "VODAssetID: " + vodAssetID
				+ " startOffset: " + startOffset
				+ " playbackSpeed: " + playbackSpeed;
	}
}
