package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * Container of specific informations for events of type evChangeView
 * @author MCortesi
 *
 */
public class EvChangeViewBean extends EventBean implements IChannelable {

	public static final String EVENT_NAME = "evChangeView";
	private String recordedTime;
	private Integer playbackSpeed;
	private String originalNetworkID="NA";
	private String transportStreamID="NA";
	private String siServiceID="NA";
	private String chType;
	private String serviceKey="NA";
	private String tunerID;

	public EvChangeViewBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}

	public String getRecordedTime() {
		return recordedTime;
	}

	public void setRecordedTime(String recordedTime) {
		if (!recordedTime.equals("NA")) {
			this.recordedTime = TimeUtils.UTC2LocalTime(recordedTime, "CET", getErrorLogger());
		} else {
			this.recordedTime = recordedTime;
		}
	}

	public Integer getPlaybackSpeed() {
		return playbackSpeed;
	}

	public void setPlaybackSpeed(Integer playbackSpeed) {
		this.playbackSpeed = playbackSpeed;
	}
	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}
	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}
	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}
	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}
	@Override
	public String getSiServiceID() {
		return siServiceID;
	}
	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}
	@Override
	public String getServiceKey() {
		return serviceKey;
	}
	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	@Override
	public String getChType() {
		return chType;
	}
	@Override
	public void setChType(String chType) {
		this.chType = chType;
	}
	
	public String getTunerID() {
		return tunerID;
	}
	
	public void setTunerID(String tunerID) {
		this.tunerID = tunerID;
	}
	
	@Override
	public boolean isLive() {
		return ((getPlaybackSpeed() == null) && 
				(getRecordedTime() == null));
	}
	
	@Override
	public String toString() {
		return super.toString() + "originalNetworkID: " + originalNetworkID
				+ " transportStreamID: " + transportStreamID
				+ " siServiceID: " + siServiceID
				+ " serviceKey: " + serviceKey
				+ " recordedTime: " + recordedTime
				+ " playbackSpeed: " + playbackSpeed
				+ " channelType: " + chType;
	}
}
