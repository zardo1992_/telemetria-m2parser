package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;

public class EvStandbyInBean extends EventBean {

	public EvStandbyInBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}
}
