package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;

/**
 * Container of specific informations for events of type evAdSmart 
 * @author MCortesi
 *
 */
public class EvAdSmartBean extends EventBean implements IChannelable {
	public static final String EVENT_NAME = "evAdSmart";
	
	private String campaignID;
	private String contentInstanceID;
	private String availId;
	private int startOffset;
	private String originalNetworkID="NA";
	private String transportStreamID="NA";
	private String siServiceID="NA";
	private long duration;
	private String serviceKey="NA";
	private String chType;
	private boolean adExists = false;

	public EvAdSmartBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}

	public String getCampaignID() {
		return campaignID;
	}

	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

	public String getContentInstanceID() {
		return contentInstanceID;
	}

	public void setContentInstanceID(String contentInstanceID) {
		this.contentInstanceID = contentInstanceID;
	}

	public String getAvailId() {
		return availId;
	}

	public void setAvailId(String availId) {
		this.availId = availId;
	}

	public int getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(int startOffset) {
		this.startOffset = startOffset;
	}

	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}

	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}

	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}

	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}

	@Override
	public String getSiServiceID() {
		return siServiceID;
	}

	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}

	@Override
	public String getServiceKey() {
		return serviceKey;
	}

	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	
	public String getChType() {
		return chType;
	}
	
	public void setChType(String chType) {
		this.chType = chType;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	public boolean isAdExists() {
		return adExists;
	}
	
	public void setAdExists(boolean adExists) {
		this.adExists = adExists;
	}
	
	@Override
	public String toString() {
		return super.toString() + "duration: " + duration
				+ "originalNetworkID: " + originalNetworkID
				+ " transportStreamID: " + transportStreamID
				+ " siServiceID: " + siServiceID
				+ " serviceKey: " + serviceKey
				+ " campaignID: " + campaignID
				+ " contentInstanceID: " + contentInstanceID
				+ " availID: " + availId
				+ " startOffset: " + startOffset
				+ " channelType: " + chType;
	}
}
