package com.nttdata.admeter.event.input;

import org.json.JSONArray;
import org.json.JSONObject;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.parser.interfaces.IPrinter;

public class EvTunerActivityBean extends EventBean implements IPrinter, IChannelable {

	public static final String INPUT_EVENT_NAME = "evTunerActivity";
	public static final String OUTPUT_EVENT_NAME = "TUNER";
	private String serviceKey			= "NA";
	private String originalNetworkID 	= "NA";
	private String transportStreamID 	= "NA";
	private String siServiceID			= "NA";
	private String chType;
	private String tunerID;

	public EvTunerActivityBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}
	@Override
	public String getServiceKey() {
		return serviceKey;
	}
	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}
	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}
	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}
	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}
	
	@Override
	public String getSiServiceID() {
		return siServiceID;
	}
	
	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}
	
	public String getTunerID() {
		return tunerID;
	}
	
	public void setTunerID(String tunerID) {
		this.tunerID = tunerID;
	}
	
	@Override
	public JSONObject toJSONObject() {
	
		JSONObject eventObj = new JSONObject();
		
		eventObj.put("ts", getRefDate());
		eventObj.put("event", OUTPUT_EVENT_NAME);
		
		/* user_info */
		eventObj.put("user_info", printUserInfo());
		
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		JSONArray tdvb = new JSONArray();
	    
		eventInfoObj.put("rep_id", getReportID());
		
		tdvb.put(originalNetworkID);
		tdvb.put(transportStreamID);
		tdvb.put(siServiceID);
		
		eventInfoObj.put("tdvb", tdvb);
		
		eventInfoObj.put("skey", serviceKey);
	
		eventInfoObj.put("tuner_id", tunerID);
		eventInfoObj.put("ch_type", getChType());
		eventObj.put("event_info", eventInfoObj);
		
		return eventObj;
	}
	
	@Override
	public String getChType() {
		return chType;
	}
	@Override
	public void setChType(String chType) {
		this.chType = chType;
	}
}
