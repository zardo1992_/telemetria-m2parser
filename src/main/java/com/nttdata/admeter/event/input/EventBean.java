package com.nttdata.admeter.event.input;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.fsm.FiniteStateMachine.State;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.OutputUtils;

/**
 * Container object of the event deserialized from the input file.
 * @author MCortesi
 *
 */

public class EventBean {

	final
	private String	source;
	
	private ErrorLogger errorLogger;
	private IConfig config;
	
	//TODO: quando sapremo il valore di stbSerialNumber togli NA
	private String stbSerialNumber = "NA";
	private String refDate;
	/**
	 * Contains the hashed smartcard value
	 */
	private String cardID;
	private String subscriberID;
	private String deviceID;
	private String deviceType;
	

	private String panelID;
	private String xmlDate;
	private String logDate;
	private String reportID;
	
	private long obsTime;

	private int counter;

	public EventBean(ErrorLogger errorLogger, IConfig config, String source) {
		this.errorLogger = errorLogger;
		this.config = config;
		this.source = source;
	}

	public String getSource() { return this.source; }

	
	public String getRefDate() {
		return refDate;
	}

	public void setRefDate(String refDate) {
		this.refDate = refDate;
	}

	public String getDeviceType() { return deviceType; }
	public void setDeviceType(String value) { deviceType = value; }
	
	public String getCardID() {
		return cardID;
	}

	public void setCardID(String cardID) {
		this.cardID = cardID;
	}

	public String getSubscriberID() {
		return subscriberID;
	}

	public void setSubscriberID(String subscriberID) {
		this.subscriberID = subscriberID;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String value) {
		this.deviceID = value;
	}
	
	public String getPanelID() {
		return panelID;
	}

	public void setPanelID(String panelID) {
		this.panelID = panelID;
	}

	public String getStbSerialNumber() {
		return stbSerialNumber;
	}

	public void setStbSerialNumber(String stbSerialNumber) {
		this.stbSerialNumber = stbSerialNumber;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getXmlDate() {
		return xmlDate;
	}

	public void setXmlDate(String xmlDate) {
		this.xmlDate = xmlDate;
	}

	public ErrorLogger getErrorLogger() {
		return errorLogger;
	}

	public void setErrorLogger(ErrorLogger errorLogger) {
		this.errorLogger = errorLogger;
	}

	public IConfig getConfig() {
		return config;
	}

	public void setConfig(IConfig config) {
		this.config = config;
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	public long getObsTime() {
		return obsTime;
	}
	
	public void setObsTime(long obsTime) {
		this.obsTime = obsTime;
	}

	@Override
	public String toString() {
		return "refDate: " + refDate + " cardID: " + cardID + " subscriberID: "
				+ subscriberID + " panelID: " + panelID;
	}

	/**
	 * Puts user informations in a JSONObject, so that they returns useful for sons Events.
	 * @return the JSONObject containing user informations
	 */
	public JSONObject printUserInfo() {
		JSONObject userInfoObj = new JSONObject();

		//		userInfoObj.put("stb_sn", stbSerialNumber);
		userInfoObj.put("sc_sn", cardID);
		userInfoObj.put("subs_id", subscriberID);
		userInfoObj.put("panel_id", panelID);
		if (!OutputUtils.isEmpty(deviceID))
			userInfoObj.put("device_id", deviceID);
		if (!OutputUtils.isEmpty(deviceType))
			userInfoObj.put("device_type", deviceType);
			

		return userInfoObj;
	}

	/**
	 * Adds the current State of finite state machine and payloads with speed equals to 0
	 * to the JSONObject that will be written on CouchbaseDB
	 * 
	 * @param eventToWriteObj
	 * 			the JSONObject that will be written
	 * @param viewChToWrite
	 * 
	 * @param printedPayload
	 */
	public JSONObject addMissingParameters(JSONObject eventToWriteObj, List<EventPayloadBean> printedPayload, State state) {
		JSONArray payloadArray = eventToWriteObj.getJSONArray("payload");

		//Putting into the json to write the current state of finite state machine
		eventToWriteObj.put("state", state);

		//Clearing the already existing payload array
		payloadArray = new JSONArray();

		//Repopulating payload array also with payloads with speed equals to 0
		for (int i = 0; i < printedPayload.size(); i++) {
			JSONArray payloadToAdd = new JSONArray();
			payloadToAdd.put(printedPayload.get(i).getStartTs());
			payloadToAdd.put(printedPayload.get(i).getEndTs());
			payloadToAdd.put(String.valueOf(printedPayload.get(i).getxSpeed()));
			payloadToAdd.put(String.valueOf(printedPayload.get(i).getDuration()));
			payloadToAdd.put(printedPayload.get(i).getActionStart());
			payloadToAdd.put(printedPayload.get(i).getActionEnd());
			payloadArray.put(payloadToAdd);
		}

		eventToWriteObj.put("payload", payloadArray);
		return eventToWriteObj;
	}

	/**
	 * Reads common informations from the couchbase document content
	 * and sets the proper attributes
	 * @param content
	 * 			the couchbase document content in json format
	 */
	public void readFromJson(JsonObject content) {
		refDate = content.getString("ts");

		// Setting common User Informations
		JsonObject userInfoObj = content.getObject("user_info");
		stbSerialNumber = userInfoObj.getString("stb_sn");
		panelID = userInfoObj.getString("panel_id");
		cardID = userInfoObj.getString("sc_sn");
		subscriberID = userInfoObj.getString("subs_id");
		if (userInfoObj.containsKey("device_id")) {
			deviceID = userInfoObj.getString("device_id");
		}
		if (userInfoObj.containsKey("device_type")) {
			deviceType = userInfoObj.getString("device_type");
		}
	}

	public boolean isLive() {
		return false;
	}
	
	/**
	 * Reads payloads from the couchbase document content
	 * and populates a new refreshed payload list with the retrieved
	 * informations
	 * @param content
	 * 			the couchbase document content in json format
	 * @return the populated refreshed payload list
	 */
	public List<EventPayloadBean> populatePayload(JsonObject content) {
		// Setting payloads
		JsonArray payloadListArray = content.getArray("payload");
		JsonArray singlePayloadArray = null;
		EventPayloadBean payloadBean;
		List<EventPayloadBean> payloadList = new ArrayList<>();

		for (int i = 0; i < payloadListArray.size(); i++) {

			payloadBean = new EventPayloadBean();
			singlePayloadArray = payloadListArray.getArray(i);
			payloadBean.setStartTs(singlePayloadArray.getString(0));
			payloadBean.setEndTs(singlePayloadArray.getString(1));

			payloadBean.setxSpeed(Integer.parseInt(singlePayloadArray.getString(2)));
			payloadBean.setDuration(Long.parseLong(singlePayloadArray.getString(3)));
			payloadBean.setActionStart(singlePayloadArray.getString(4));
			payloadBean.setActionEnd(singlePayloadArray.getString(5));
			payloadList.add(payloadBean);
		}

		return payloadList;
	}
}
