package com.nttdata.admeter.event.input;

import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;

public class EvSurfBean extends EventBean {

	public EvSurfBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}
}
