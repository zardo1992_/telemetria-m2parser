package com.nttdata.admeter.event.output;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.parser.interfaces.IPrinter;

/**
 * Container of informations of output event of type VIEWCH.
 * Implementing {@link com.nttdata.admeter.parser.interfaces.IPrinter} interface, the class supplies a specific
 * implementation for method {@link com.nttdata.admeter.parser.interfaces.IPrinter#printEvent()}
 * used to print this output event on intermediate file.
 * The class also take advantage of heredity from EventBean superclass supplying an override implementation
 * of method {@link com.nttdata.admeter.event.input.EventBean#readFromJson(JsonObject)}, used to retrieve specific
 * informations regarding only this output event from document saved in CouchBase, and method
 * {@link com.nttdata.admeter.event.input.EventBean#addMissingParameters(JSONObject, List, State)}, used to add
 * specific parameters of this event to the document that has to be written on CouchBase.
 * @author MCortesi
 *
 */
public class ViewChEventBean extends EventBean implements IPrinter, IChannelable {

	public static final String EVENT_NAME = "VIEWCH";
	private String reportID;
	private String chType;
	private String originalNetworkID = "NA";
	private String transportStreamID = "NA";
	private String siServiceID = "NA";
	private String serviceKey = "NA";
	private long totalViewing;
	private String pbflg;
	private String recTime;
	private String tunerID;
	private List<EventPayloadBean> payload;
	
	public ViewChEventBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
	
	public String getChType() {
		return chType;
	}

	public void setChType(String chType) {
		this.chType = chType;
	}

	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}

	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}

	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}

	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}

	@Override
	public String getSiServiceID() {
		return siServiceID;
	}

	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}

	@Override
	public String getServiceKey() {
		return serviceKey;
	}

	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public long getTotalViewing() {
		return totalViewing;
	}

	public void setTotalViewing(long totalViewing) {
		this.totalViewing = totalViewing;
	}

	public String getPbflg() {
		return pbflg;
	}

	public void setPbflg(String pbflg) {
		this.pbflg = pbflg;
	}

	public String getRecTime() {
		return recTime;
	}

	public void setRecTime(String recTime) {
		this.recTime = recTime;
	}
	
	public String getTunerID() {
		return tunerID;
	}
	
	public void setTunerID(String tunerID) {
		this.tunerID = tunerID;
	}

	public List<EventPayloadBean> getPayload() {
		return payload;
	}

	public void setPayload(List<EventPayloadBean> payload) {
		this.payload = payload;
	}
	
	@Override
	public JSONObject toJSONObject() {
		
		JSONObject eventObj = new JSONObject();
		
		/* header */
		eventObj.put("ts", getRefDate());
		eventObj.put("obstime", String.valueOf(super.getObsTime()));
		eventObj.put("event", EVENT_NAME);
		
		/* user_info */
		eventObj.put("user_info", printUserInfo());
		
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		JSONArray tdvb = new JSONArray();
	    
		eventInfoObj.put("rep_id", getReportID());
		eventInfoObj.put("ch_type", chType);
		
		tdvb.put(originalNetworkID);
		tdvb.put(transportStreamID);
		tdvb.put(siServiceID);

		eventInfoObj.put("tdvb", tdvb);
		eventInfoObj.put("skey", serviceKey);	
		eventInfoObj.put("tot_view", String.valueOf(totalViewing));
		eventInfoObj.put("pbflg", pbflg);
		eventInfoObj.put("tuner_id" , tunerID);
		
		if (recTime != null && !recTime.equals("NA")) {
			eventInfoObj.put("rec_time", recTime);
		}
		eventObj.put("event_info", eventInfoObj);
		
		/* payload */
		JSONArray payloadArray = new JSONArray();
		for (int i = 0; i < payload.size(); i++) {
			if (payload.get(i).getxSpeed() != 0) {
				payloadArray.put(payload.get(i).printPayload());
			}	
		}
		eventObj.put("payload", payloadArray);
	
		return eventObj;
	}
	
	@Override
	public void readFromJson(JsonObject content) {

		super.readFromJson(content);
		super.setObsTime(Long.parseLong(content.getString("obstime")));
				
		// Setting Event Informations
		JsonObject eventInfoObj = content.getObject("event_info");
		recTime = eventInfoObj.getString("rec_time");
		reportID = eventInfoObj.getString("rep_id");
		chType = eventInfoObj.getString("ch_type");
		pbflg = eventInfoObj.getString("pbflg");
		totalViewing = Long.parseLong(eventInfoObj.getString("tot_view"));
		tunerID = eventInfoObj.getString("tuner_id");
		JsonArray tdvb = eventInfoObj.getArray("tdvb");
		
		originalNetworkID = tdvb.getString(0);
		transportStreamID = tdvb.getString(1);
		siServiceID = tdvb.getString(2);
		
		serviceKey = eventInfoObj.getString("skey");
		
		payload = super.populatePayload(content);
	}
}
