package com.nttdata.admeter.event.output;

import org.json.JSONObject;

import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.output.OutputMessage;
import com.nttdata.admeter.parser.interfaces.IPrinter;
import com.nttdata.admeter.utils.TimeUtils;

public class EventPrinterHelper<T extends EventBean & IPrinter> {

	private T	event;
	
	public EventPrinterHelper(T event) {
		this.event = event;
	}
	
	public OutputMessage createOutputMessage(String tag) {
		JSONObject eventObj = event.toJSONObject();
		
		OutputMessage message = null;
		
		if (event.getSource().equalsIgnoreCase(ConfigKeys.ADSMART)) {
			switch (event.getConfig().getString(ConfigKeys.FIXED.ADSMART_CFG.HEADER_IDENTIFIER)) {
				case "SUBSCRIBER":
					message = getOutputMessage(tag, eventObj, event.getSubscriberID(), 
							(event.getConfig().getString(ConfigKeys.FIXED.ADSMART_CFG.TIMESTAMP_HEADER_TYPE).equalsIgnoreCase("MSG")));
					break;
					
				case "SMARTCARD":
					message = getOutputMessage(tag, eventObj, event.getCardID(), 
							(event.getConfig().getString(ConfigKeys.FIXED.ADSMART_CFG.TIMESTAMP_HEADER_TYPE).equalsIgnoreCase("MSG")));
					break;
				
				case "STB_SN":
					//TODO: stb_sn data missing for now
					break;
					
				default:
					break;
			}
		}
		
		else {
			switch (event.getConfig().getString(ConfigKeys.FIXED.ETHAN_CFG.HEADER_IDENTIFIER)) {
				case "SUBSCRIBER":
					message = getOutputMessage(tag, eventObj, event.getSubscriberID(), 
							(event.getConfig().getString(ConfigKeys.FIXED.ETHAN_CFG.TIMESTAMP_HEADER_TYPE).equalsIgnoreCase("MSG")));
					break;
					
				case "SMARTCARD":
					message = getOutputMessage(tag, eventObj, event.getCardID(), 
							(event.getConfig().getString(ConfigKeys.FIXED.ETHAN_CFG.TIMESTAMP_HEADER_TYPE).equalsIgnoreCase("MSG")));
					break;
				
				case "STB_SN":
					//TODO: stb_sn data missing for now
					break;
					
				case "DEVICEID":
					message = getOutputMessage(tag, eventObj, event.getDeviceID(), 
							(event.getConfig().getString(ConfigKeys.FIXED.ETHAN_CFG.TIMESTAMP_HEADER_TYPE).equalsIgnoreCase("MSG")));
					break;
					
				default:
					break;
			}
		}
		
		return message;
	}
	
	private OutputMessage getOutputMessage(String eventTag, JSONObject eventObj, String identifier, boolean msg) {
		 OutputMessage message = new OutputMessage(
				identifier == null ? "" : identifier, 
				eventTag, 
				(msg ? 
					TimeUtils.truncateToMinute(TimeUtils.UTCstring2long(event.getRefDate(), event.getErrorLogger())) : 
					System.currentTimeMillis()), 
				event.getObsTime(), 
				eventObj.toString(), 
				event.getErrorLogger());
		 
		 message.setJson(eventObj);
		 
		 return message;
	}
}
