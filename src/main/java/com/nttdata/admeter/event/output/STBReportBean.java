package com.nttdata.admeter.event.output;


import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.payload.STBPayloadBean;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IPrinter;

/**
 * Container of informations of STBReport event that has to be written
 * every time a subscriber tag is closed.
 * @author MCortesi
 *
 */
public class STBReportBean extends EventBean implements IPrinter{
	
	public static final String EVENT_NAME = "STBREP";
	
	private String ts;
	
	/**
	 * Date when the collector received data and wrote it to the file.
	 * It is the same present in the private header.
	 */
	private String receivedDate;
	
	/**
	 * Time in [ms] needed to process a subscriber inside XML.
	 */
	private long processingTime;
	
	/**
	 * List of input event occurences datas:
	 * [eventName, minDate, maxDate, occurrencesNumber]
	 */
	private List<STBPayloadBean> payloadIn;
	
	/**
	 * List of output event occurences datas:
	 * [eventName, occurencesNumber]
	 */
	private List<STBPayloadBean> payloadOut;
	
	public STBReportBean(ErrorLogger errorlogger, IConfig config, String source) {
		super(errorlogger, config, source);
	}


	public String getTs() {
		return ts;
	}
	
	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(long processingTime) {
		this.processingTime = processingTime;
	}
	
	public List<STBPayloadBean> getPayloadIn() {
		return payloadIn;
	}
	
	public void setPayloadIn(List<STBPayloadBean> payloadIn) {
		this.payloadIn = payloadIn;
	}
	
	public List<STBPayloadBean> getPayloadOut() {
		return payloadOut;
	}
	
	public void setPayloadOut(List<STBPayloadBean> payloadOut) {
		this.payloadOut = payloadOut;
	}
	
	@Override
	public JSONObject toJSONObject() {
		
		JSONObject evObj = new JSONObject();
		evObj.put("ts", ts);
		evObj.put("event", "STBREP");
		
		/* user_info */
		evObj.put("user_info", printUserInfo());
		
		JSONObject eventInfoObj = new JSONObject();
		eventInfoObj.put("rep_id", getReportID()); //MD5
		eventInfoObj.put("log_date", getLogDate());
		eventInfoObj.put("xml_date", getXmlDate());
		eventInfoObj.put("received_date", receivedDate);
		eventInfoObj.put("proc_time", String.valueOf(processingTime));
		evObj.put("event_info", eventInfoObj);
		
		JSONArray payloadInArray = new JSONArray();
		for (int i = 0; i < payloadIn.size(); i++) {
			JSONArray payloadInElementArray = new JSONArray();
			payloadInElementArray.put(payloadIn.get(i).getEventName());
			payloadInElementArray.put(payloadIn.get(i).getMinDate());
			payloadInElementArray.put(payloadIn.get(i).getMaxDate());
			payloadInElementArray.put(String.valueOf(payloadIn.get(i).getCounter()));
			payloadInArray.put(payloadInElementArray);
		}
		
		evObj.put("payload_in", payloadInArray);
		
		JSONArray payloadOutArray = new JSONArray();
		
		//Payload out has not to be printed when there are no output events.
		if (payloadOut.size() > 0) {
			for (int i = 0; i < payloadOut.size(); i++) {
				JSONArray payloadOutElementArray = new JSONArray();
				payloadOutElementArray.put(payloadOut.get(i).getEventName());
				payloadOutElementArray.put(String.valueOf(payloadOut.get(i).getCounter()));
				payloadOutArray.put(payloadOutElementArray);
			}
		evObj.put("payload_out", payloadOutArray);
		}
		
		return evObj;
	}	
}
