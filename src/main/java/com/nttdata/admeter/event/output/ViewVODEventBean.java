package com.nttdata.admeter.event.output;


import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IPrinter;

/**
 * Container of informations of output event of type VIEWVOD.
 * used to print this output event on intermediate file.
 * The class also take advantage of heredity from EventBean superclass supplying an override implementation
 * of method {@link com.nttdata.admeter.event.input.EventBean#readFromJson(JsonObject)}, used to retrieve specific
 * informations regarding only this output event from document saved in CouchBase.
 * @author MCortesi
 *
 */
public class ViewVODEventBean extends EventBean implements IPrinter{

	public static final String EVENT_NAME = "VIEWVOD";
	private String reportID;
	private String vodID;
	private long totalViewing;
	private int offset;
	private List<EventPayloadBean> payload;
	
	public ViewVODEventBean(ErrorLogger errorLogger, IConfig config,String source) {
		super(errorLogger, config, source);
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}

	public String getVodID() {
		return vodID;
	}

	public void setVodID(String vodID) {
		this.vodID = vodID;
	}

	public long getTotalViewing() {
		return totalViewing;
	}

	public void setTotalViewing(long totalViewing) {
		this.totalViewing = totalViewing;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public List<EventPayloadBean> getPayload() {
		return payload;
	}
	
	public void setPayload(List<EventPayloadBean> payload) {
		this.payload = payload;
	}
	
	
	@Override
	public JSONObject toJSONObject() {
		
		JSONObject eventObj = new JSONObject();
		
		eventObj.put("ts", getRefDate());
		eventObj.put("obstime", String.valueOf(super.getObsTime()));
		eventObj.put("event", EVENT_NAME);
		
		/* user_info */
		eventObj.put("user_info", printUserInfo());
		
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		eventInfoObj.put("rep_id", getReportID());
		eventInfoObj.put("vod_id", vodID);
		eventInfoObj.put("tot_view", String.valueOf(totalViewing));
		eventInfoObj.put("offset", String.valueOf(offset));
		
		eventObj.put("event_info", eventInfoObj);
		
		/* payload */
		JSONArray payloadArray = new JSONArray();
		for (int i = 0; i < payload.size(); i++) {
			if (payload.get(i).getxSpeed() != 0) {
				payloadArray.put(payload.get(i).printPayload());
			}
		}
		
		eventObj.put("payload", payloadArray);
		
		return eventObj;
	}
	
	@Override
	public void readFromJson(JsonObject content) {
		
		super.readFromJson(content);
		super.setObsTime(Long.parseLong(content.getString("obstime")));		
		// Setting Event Informations
		JsonObject eventInfoObj = content.getObject("event_info");
		reportID = eventInfoObj.getString("rep_id");
		vodID = eventInfoObj.getString("vod_id");
		totalViewing = Long.parseLong(eventInfoObj.getString("tot_view"));
		offset = Integer.parseInt(eventInfoObj.getString("offset"));
		
		payload = super.populatePayload(content);
	}
}
