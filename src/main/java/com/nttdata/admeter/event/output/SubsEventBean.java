package com.nttdata.admeter.event.output;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.fsm.FiniteStateMachine.State;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.parser.interfaces.IPrinter;

/**
 * Container of informations of output event of type SUBS.
 * Implementing {@link com.nttdata.admeter.parser.interfaces.IPrinter} interface, the class supplies a specific
 * implementation for method {@link com.nttdata.admeter.parser.interfaces.IPrinter#printEvent()}
 * used to print this output event on intermediate file.
 * The class also take advantage of heredity from EventBean superclass supplying an override implementation
 * of method {@link com.nttdata.admeter.event.input.EventBean#readFromJson(JsonObject)}, used to retrieve specific
 * informations regarding only this output event from document saved in CouchBase, and method
 * {@link com.nttdata.admeter.event.input.EventBean#addMissingParameters(JSONObject, List, State)}, used to add
 * specific parameters of this event to the document that has to be written on CouchBase.
 * @author MCortesi
 *
 */
public class SubsEventBean extends EventBean implements IPrinter, IChannelable  {

	public static final String EVENT_NAME = "SUBS";

	/**
	 * Unique identifier of the report obtained with MD5 hash of the string
	 * "subs_id|xml_date|log_date". It is generated the first time a new subscriber
	 * tag is opened and it will be used for all the other events of that subscriber
	 * with the same subs_id, xml_date and log_date
	 */
	private String reportID;

	/**
	 * DVB triplet of input event which generate this output event. If it is directly
	 * specified in XML it can be written from that, else it will be obtained
	 * externally as a result of research method {@link com.nttdata.admeter.parser.SAXEventParser#retrieveDVB}
	 */
	private String originalNetworkID = "NA";
	private String transportStreamID = "NA";
	private String siServiceID = "NA";
	private String chType;
	private long adOffset; // offset dello spot
	private long naturalEnd;

	private String serviceKey = "NA";

	/**
	 * Campaign identifier took from XML
	 */
	private String cmpID;

	/**
	 * Total duration of advertising substitution slot
	 */
	private long cmpDur;

	/**
	 * Total duration of vision, obtained as a sum of all {@link com.nttdata.admeter.event.payload.EventPayloadBean#getDuration()}
	 * of payload list
	 */
	private long cmpView;

	/**
	 * Event contentInstanceID
	 */
	private String cmpUpID;

	/**
	 * Event availID
	 */
	private String slotID;

	/**
	 * Playback flag: 'Y' -> recTime OK, bflg OK, offset OK,
	 * eventualmente 0. 'N' -> recTime missing, bflg missing, offset missing.
	 */
	private String pbflg;

	/**
	 * Recorded date time in format "yyyy-MM-ddTHH:mm:ss±[hhmm]"
	 */
	private String recTime;

	/**
	 * Beginning flag: startOffset = 0 -> 'Y' startOffset != 0 -> 'N'
	 */
	private String bflg;

	/**
	 * Event startOffset
	 */
	private long startOffset;

	/**
	 * Event payload list containing: 
	 * - TimestampStart: EventTime attribute of generator event in format "yyyy-MM-ddTHH:mm:SS";
	 * - TimestampEnd: EventTime attribute of the closing event in format "yyyy-MM-ddTHH:mm:SS";
	 * - XSpeed: vision speed, negative in the case of rewind;
	 * - Duration: difference between Timestamp End and Timestamp Start in [ms];
	 * - action_code_start: code representing the opening action which generated the vision slot;
	 * - action_code_end: code representing the closing action which ended the vision slot.
	 */
	private List<EventPayloadBean> payload;
	
	private String tunerID;

	public SubsEventBean(ErrorLogger errorLogger, IConfig config, String source) {
		super(errorLogger, config, source);
		adOffset = 0;
		naturalEnd = 0;
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}

	@Override
	public String getOriginalNetworkID() {
		return originalNetworkID;
	}

	@Override
	public void setOriginalNetworkID(String originalNetworkID) {
		this.originalNetworkID = originalNetworkID;
	}

	@Override
	public String getTransportStreamID() {
		return transportStreamID;
	}

	@Override
	public void setTransportStreamID(String transportStreamID) {
		this.transportStreamID = transportStreamID;
	}

	@Override
	public String getSiServiceID() {
		return siServiceID;
	}

	@Override
	public void setSiServiceID(String siServiceID) {
		this.siServiceID = siServiceID;
	}

	@Override
	public String getServiceKey() {
		return serviceKey;
	}

	@Override
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getCmpID() {
		return cmpID;
	}

	public void setCmpID(String cmpID) {
		this.cmpID = cmpID;
	}

	public long getCmpDur() {
		return cmpDur;
	}

	public void setCmpDur(long cmpDur) {
		this.cmpDur = cmpDur;
	}

	public long getCmpView() {
		return cmpView;
	}

	public void setCmpView(long cmpView) {
		this.cmpView = cmpView;
	}

	public String getCmpUpID() {
		return cmpUpID;
	}

	public void setCmpUpID(String cmpUpID) {
		this.cmpUpID = cmpUpID;
	}

	public String getSlotID() {
		return slotID;
	}

	public void setSlotID(String slotID) {
		this.slotID = slotID;
	}

	public String getPbflg() {
		return pbflg;
	}

	public void setPbflg(String pbflg) {
		this.pbflg = pbflg;
	}

	public String getRecTime() {
		return recTime;
	}

	public void setRecTime(String recTime) {
		this.recTime = recTime;
	}

	public String getBflg() {
		return bflg;
	}

	public void setBflg(String bflg) {
		this.bflg = bflg;
	}

	public long getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(long startOffset) {
		this.startOffset = startOffset;
	}

	public List<EventPayloadBean> getPayload() {
		return payload;
	}

	public void setPayload(List<EventPayloadBean> payload) {
		this.payload = payload;
	}

	public String getChType() {
		return chType;
	}

	public void setChType(String chType) {
		this.chType = chType;
	}

	public long getAdOffset()
	{
		return adOffset;
	}

	public void setAdOffset(long adOffset)
	{
		this.adOffset = adOffset;
	}

	public long getNaturalEnd()
	{
		return naturalEnd;
	}

	public void setNaturalEnd(long naturalEnd)
	{
		this.naturalEnd = naturalEnd;
	}
	
	public String getTunerID() {
		return tunerID;
	}
	
	public void setTunerID(String tunerID) {
		this.tunerID = tunerID;
	}

	@Override
	public JSONObject toJSONObject() {
		
		JSONObject eventObj = new JSONObject();

		eventObj.put("ts", getRefDate());
		eventObj.put("obstime", String.valueOf(super.getObsTime()));
		eventObj.put("event", EVENT_NAME);
		
		/* user_info */
		eventObj.put("user_info", printUserInfo());
	
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		JSONArray tdvb = new JSONArray();

		eventInfoObj.put("rep_id", getReportID());
		
		tdvb.put(originalNetworkID);
		tdvb.put(transportStreamID);
		tdvb.put(siServiceID);
		
		eventInfoObj.put("tdvb", tdvb);
		
		eventInfoObj.put("skey", String.valueOf(serviceKey));
		
		eventInfoObj.put("cmp_id", cmpID);
		eventInfoObj.put("cmp_dur", String.valueOf(cmpDur*1000));
		eventInfoObj.put("cmp_view", String.valueOf(cmpView));
		eventInfoObj.put("cmp_upid", cmpUpID);
		eventInfoObj.put("slot_id", slotID);
		eventInfoObj.put("pbflg", pbflg);
		eventInfoObj.put("bflg", bflg);
		
		if (recTime!=null && !recTime.equals("NA")) {
			eventInfoObj.put("rec_time", recTime);	
		}
		
		eventInfoObj.put("offset", String.valueOf(startOffset));
		
		eventObj.put("event_info", eventInfoObj);

		/* payload */
		JSONArray payloadArray = new JSONArray();
		for (int i = 0; i < payload.size(); i++) {
			if (payload.get(i).getxSpeed() != 0) {
				payloadArray.put(payload.get(i).printPayload());
			}
		}

		eventObj.put("payload", payloadArray);
		
		return eventObj;
	}
	
	@Override
	public void readFromJson(JsonObject content) {
		
		super.readFromJson(content);
		super.setObsTime(Long.parseLong(content.getString("obstime")));
		
		// Setting Event Informations
		JsonObject eventInfoObj = content.getObject("event_info");
		reportID = eventInfoObj.getString("rep_id");
		JsonArray tdvb = eventInfoObj.getArray("tdvb");
		
		originalNetworkID = tdvb.getString(0);
		transportStreamID = tdvb.getString(1);
		siServiceID = tdvb.getString(2);
		serviceKey = eventInfoObj.getString("skey");
		
		cmpID = eventInfoObj.getString("cmp_id");
		cmpDur = Long.parseLong(eventInfoObj.getString("cmp_dur"))/1000 ;
		cmpView = Long.parseLong(eventInfoObj.getString("cmp_view"));
		cmpUpID = eventInfoObj.getString("cmp_upid");
		slotID = eventInfoObj.getString("slot_id");
		pbflg = eventInfoObj.getString("pbflg");
		recTime = eventInfoObj.getString("rec_time");
		bflg = eventInfoObj.getString("bflg");
		startOffset = Long.parseLong(eventInfoObj.getString("offset"));
				
		payload = super.populatePayload(content);
				
		naturalEnd = Long.parseLong(content.getString("naturalEnd"));
		adOffset = Long.parseLong(content.getString("adOffset"));
	}
	
	@Override
	public JSONObject addMissingParameters(JSONObject eventToWriteObj,
			List<EventPayloadBean> printedPayload, State state) {
		JSONObject tempObj = super.addMissingParameters(eventToWriteObj, printedPayload, state);
		tempObj.put("naturalEnd", String.valueOf(naturalEnd));
		tempObj.put("adOffset", String.valueOf(adOffset));
		return tempObj;
	}
}
