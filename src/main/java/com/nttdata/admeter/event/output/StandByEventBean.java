package com.nttdata.admeter.event.output;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.parser.interfaces.IPrinter;

public class StandByEventBean extends EventBean implements IPrinter {
	
	public static final String EVENT_NAME = "STANDBY";
	
	private String reportID;
	private List<EventPayloadBean> payload;
	
	public StandByEventBean(ErrorLogger errorLogger, IConfig config,
			String source) {
		super(errorLogger, config, source);
		payload = new ArrayList<>();
	}
	
	public String getReportID() {
		return reportID;
	}
	
	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
	
	public List<EventPayloadBean> getPayload() {
		return payload;
	}

	public void addPayload(EventPayloadBean value) {
		this.payload.add(value);
	}


	@Override
	public JSONObject toJSONObject() {
		JSONObject eventObj = new JSONObject();

		eventObj.put("ts", getRefDate());
		eventObj.put("obstime", String.valueOf(super.getObsTime()));
		eventObj.put("event", EVENT_NAME);
		
		/* user_info */
		JSONObject userInfo = printUserInfo();
		eventObj.put("user_info", userInfo);
		
		/* event_info */
		JSONObject eventInfoObj = new JSONObject();
		eventInfoObj.put("rep_id", getReportID());
		eventObj.put("event_info", eventInfoObj);
	
		/* payload */
		JSONArray payloadArray = new JSONArray();
		for (int i = 0; i < payload.size(); i++) {
			if (payload.get(i).getxSpeed() != 0) {
				payloadArray.put(payload.get(i).printPayload());
			}
		}

		eventObj.put("payload", payloadArray);
		
		return eventObj;
	}
	
	@Override
	public void readFromJson(JsonObject content) {
		super.readFromJson(content);
		
		JsonObject eventInfoObj = content.getObject("event_info");
		reportID = eventInfoObj.getString("rep_id");
		
		payload = super.populatePayload(content);
	}

}
