package com.nttdata.admeter.event.payload;

import org.json.JSONArray;

/**
 * Container of event payload informations
 * @author MCortesi
 *
 */
public class EventPayloadBean {

	private String startTs;
	private String endTs;
	private int xSpeed;
	private long duration;
	private String actionStart;
	private String actionEnd;

	public EventPayloadBean() {
		// TODO Auto-generated constructor stub
	}

	public EventPayloadBean(String startTs, String endTs, int xSpeed,
			long duration, String actionStart, String actionEnd) {
		super();
		this.startTs = startTs;
		this.endTs = endTs;
		this.xSpeed = xSpeed;
		this.duration = duration;
		this.actionStart = actionStart;
		this.actionEnd = actionEnd;
	}

	public String getStartTs() {
		return startTs;
	}

	public void setStartTs(String startTs) {
		this.startTs = startTs;
	}

	public String getEndTs() {
		return endTs;
	}

	public void setEndTs(String endTs) {
		this.endTs = endTs;
	}

	public int getxSpeed() {
		return xSpeed;
	}

	public void setxSpeed(int xSpeed) {
		this.xSpeed = xSpeed;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getActionStart() {
		return actionStart;
	}

	public void setActionStart(String actionStart) {
		this.actionStart = actionStart;
	}

	public String getActionEnd() {
		return actionEnd;
	}

	public void setActionEnd(String actionEnd) {
		this.actionEnd = actionEnd;
	}
	
	public JSONArray printPayload(){
		JSONArray payloadElementArray = new JSONArray();

		payloadElementArray.put(startTs);
		payloadElementArray.put(endTs);
		payloadElementArray.put((xSpeed == Integer.MAX_VALUE) ? "NA" :String.valueOf(xSpeed));
		payloadElementArray.put(String.valueOf(duration));
		payloadElementArray.put(actionStart);
		payloadElementArray.put(actionEnd);
		
		return payloadElementArray;
	}

}
