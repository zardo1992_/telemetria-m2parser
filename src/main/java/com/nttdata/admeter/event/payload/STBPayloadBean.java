package com.nttdata.admeter.event.payload;

/**
 * Container of STB event payload informations
 * @author MCortesi
 *
 */
public class STBPayloadBean {

	private String eventName;
	private String minDate;
	private String maxDate;
	private int counter;
	
	public STBPayloadBean(String eventName, String minDate, String maxDate,
			int counter) {
		super();
		this.eventName = eventName;
		this.minDate = minDate;
		this.maxDate = maxDate;
		this.counter = counter;
	}

	public STBPayloadBean(String eventName, int counter) {
		super();
		this.eventName = eventName;
		this.counter = counter;
	}
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getMinDate() {
		return minDate;
	}

	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
}
