package com.nttdata.admeter.couchbase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.view.Stale;
import com.couchbase.client.java.view.ViewQuery;
import com.couchbase.client.java.view.ViewResult;
import com.couchbase.client.java.view.ViewRow;
import com.nttdata.admeter.channel.ChannelBean;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.JsonUtils;
import com.nttdata.admeter.utils.LOG;
import com.nttdata.admeter.utils.TimeUtils;

public class DButils {

	// Used to store campaigns and channels cache from file and couchbase
	private static Map<String, Integer> campaignsFileAdsmart = new HashMap<String, Integer>();
	private static Map<String, Integer> campaignsCouchbaseAdsmart = new HashMap<String, Integer>();
	private static List<ChannelBean> channelsFileAdsmart = new ArrayList<ChannelBean>();
	private static List<ChannelBean> channelsCouchbaseAdsmart = new ArrayList<ChannelBean>();

	private static Map<String, Integer> campaignsFileEthan = new HashMap<String, Integer>();
	private static Map<String, Integer> campaignsCouchbaseEthan = new HashMap<String, Integer>();
	private static List<ChannelBean> channelsFileEthan = new ArrayList<ChannelBean>();
	private static List<ChannelBean> channelsCouchbaseEthan = new ArrayList<ChannelBean>();

	static synchronized 
	public void reloadCampaignsFileSafe(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {
		Throwable cause = null;
		for (int i = 0; i < 3; i++) {
			try {
				reloadCampaignsFile(errorLogger, config, source);
				return;
				
			} catch (Exception e) {
				cause = e;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ignore) {}
			}
		}
		
		if (cause != null)
			throw new IOException(cause.getMessage(), cause);
	}
	
	/**
	* Reload campaigns from file (path in config) and loads in a specific list.
	* 
	* @param logger
	* @param errorLogger
	* @param config contains the campaign file path
	* @throws Exception
	*/
	public static synchronized void reloadCampaignsFile(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {

		LOG.info(source, "Start DButils.reloadCampaignsFile");

		String fileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_DIR)
				+ config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			fileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_DIR)
					+ config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
		}

		Map<String, Integer> campaignsFileTemp = new HashMap<String, Integer>();

		JSONObject jsonFileObject = JsonUtils.getJson(fileName);
		JSONArray jsonCampaignsList = jsonFileObject.getJSONArray("campaign_list");

		for (int i = 0; i < jsonCampaignsList.length(); i++) {
			JSONObject jsonCampaignObject = (JSONObject) jsonCampaignsList.get(i);

			String campaignId = jsonCampaignObject.getString("campaign_id");
			int duration = Integer.parseInt(jsonCampaignObject.getString("duration"));

			campaignsFileTemp.put(campaignId, duration);
		}

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			campaignsFileAdsmart = campaignsFileTemp;
		else
			campaignsFileEthan = campaignsFileTemp;

		LOG.info(source, "End DButils.reloadCampaignsFile, #campaigns: " + campaignsFileTemp.size());
	}

	static synchronized 
	public void reloadChannelsFileSafe(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {
		Throwable cause = null;
		for (int i = 0; i < 3; i++) {
			try {
				reloadChannelsFile(errorLogger, config, source);
				return;
				
			} catch (Exception e) {
				cause = e;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ignore) {}
			}
		}
		
		if (cause != null)
			throw new IOException(cause.getMessage(), cause);
	}
	
	/**
	* Reload channels from file (path in config) and loads in a specific list.
	* 
	* @param logger
	* @param errorLogger
	* @param config config contains the channels file path
	* @throws Exception
	*/
	public static synchronized void reloadChannelsFile(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {

		LOG.info(source, "Start DButils.reloadChannelsFile");

		String fileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_DIR)
				+ config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CHANNEL_FILE_NAME);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			fileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_DIR)
					+ config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CHANNEL_FILE_NAME);
		}

		List<ChannelBean> channelsFileTemp = new ArrayList<ChannelBean>();

		JSONObject jsonFileObject = JsonUtils.getJson(fileName);
		JSONArray jsonChannelsList = jsonFileObject.getJSONArray("channel_list");

		for (int i = 0; i < jsonChannelsList.length(); i++) {
			JSONObject jsonChannelObject = (JSONObject) jsonChannelsList.get(i);
			ChannelBean newChannel = new ChannelBean();

			String skey = jsonChannelObject.getString("skey");
			String name = jsonChannelObject.getString("name");
			String sIServiceID = jsonChannelObject.getJSONArray("tdvb").getString(2);
			String originalNetworkID = jsonChannelObject.getJSONArray("tdvb").getString(0);
			String transportStreamID = jsonChannelObject.getJSONArray("tdvb").getString(1);
			String type = jsonChannelObject.getString("type");
			String definition = jsonChannelObject.getString("definition");
			long fromLong = TimeUtils.UTCstring2long(jsonChannelObject.getString("from"), errorLogger);
			long toLong = TimeUtils.UTCstring2long(jsonChannelObject.getString("to"), errorLogger);

			// I load all the values in the channel
			newChannel.setServiceKey(skey);
			newChannel.setName(name);
			newChannel.setSiServiceID(sIServiceID);
			newChannel.setOriginalNetworkID(originalNetworkID);
			newChannel.setTransportStreamID(transportStreamID);
			newChannel.setType(type);
			newChannel.setDefinition(definition);
			newChannel.setFromDate(fromLong);
			newChannel.setToDate(toLong);

			channelsFileTemp.add(newChannel);
		}

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			channelsFileAdsmart = channelsFileTemp;
		else
			channelsFileEthan = channelsFileTemp;

		LOG.info(source, "End DButils.reloadChannelsFile, #channels: " + channelsFileTemp.size());

	}

	/**
	* Reload campaigns from couchbase (view name and DD in config) and loads in a specific list.
	* 
	* @param logger
	* @param errorLogger
	* @param config
	* @throws Exception
	*/
	public static synchronized void reloadCampaignsCouchbase(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {

		LOG.info(source, "Start DButils.reloadCampaignsCouchbase");

		Map<String, Integer> campaignsListTemp = new HashMap<String, Integer>();

		String bucketChannel_activeView_name = config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketChannel_activeView_name = config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW);
		}

		String bucketChannel_activeView_dd = config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW_DD);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketChannel_activeView_dd = config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.ALL_CAMPAIGNS_VIEW_DD);
		}

		ViewQuery myView = ViewQuery.from(bucketChannel_activeView_dd, bucketChannel_activeView_name);
		myView.stale(Stale.FALSE); // If not set to false uses a fast (but not
		// updated) data version

		ViewResult result = Database.getBucket(source, Database.Type.Campaign).query(myView, 30, TimeUnit.SECONDS);
		for (ViewRow row : result) {
			JsonObject jsonCampaignObject = row.document().content().getObject("element");

			String campaignId = jsonCampaignObject.getString("campaignId");
			int duration = Integer.parseInt(jsonCampaignObject.getString("duration"));

			campaignsListTemp.put(campaignId, duration);
		}

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			campaignsCouchbaseAdsmart = campaignsListTemp;
		else
			campaignsCouchbaseEthan = campaignsListTemp;

		LOG.info(source, "End DButils.reloadCampaignsCouchbase, #campaigns: " + campaignsListTemp.size());

	}

	/**
	* Reload campaigns from couchbase (view name and DD in config) and loads in a specific list.
	* 
	* @param logger
	* @param errorLogger
	* @param config
	* @throws Exception
	*/
	public static synchronized void reloadCampaignsHistoryCouchbase(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {

		LOG.info(source, "Start DButils.reloadCampaignsHistoryCouchbase");

		Map<String, Integer> campaignsHistoryListTemp = new HashMap<String, Integer>();

		String bucketCampaignHistory_activeView_name = config
				.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketCampaignHistory_activeView_name = config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW);
		}

		String bucketCampaignHistory_activeView_dd = config
				.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW_DD);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketCampaignHistory_activeView_dd = config
					.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.ALL_CAMPAIGNS_HISTORY_VIEW_DD);
		}

		ViewQuery myView = ViewQuery.from(bucketCampaignHistory_activeView_dd, bucketCampaignHistory_activeView_name);
		myView.stale(Stale.FALSE); // If not set to false uses a fast (but not
		// updated) data version

		ViewResult result = Database.getBucket(source, Database.Type.CampaignHistory).query(myView, 30, TimeUnit.SECONDS);
		List<CampaignBean> campaignsToAddList = new ArrayList<>();

		for (ViewRow row : result) {

			CampaignBean campaign;
			JsonObject jsonCampaignObject = row.document().content().getObject("element");

			String campaignId = jsonCampaignObject.getString("campaignId");
			int duration = Integer.parseInt(jsonCampaignObject.getString("duration"));
			String lastUpdateString = row.document().content().getString("lastUpdate");
			long lastUpdateTs = TimeUtils.UTCstring2long(lastUpdateString, errorLogger);

			campaign = new CampaignBean(campaignId, duration, lastUpdateTs);

			boolean keyfound = source.equalsIgnoreCase(ConfigKeys.ADSMART) ? campaignsCouchbaseAdsmart.containsKey(campaignId)
					: campaignsCouchbaseEthan.containsKey(campaignId);
			if (keyfound == false) {
				LOG.debug(source, "Campaign history reloaded time difference: " + (System.currentTimeMillis() - lastUpdateTs));
				if (System.currentTimeMillis() - lastUpdateTs <= config.getLong(ConfigKeys.RUNTIME.TBL_CAMPAIGN_HISTORY_READ_SINCE_DAY)) {
					campaignsToAddList.add(campaign);
				}
			}
		}

		for (int i = 0; i < campaignsToAddList.size(); i++) {
			for (int j = 0; j < campaignsToAddList.size(); j++) {
				CampaignBean campaignITemp = campaignsToAddList.get(i);
				CampaignBean campaignJTemp = campaignsToAddList.get(j);
				if ((campaignJTemp.getCampaignID().equals(campaignITemp.getCampaignID()))) {
					if (campaignJTemp.getLastUpdate() < campaignITemp.getLastUpdate()) {
						LOG.debug(source, "Removing campaign from campaignsToAddList | campaignID: " + campaignJTemp.getCampaignID() + ", duration: "
								+ campaignJTemp.getDuration() + ", lastUpdate: " + TimeUtils.long2String(campaignJTemp.getLastUpdate(), errorLogger));
						campaignsToAddList.remove(campaignJTemp);
					} else if (campaignJTemp.getLastUpdate() > campaignITemp.getLastUpdate()) {
						LOG.debug(source, "Removing campaign from campaignsToAddList | campaignID: " + campaignITemp.getCampaignID() + ", duration: "
								+ campaignITemp.getDuration() + ", lastUpdate: " + TimeUtils.long2String(campaignITemp.getLastUpdate(), errorLogger));
						campaignsToAddList.remove(campaignITemp);
					}
				}
			}
		}

		LOG.debug(source, "campaignsToAddList size: " + campaignsToAddList.size());
		for (int i = 0; i < campaignsToAddList.size(); i++) {
			campaignsHistoryListTemp.put(campaignsToAddList.get(i).getCampaignID(), campaignsToAddList.get(i).getDuration());
			LOG.debug(source, "campaignsToAdd[" + i + "]: " + campaignsToAddList.get(i).getCampaignID() + "|" + campaignsToAddList.get(i).getDuration());
		}

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			campaignsCouchbaseAdsmart.putAll(campaignsHistoryListTemp);
		else
			campaignsCouchbaseEthan.putAll(campaignsHistoryListTemp);

		LOG.info(source, "End DButils.reloadCampaignsHistoryCouchbase, #campaignsFromHistory: " + campaignsHistoryListTemp.size());

	}

	/**
	* Reload channels from couchbase (view name and DD in config) and loads in a specific list.
	* 
	* @param logger
	* @param errorLogger
	* @param config
	* @throws Exception
	*/
	public static synchronized void reloadChannelsCouchbase(ErrorLogger errorLogger, IConfig config, String source)
			throws Exception {

		LOG.info(source, "Start DButils.reloadChannelsCouchbase");

		List<ChannelBean> channelsListTemp = new ArrayList<ChannelBean>();

		String bucketChannel_activeView_name = config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketChannel_activeView_name = config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW);
		}

		String bucketChannel_activeView_dd = config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW_DD);
		if (source.equalsIgnoreCase(ConfigKeys.ETHAN)) {
			bucketChannel_activeView_dd = config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.ALL_CHANNELS_VIEW_DD);
		}

		ViewQuery myView = ViewQuery.from(bucketChannel_activeView_dd, bucketChannel_activeView_name);
		myView.stale(Stale.FALSE); // If not set to false uses a fast (but not
		// updated) data version

		ViewResult result = Database.getBucket(source, Database.Type.Channel).query(myView, 30, TimeUnit.SECONDS);

		for (ViewRow row : result) {
			JsonObject jsonChannelObject = row.document().content().getObject("element");

			ChannelBean newChannel = new ChannelBean();

			String skey = jsonChannelObject.getString("skey");
			String name = jsonChannelObject.getString("name");
			String sIServiceID = jsonChannelObject.getArray("tdvb").getString(2);
			String originalNetworkID = jsonChannelObject.getArray("tdvb").getString(0);
			String transportStreamID = jsonChannelObject.getArray("tdvb").getString(1);
			String type = jsonChannelObject.getString("type");
			String definition = jsonChannelObject.getString("definition");
			long fromLong = TimeUtils.UTCstring2long(jsonChannelObject.getString("from"), errorLogger);
			long toLong = TimeUtils.UTCstring2long(jsonChannelObject.getString("to"), errorLogger);

			// I load all the values in the channel
			newChannel.setServiceKey(skey);
			newChannel.setName(name);
			newChannel.setSiServiceID(sIServiceID);
			newChannel.setOriginalNetworkID(originalNetworkID);
			newChannel.setTransportStreamID(transportStreamID);
			newChannel.setType(type);
			newChannel.setDefinition(definition);
			newChannel.setFromDate(fromLong);
			newChannel.setToDate(toLong);

			channelsListTemp.add(newChannel);
		}

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			channelsCouchbaseAdsmart = channelsListTemp;
			LOG.info(source, "End DButils.reloadChannelsCouchbase, #channels: " + channelsCouchbaseAdsmart.size());
		} else {
			channelsCouchbaseEthan = channelsListTemp;
			LOG.info(source, "End DButils.reloadChannelsCouchbase, #channels: " + channelsCouchbaseEthan.size());
		}

	}

	/**
	* Returns the duration in seconds of a campaigns with a specific campaignID, it looks up first in the local file and
	* then in the couchbase data, if ID not found returns 0
	* 
	* @param campaignId
	* @return
	*/
	public static synchronized int getDurationFromCampaign(String campaignId, String source) {

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			if (campaignsFileAdsmart.containsKey(campaignId))
				return campaignsFileAdsmart.get(campaignId);
			if (campaignsCouchbaseAdsmart.containsKey(campaignId))
				return campaignsCouchbaseAdsmart.get(campaignId);
		} else {
			if (campaignsFileEthan.containsKey(campaignId))
				return campaignsFileEthan.get(campaignId);
			if (campaignsCouchbaseEthan.containsKey(campaignId))
				return campaignsCouchbaseEthan.get(campaignId);
		}
		return 0;
	}

	public static synchronized boolean isCampaignExistent(String campaignId, String source) {
		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			if (campaignsFileAdsmart.containsKey(campaignId) || campaignsCouchbaseAdsmart.containsKey(campaignId)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (campaignsFileEthan.containsKey(campaignId) || campaignsCouchbaseEthan.containsKey(campaignId)) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	* Returns a list of channels with a specific serviceKey, it looks up first in the local file and then in the
	* couchbase data, if sKey not found returns an empty list
	* 
	* @param skey
	* @return
	*/
	public static synchronized List<ChannelBean> getChannelFromSkey(String skey, String source) {
		List<ChannelBean> channelsListTemp = new ArrayList<ChannelBean>();

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			for (ChannelBean c : channelsFileAdsmart) {
				if (skey.equals(c.getServiceKey()))
					channelsListTemp.add(c);
			}

			for (ChannelBean c : channelsCouchbaseAdsmart) {
				if (skey.equals(c.getServiceKey()))
					channelsListTemp.add(c);
			}
		} else {
			for (ChannelBean c : channelsFileEthan) {
				if (skey.equals(c.getServiceKey()))
					channelsListTemp.add(c);
			}

			for (ChannelBean c : channelsCouchbaseEthan) {
				if (skey.equals(c.getServiceKey()))
					channelsListTemp.add(c);
			}
		}

		return channelsListTemp;
	}

	/**
	* Returns a list of channels with specific networkID, transportID and serviceID; it looks up first in the local file
	* and then in the couchbase data, if not found returns an empty list
	* 
	* @param netID
	* @param tsID
	* @param sID
	* @return
	*/
	public static synchronized List<ChannelBean> getChannelFromIDs(String netID, String tsID, String sID, String source) {

		List<ChannelBean> channelsListTemp = new ArrayList<ChannelBean>();

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
			for (ChannelBean c : channelsFileAdsmart) {
				if (c.getSiServiceID().equals(sID) && c.getOriginalNetworkID().equals(netID) && c.getTransportStreamID().equals(tsID))
					channelsListTemp.add(c);
			}

			for (ChannelBean c : channelsCouchbaseAdsmart) {
				if (c.getSiServiceID().equals(sID) && c.getOriginalNetworkID().equals(netID) && c.getTransportStreamID().equals(tsID))
					channelsListTemp.add(c);
			}
		} else {
			for (ChannelBean c : channelsFileEthan) {
				if (c.getSiServiceID().equals(sID) && c.getOriginalNetworkID().equals(netID) && c.getTransportStreamID().equals(tsID))
					channelsListTemp.add(c);
			}

			for (ChannelBean c : channelsCouchbaseEthan) {
				if (c.getSiServiceID().equals(sID) && c.getOriginalNetworkID().equals(netID) && c.getTransportStreamID().equals(tsID))
					channelsListTemp.add(c);
			}
		}

		return channelsListTemp;
	}
	
    /**
     * legge uno stato da couchbase con id stateId, in caso di disconnessione ci riprova come da configurazione, se
     * continua a fallire lancia una eccezione. Se la connessione funziona ma id non presente nel database restituisce
     * null.
     * @param stateId Id dello stato da cercare
     * @param source Ethan / Adsmart
     * @param config
     * @param errorLogger
     * @return Il documento con lo stato o null
     * @throws Exception Lanciato in caso di timeout
     */
    public static JsonDocument readState(String stateId, String source, IConfig config, ErrorLogger errorLogger) throws Exception {
        int dbRetryLeft = config.getInteger(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_RETRY_NUM);
        JsonDocument response = null;

        while (response == null) {
            try {
                response = Database.getBucket(source, Database.Type.StateMachine).get(stateId);
            } catch (RuntimeException e) {
                if ("java.util.concurrent.TimeoutException".equalsIgnoreCase(e.getCause().getClass().getName())) {
                    // se couchbase va in timeout asptetto tot secondi e ci riprovo n volte prima di lanciare eccezione
                    LOG.info(source, "Timeout reading couchbase, waiting for retry. Retry left: " + dbRetryLeft);
                    errorLogger.write(ErrorCode.DB_READ, "[module:Parser,method:readState,exception:" + e.getMessage() + "]");
                    dbRetryLeft--;
                    if (dbRetryLeft < 0)
                        throw new Exception("Couchbase read timeout, max retry reached.", e);
                    Thread.sleep(config.getLong(ConfigKeys.FIXED.COMMON_CFG.DATABASE_READ_WAIT_MSEC));
                    continue;
                } else {
                    throw e;
                }
            }
        }

        return response;
    }
    
    /**
     * Salva uno stato in couchbase, in caso di disconnessione ci riprova come da configurazione, se continua a fallire
     * lancia una eccezione.
     * @param document docuemnto da salvare
     * @param source Ethan / Adsmart
     * @param config
     * @param errorLogger
     * @throws Exception Lanciato in caso di timeout
     */
    public static void writeState(JsonDocument document, String source, IConfig config, ErrorLogger errorLogger) throws Exception {
        int dbRetryLeft = config.getInteger(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_RETRY_NUM);
        boolean toWrite = true;

        while (toWrite) {
            try {
                Database.getBucket(source, Database.Type.StateMachine).upsert(document);
                toWrite = false;
            } catch (RuntimeException e) {
                if ("java.util.concurrent.TimeoutException".equalsIgnoreCase(e.getCause().getClass().getName())) {
                    // se couchbase va in timeout asptetto tot secondi e ci riprovo n volte prima di lanciare eccezione
                    LOG.info(source, "Timeout writing couchbase, waiting for retry. Retry left: " + dbRetryLeft);
                    errorLogger.write(ErrorCode.DB_WRITE, "[module:Parser,method:writeState,exception:" + e.getMessage() + "]");
                    dbRetryLeft--;
                    if (dbRetryLeft < 0)
                        throw new Exception("Couchbase write timeout, max retry reached.", e);
                    Thread.sleep(config.getLong(ConfigKeys.FIXED.COMMON_CFG.DATABASE_WRITE_WAIT_MSEC));
                    continue;
                } else {
                    throw e;
                }
            }
        }
    }

	// --- Standard getters and setters ---
	public static Map<String, Integer> getCampaignsCouchbase(String source) {
		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			return campaignsCouchbaseAdsmart;
		else
			return campaignsCouchbaseEthan;
	}
	
	public static List<ChannelBean> getChannelsCouchbase(String source) {
		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			return channelsCouchbaseAdsmart;
		else
			return channelsCouchbaseEthan;
	}
}
