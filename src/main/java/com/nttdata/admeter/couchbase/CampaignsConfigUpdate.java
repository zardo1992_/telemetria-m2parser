package com.nttdata.admeter.couchbase;

import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IConfigUpdate;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.LOG;

public class CampaignsConfigUpdate implements IConfigUpdate {

	private ErrorLogger errorLogger;
	private IConfig config;
	private String source;
	private String fileName = "";

	public CampaignsConfigUpdate(ErrorLogger errorLogger, IConfig config, String source) {
		super();
		this.errorLogger = errorLogger;
		this.config = config;
		this.source = source;

		if (source.equalsIgnoreCase(ConfigKeys.ADSMART))
			this.fileName = config.getString(ConfigKeys.FIXED.ADSMART_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
		else
			this.fileName = config.getString(ConfigKeys.FIXED.ETHAN_CFG.OVERRIDE_CAMPAIGN_FILE_NAME);
	}

	@Override
	public boolean update() {
		try {
			LOG.info(source, ": Triggered campaigns reload from file: " + this.fileName);
			DButils.reloadCampaignsFileSafe(errorLogger, config, source);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(source,"Error reloading campaigns from json file: ", e);
			errorLogger.write(ErrorCode.GENERIC,
					"[module:Parser,method:CampaignsConfigUpdate,exception:" + e.getMessage() + "]");
			return false;
		}
		return true;
	}

	@Override
	public String getConfigFileName() {
		return this.fileName;
	}

}
