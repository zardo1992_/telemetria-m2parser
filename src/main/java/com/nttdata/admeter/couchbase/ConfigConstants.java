package com.nttdata.admeter.couchbase;

/**
 * Configuration constants specific for Couchbase communication
 */
public class ConfigConstants {

   public static final String BUCKET_CAMPAIGN = "tableCampaignList";
   public static final String BUCKET_CHANNEL = "tableChannelList";
   public static final String BUCKET_CAMPAIGN_HISTORY = "tableCampaignHistory";
   
   public static final String BUCKET_STATE_MACHINE = "tableStateMachine";

   public static final String BUCKET_NAME = "bucketName";
   public static final String BUCKET_PWD = "bucketPass";

   public static final String ALL_CAMPAIGNS_VIEW = "allCampaignsView";
   public static final String ALL_CAMPAIGNS_VIEW_DD = "allCampaignsViewDD";
   public static final String ALL_CHANNELS_VIEW = "allChannelsView";
   public static final String ALL_CHANNELS_VIEW_DD = "allChannelsViewDD";
   public static final String ALL_CAMPAIGNS_HISTORY_VIEW = "allCampaignsHistoryView";
   public static final String ALL_CAMPAIGNS_HISTORY_VIEW_DD = "allCampaignsHistoryViewDD";
}
