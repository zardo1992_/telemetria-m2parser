package com.nttdata.admeter.couchbase;

/**
 * Container of campaign informations.
 * @author MCortesi
 *
 */
public class CampaignBean {

	private String campaignID;
	private int duration;
	private long lastUpdate;
	
	public CampaignBean(String campaignID, int duration, long lastUpdate) {
		super();
		this.campaignID = campaignID;
		this.duration = duration;
		this.lastUpdate = lastUpdate;
	}

	public String getCampaignID() {
		return campaignID;
	}

	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
