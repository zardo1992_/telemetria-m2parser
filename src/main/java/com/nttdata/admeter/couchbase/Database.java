package com.nttdata.admeter.couchbase;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.utils.LOG;

public class Database {

	public enum Type {
		Campaign,
		StateMachine,
		Channel,
		CampaignHistory
	} 
	
	private static IConfig config = null;
	
	private static Cluster myCluster = null;
	private static List<String> nodes = null;
	private static CouchbaseEnvironment env = null;
	
	static final
	private Map<Type, Bucket>  bucketMapAdSmart = new HashMap<>(4);
	
	static final
	private Map<Type, Bucket>  bucketMapEthan = new HashMap<>(4);
	

	public static void connect() throws ConnectException{

		// I load the nodes list
		JSONArray configNodes = config.getArray(ConfigKeys.FIXED.COMMON_CFG.DATABASE_IP.ARRAY);
		nodes = new ArrayList<String>();
		for( int i=0; i<configNodes.length(); i++){
			nodes.add(configNodes.getString(i));
		} 

		LOG.info("Connecting to couchbase nodes:"+ nodes.toString());

		if (Database.env == null) {
			// I connect to nodes with a long timeout
			Database.env = DefaultCouchbaseEnvironment.builder()
					.connectTimeout(30000)  // in milliseconds, default is 5s
					.autoreleaseAfter(30000) // if not set the data get from db will be deleted too early
					.build();
		}
		
		LOG.info("Enviroment built.");

		try {
			if (myCluster == null) {
				Database.myCluster = CouchbaseCluster.create(Database.env, nodes);
				LOG.info("Cluster created.");
			}
			
			LOG.info("Opening buckets...");

			/* AdSmart */
			bucketMapAdSmart.put(Type.Campaign, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_PASS))
				);
			
			bucketMapAdSmart.put(Type.CampaignHistory, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_PASS))
				);
			
			bucketMapAdSmart.put(Type.Channel, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_PASS))
				);
			
			bucketMapAdSmart.put(Type.StateMachine, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ADSMART_CFG.DB_TABLE_STATE_MACHINE.BUCKET_PASS))
				);
			/* Ethan */
			bucketMapEthan.put(Type.Campaign, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_LIST.BUCKET_PASS))
				);
			
			bucketMapEthan.put(Type.CampaignHistory, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CAMPAIGN_HISTORY.BUCKET_PASS))
				);
			
			bucketMapEthan.put(Type.Channel, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_CHANNEL_LIST.BUCKET_PASS))
				);
			
			bucketMapEthan.put(Type.StateMachine, 
					Database.myCluster.openBucket(
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.BUCKET_NAME), 
							config.getString(ConfigKeys.FIXED.ETHAN_CFG.DB_TABLE_STATE_MACHINE.BUCKET_PASS))
				);
			
			
		} catch (Exception e) {
			// If something goes wrong I close the connections
			LOG.error("Error connecting to couchBase: "+ e);
			closeConnection();
			throw new ConnectException();
		}
		
		LOG.info("Successfully connected to couchbase....");
	}
	
	
	public static boolean isConnectionOK() {

		// To know if the connection is ok I try to get a document with a fake id, if connection is ok no exception will be thrown
		// doesn't matter if the document exists or not.
		try {
			if (getBucket(ConfigKeys.ADSMART, Type.StateMachine) == null )
				connect();
			getBucket(ConfigKeys.ADSMART, Type.StateMachine).get("fake-id");
		} catch (Exception e) {
			// If something goes wrong I return false
			LOG.error("Couchbase connection KO: "+ e);
			return false;
		}
		
		LOG.info("Couchbase connection OK.");
		return true;
	}

	public static void closeConnection(){

		LOG.info("Closing connection to couchbase....");

		for (Map.Entry<Type, Bucket> entry: bucketMapAdSmart.entrySet()) {
			entry.getValue().close();
		}
		bucketMapAdSmart.clear();
		
		for (Map.Entry<Type, Bucket> entry: bucketMapEthan.entrySet()) {
			entry.getValue().close();
		}
		bucketMapEthan.clear();
		
        if (Database.myCluster != null) {
            Database.myCluster.disconnect();
        }
	}
	

	public static void setConfig(IConfig config) {
		Database.config = config;
	}
	
	static
	protected Bucket getBucket(String source, Type type) {
		return  source.equalsIgnoreCase(ConfigKeys.ADSMART) ?
			bucketMapAdSmart.get(type) : bucketMapEthan.get(type);
	}
}
