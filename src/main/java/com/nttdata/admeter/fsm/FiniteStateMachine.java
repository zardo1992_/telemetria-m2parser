package com.nttdata.admeter.fsm;

import com.nttdata.admeter.event.input.EventBean;

public interface FiniteStateMachine {

	public enum State {
		START, 
		S0LIVEVIEW, 
		S1RECVIEW, 
		S2VODVIEW, 
		S3ADLIVE, 
		S4ADREC, 
		S5STANDBY,
		S6STANDBYOUT,
		S7SURF
	}

	void setCurrentState(State valueOf);

	State getCurrentState();

	void setCurrentEvent(EventBean viewChToRead);

	void doAction(EventBean inputEvent);

	int getViewChCounter();

	int getSubsCounter();

	int getViewVODCounter();

	void setViewChCounter(int i);

	void setSubsCounter(int i);

	void setViewVODCounter(int i);

	EventBean getCurrentEvent();

}
