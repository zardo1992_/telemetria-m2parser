package com.nttdata.admeter.fsm;

import java.util.ArrayList;
import java.util.List;

import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constants.M2Statistics;
import com.nttdata.admeter.event.input.EvAdSmartBean;
import com.nttdata.admeter.event.input.EvChangeViewBean;
import com.nttdata.admeter.event.input.EvStandbyInBean;
import com.nttdata.admeter.event.input.EvVODPlaybackBean;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.output.EventPrinterHelper;
import com.nttdata.admeter.event.output.SubsEventBean;
import com.nttdata.admeter.event.output.ViewChEventBean;
import com.nttdata.admeter.event.output.ViewVODEventBean;
import com.nttdata.admeter.event.payload.EventPayloadBean;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.output.OutputMessage;
import com.nttdata.admeter.runnable.StatParserRunnable;
import com.nttdata.admeter.utils.LOG;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * Implementation of the finite state machine, which receives events from Collector output,
 * processes informations contained in those events basing on the current state setted, and returns an output structure.
 * The FSM has 6 states:
 * - START: the FSM will be in this state every time it will be initialized;
 * - S0LIVEVIEW;
 * - S1RECVIEW;
 * - S2VODVIEW;
 * - S3ADLIVE;
 * - S4ADREC;
 * - S5STANDBY.
 * @author MCortesi
 *
 */
public class FiniteStateMachineAdSmart implements FiniteStateMachine {

	private ErrorLogger			errorLogger;
	private IConfig				config;
	private StatParserRunnable	stats;
	private State				currentState;
	private EventBean			currentEvent;
	private List<OutputMessage> eventsToWrite;

	private int viewChCounter = 0;
	private int subsCounter = 0;
	private int viewVODCounter = 0;

	public FiniteStateMachineAdSmart(ErrorLogger errorLogger, IConfig config, StatParserRunnable stats, List<OutputMessage> eventsToWrite) {
		super();
		this.errorLogger = errorLogger;
		this.config = config;
		this.stats = stats;
		this.currentState = State.START;
		this.eventsToWrite = eventsToWrite;
	}

	public State getCurrentState() {
		return currentState;
	}

	public void setCurrentState(State currentState) {
		this.currentState = currentState;
	}

	public void doAction(EventBean event) {

		switch (currentState) {
		case START :
			processStartState(event);
			break;

		case S0LIVEVIEW :
			processS0LiveViewState(event);
			break;

		case S1RECVIEW :
			processS1RecViewState(event);
			break;

		case S2VODVIEW :
			processS2VODViewState(event);
			break;

		case S4ADREC :
			processS4AdRecState(event);
			break;

		case S3ADLIVE :
			processS3AdLiveState(event);
			break;

		case S5STANDBY :
			processS5StandByInState(event);
			break;

		default :
			break;
		}
	}

	protected void processStartState(EventBean event) {

		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered start state");

		if ((event instanceof EvChangeViewBean)) {

			currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);

			EvChangeViewBean eventIn = (EvChangeViewBean) event;
			ViewChEventBean eventOut = (ViewChEventBean)currentEvent;

			// Arrived evChangeView without playbackSpeed and recorded time -> S0LiveView state
			if (eventIn.isLive()) {
				initEvent(eventOut, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
				currentState = State.S0LIVEVIEW;
				// Arrived evChangeView with playbackSpeed not null -> S1RecView
			} else {

				initEvent(eventOut, eventIn, ConfigKeys.YES, eventIn.getRecordedTime(), event.getRefDate(), "01");
				currentState = State.S1RECVIEW;
			}

		} else if (event instanceof EvVODPlaybackBean) {
			// Arrived evVODPlayback -> S2VODView
			currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
			initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
			currentState = State.S2VODVIEW;

			// Arrived evAdSmart -> unexpected in START state either if it is LIVE or RECORDED
		} else if (event instanceof EvAdSmartBean) {
			EvAdSmartBean eventIn = (EvAdSmartBean) event;

			if (eventIn.getStartOffset() == 0) {
				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((SubsEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
				currentState = State.S3ADLIVE;
			} else {
				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((SubsEventBean) currentEvent, eventIn, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
				currentState = State.S4ADREC;
			}

			errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart is unexpected in start state. EventTime: " + event.getRefDate());
			LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | UNEXPECTED EVENT. evAdSmart is unexpected in start state. EventTime: " + event.getRefDate());

		} else if (event instanceof EvStandbyInBean) {
			// Arrived evStandbyIn -> S5STANDBY (directly, without any check)
			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}	

	protected void processS0LiveViewState(EventBean event) {
		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered S0 state");

		if (event instanceof EvChangeViewBean) {
			EvChangeViewBean eventIn = (EvChangeViewBean) event;
			ViewChEventBean eventOut = (ViewChEventBean)currentEvent;
			// Arrived evChangeView without playback speed and recorded time -> S0LIVEVIEW (if not negative case)
			if (eventIn.isLive()) {
				// Checking service keys of current event and last event
				if ( sameChannel(eventIn, eventOut) == false ) {

					cutEvent(eventOut, event, "10");

					currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewChEventBean)currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
					currentState = State.S0LIVEVIEW;

					// If keys are the same current event is discarded because it is a duplicated event
				} else {
					stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
					errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
					isDiscarded = true;
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
				}

				/*
				 * Arrived evChangeView with playbackSpeed and recorded time not null -> S1RECVIEW
				 * (multiple cases based on playback speed) 
				 */
			} else {

				// Pause (playbackSpeed equals to zero): live view becomes recorded.
				String actionCodeEnd = "10";
				if (eventIn.getPlaybackSpeed() == 0)
					actionCodeEnd = sameChannel(eventIn, eventOut) ? "13" : "10";
				cutEvent(eventOut, event, actionCodeEnd);

				//In any case, the initialization of ViewCh recorded is the same
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean)currentEvent, eventIn, ConfigKeys.YES, eventIn.getRecordedTime(), event.getRefDate(), "01");
				currentState = State.S1RECVIEW;
			}

		} else if (event instanceof EvAdSmartBean) {
			EvAdSmartBean eventIn = (EvAdSmartBean) event;
			ViewChEventBean eventOut = (ViewChEventBean)currentEvent;

			// Saving lastServiceKey before initializing a new currentEvent of type "SUBS"
			boolean wereSameChannel = sameChannel(eventIn, eventOut);
			cutEvent(eventOut, event, "03");

			currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
			SubsEventBean eventOutSubs = (SubsEventBean)currentEvent;

			// Arrived evAdSmart with startOffset equals to zero -> S3ADLIVE
			if (eventIn.getStartOffset() == 0) {

				currentState = State.S3ADLIVE;

				if (wereSameChannel == false) {
					initEvent(eventOutSubs, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
					errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
				} else {
					initEvent(eventOutSubs, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, eventOut.getTunerID());
				}
				// Arrived evAdSmart with startOffset different from zero -> S4ADREC
			} else {
				initEvent(eventOutSubs, eventIn, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
				currentState = State.S4ADREC;
				errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
				LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
			}

		} else if (event instanceof EvVODPlaybackBean) {
			// Arrived evVODPlayback -> S2VODVIEW (directly, without any check)
			cutEvent((ViewChEventBean) currentEvent, event, "10");
			currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
			initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
			currentState = State.S2VODVIEW;

		} else if (event instanceof EvStandbyInBean) {
			// Arrived evStandbyIn -> S5STANDBY (directly, without any check)
			cutEvent((ViewChEventBean) currentEvent, event, "20");
			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}

	protected void processS1RecViewState(EventBean event) {

		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered S1 state");

		if (event instanceof EvChangeViewBean) {
			EvChangeViewBean eventIn = (EvChangeViewBean) event;

			// Arrived evChangeView without playback speed and recorded time -> S0LIVEVIEW
			if (eventIn.isLive()) {
				cutEvent((ViewChEventBean) currentEvent, event, "10");
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
				currentState = State.S0LIVEVIEW;
			} else {
				ViewChEventBean eventOut = (ViewChEventBean) currentEvent;

				// Arrived evChangeView with playback speed and recorded time not null -> S1RECVIEW
				if (sameChannel(eventIn, eventOut)) {

					// lastPayload is the last opened payload inside the current ViewCh event
					EventPayloadBean lastPayload = eventOut.getPayload().get(eventOut.getPayload().size() - 1);
					if (lastPayload.getxSpeed() != eventIn.getPlaybackSpeed()) {

						/*
						 *  The newly arrived evChangeView is on the same channel of the previous,
						 *  but with different playback speed, so I close lastPayload and open a new one
						 */
						long startTsConverted = TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger);
						lastPayload.setEndTs(event.getRefDate());
						long endTsConverted = TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger);
						lastPayload.setDuration(endTsConverted - startTsConverted);

						// Initializing and adding of new payload
						EventPayloadBean payloadBean = new EventPayloadBean();
						payloadBean.setStartTs(event.getRefDate());
						payloadBean.setxSpeed(((EvChangeViewBean) event).getPlaybackSpeed());

						// Action code end of lastPayload is based on playback speed of the new event
						if (eventIn.getPlaybackSpeed() > 0) {
							lastPayload.setActionEnd("12");
							payloadBean.setActionStart("12");
						} else if (eventIn.getPlaybackSpeed() < 0) {
							lastPayload.setActionEnd("11");
							payloadBean.setActionStart("11");
						} else {
							lastPayload.setActionEnd("13");
							payloadBean.setActionStart("13");
						}
						eventOut.getPayload().add(payloadBean);
						currentState = State.S1RECVIEW;
					} else {
						stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
						errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
						isDiscarded = true;
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
					}

					// Arrived evChangeView (recorded) on a different channel -> S1RECVIEW
				} else {
					cutEvent(eventOut, event, "10");
					currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.YES, eventIn.getRecordedTime(), event.getRefDate(), "01");
					currentState = State.S1RECVIEW;
				}
			}

		} else if (event instanceof EvVODPlaybackBean) {

			// Arrived evVODPlayback -> S2VODVIEW (directly, without any check)
			cutEvent((ViewChEventBean) currentEvent, event, "10");
			currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
			initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
			currentState = State.S2VODVIEW;

		} else if (event instanceof EvAdSmartBean) {
			EvAdSmartBean eventIn = (EvAdSmartBean) event;
			ViewChEventBean eventOut = (ViewChEventBean) currentEvent;

			cutEvent(eventOut, event, "03");

			//Saving lastSpeed, lastRecTime and lastServiceKey because they will be useful after new output event initialization
			int lastSpeed = eventOut.getPayload().get(eventOut.getPayload().size() - 1).getxSpeed();
			String lastRecTime = eventOut.getRecTime();
			String lastServiceKey = eventOut.getServiceKey();
			boolean wereSameChannel = sameChannel(eventIn, eventOut);

			currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);

			LOG.debug(Source.ADSMART, "Arrived ADSmart event. StartOffset: [" + eventIn.getStartOffset() + 
					"], Duration: [" + eventIn.getDuration() +
					"], LastSpeed: [" + lastSpeed + "]");

			// Checking startOffset of the new event and playback speed of the previous event
			if ( wereSameChannel ) {
				if (eventIn.getStartOffset() == 0) {
					if (lastSpeed > 0) {
						initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.YES, lastRecTime, event.getRefDate(), "03", lastSpeed, eventOut.getTunerID());
						currentState = State.S4ADREC;
					} else {			
						initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
						currentState = State.S3ADLIVE;
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with invalid startOffset. Event before EventTime: " + event.getRefDate());
					}
				} else {
					if ((eventIn.getStartOffset() == eventIn.getDuration()) && lastSpeed < 0) {
						initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.YES, lastRecTime, event.getRefDate(), "03", lastSpeed, eventOut.getTunerID());
						currentState = State.S4ADREC;
					} else {
						initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
						currentState = State.S4ADREC;
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with invalid startOffset. Event before EventTime: " + event.getRefDate());
					}
				}
			} else {
				if (eventIn.getStartOffset() == 0) {
					initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
					currentState = State.S3ADLIVE;
				} else {
					initEvent((SubsEventBean)currentEvent, eventIn, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
					currentState = State.S4ADREC;
				}
				errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with some unexpected values. EventTime: [" + event.getRefDate() +
						"], Last ServiceKey: [" + lastServiceKey + 
						"], CurrentServiceKey: [" + eventIn.getServiceKey() + 
						"], StartOffset: [" + eventIn.getStartOffset() +
						"], LastSpeed: [" + lastSpeed + "]");
				LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with some unexpected values. EventTime: [" + event.getRefDate() + 
						"], Last ServiceKey: [" + lastServiceKey + 
						"], CurrentServiceKey: [" + eventIn.getServiceKey() + 
						"], StartOffset: " + eventIn.getStartOffset() +
						"], LastSpeed: [" + lastSpeed + "]");
			}

		} else if (event instanceof EvStandbyInBean) {
			// Arrived evStandbyIn -> S5STANDBY (directly, without any check)
			cutEvent((ViewChEventBean) currentEvent, event, "20");
			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}

	protected void processS2VODViewState(EventBean event) {

		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered S2 state");

		if (event instanceof EvChangeViewBean) {
			EvChangeViewBean eventIn = (EvChangeViewBean) event;

			// Arrived evChangeView without playback speed and recorded time -> S0LIVEVIEW
			if (eventIn.isLive()) {
				cutEvent((ViewVODEventBean) currentEvent, event, "10");
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
				currentState = State.S0LIVEVIEW;

				// Arrived evChangeView with playback speed and recorded time not null -> S1RECVIEW
			} else {
				cutEvent((ViewVODEventBean) currentEvent, event, "10");
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);

				initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.YES, eventIn.getRecordedTime(), event.getRefDate(), "01");
				currentState = State.S1RECVIEW;
			}

		} else if (event instanceof EvAdSmartBean) {
			EvAdSmartBean eventIn = (EvAdSmartBean) event;

			cutEvent((ViewVODEventBean)currentEvent, event, "03");
			currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
			// Arrived evAdSmart -> checking start offset value to know where to go (=0 -> S3ADLIVE; !=0 -> S4ADREC)
			if (eventIn.getStartOffset() == 0) {
				initEvent((SubsEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
				currentState = State.S3ADLIVE;
			} else {
				initEvent((SubsEventBean) currentEvent, eventIn, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
				currentState = State.S4ADREC;
			}

			errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
			LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());

		} else if (event instanceof EvVODPlaybackBean) {
			EvVODPlaybackBean eventIn = (EvVODPlaybackBean)event;
			ViewVODEventBean eventOut = (ViewVODEventBean)currentEvent;

			if (eventIn.getVodAssetID().equals(eventOut.getVodID())) {
				// lastPayload is the last opened payload inside the current ViewCh event
				EventPayloadBean lastPayload = eventOut.getPayload().get(eventOut.getPayload().size() - 1);
				if (lastPayload.getxSpeed() != eventIn.getPlaybackSpeed()) {
					/*
					 *  The newly arrived evVODPlayback is on the same channel of the previous,
					 *  but with different playback speed, so I close lastPayload and open a new one
					 */
					long startTsConverted = TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger);
					lastPayload.setEndTs(event.getRefDate());
					long endTsConverted = TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger);
					lastPayload.setDuration(endTsConverted - startTsConverted);

					// Initializing and adding of new payload
					EventPayloadBean payloadBean = new EventPayloadBean();
					payloadBean.setStartTs(event.getRefDate());
					payloadBean.setxSpeed(eventIn.getPlaybackSpeed());

					// Action code end of lastPayload is based on playback speed of the new event
					if (eventIn.getPlaybackSpeed() > 0) {
						lastPayload.setActionEnd("12");
						payloadBean.setActionStart("12");
					} else if (eventIn.getPlaybackSpeed() < 0) {
						lastPayload.setActionEnd("11");
						payloadBean.setActionStart("11");
					} else {
						lastPayload.setActionEnd("13");
						payloadBean.setActionStart("02");
					}
					eventOut.getPayload().add(payloadBean);
					currentState = State.S2VODVIEW;
				} else {
					stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
					errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
					isDiscarded = true;
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
				}
			} else {
				cutEvent((ViewVODEventBean) currentEvent, event, "10");
				currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewVODEventBean)currentEvent, (EvVODPlaybackBean)event, event.getRefDate(), "02");
				currentState = State.S2VODVIEW;
			}
		} else if (event instanceof EvStandbyInBean) {
			// Arrived evStandbyIn -> S5STANDBY (directly, without any check)
			cutEvent((ViewVODEventBean) currentEvent, event, "20");
			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}

	protected void processS3AdLiveState(EventBean event) {

		SubsEventBean oldCurrentEvent;
		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered S3 state");

		if (event instanceof EvChangeViewBean) {
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				/*
				 * Arrived evChangeView after asset end
				 * -> creation of fake  event
				 * -> check if event is live or recorded to know where to go and how
				 */
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, (SubsEventBean) oldCurrentEvent, ConfigKeys.NO, null,
						TimeUtils.long2String(((SubsEventBean) oldCurrentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				if ((((EvChangeViewBean) event).getPlaybackSpeed() == null) && (((EvChangeViewBean) event).getRecordedTime() == null)) {

					/*
					 * closure of fake event and initialization of real event
					 * (only if service keys are different. If not, event will be treated as a duplicated)
					 */
					if ( sameChannel((EvChangeViewBean)event, (ViewChEventBean)currentEvent)==false ) {
						cutEvent((ViewChEventBean) currentEvent, event, "10");
						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.NO, null, event.getRefDate(), "00");
					} else {
						stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
						errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
						isDiscarded = true;
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
					}

					currentState = State.S0LIVEVIEW;
				} else {
					if (((EvChangeViewBean) event).getPlaybackSpeed() == 0) {
						// Pause on the same channel -> going in playback mode
						if ( sameChannel((EvChangeViewBean) event, (ViewChEventBean) currentEvent)==false ) {

							cutEvent((ViewChEventBean) currentEvent, event, "10");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean)event).getRecordedTime(), event.getRefDate(), "01");
							currentState = State.S1RECVIEW;
						} else {
							cutEvent((ViewChEventBean) currentEvent, event, "13");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean)event).getRecordedTime(), event.getRefDate(), "01");
							currentState = State.S1RECVIEW;
						}
					} else {
						cutEvent((ViewChEventBean) currentEvent, event, "10");

						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(), event.getRefDate(), "01");
						currentState = State.S1RECVIEW;
					}

				}
			} else {
				/*
				 * Arrived evChangeView without playback speed and recorded time
				 * before or in the same time of asset end
				 * -> check if event arrived in the same time of asset end
				 * -> S0LIVEVIEW
				 */
				if ((((EvChangeViewBean) event).getPlaybackSpeed() == null) && (((EvChangeViewBean) event).getRecordedTime() == null)) {

					if ( sameChannel((EvChangeViewBean)event, (SubsEventBean)currentEvent) ) {
						stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
						errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
						isDiscarded = true;
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());

					} else {
						if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {

							cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
							oldCurrentEvent = (SubsEventBean) currentEvent;
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.NO, null,
									TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "00");
						} else {

							cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "10");

							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.NO, null, event.getRefDate(), "00");
						}

						currentState = State.S0LIVEVIEW;
					}

				} else {
					/*
					 * Arrived evChangeView with playback speed and recorded time not null
					 * before or in the same time of asset end
					 * -> check if event has playback speed equals to zero (pause)
					 * -> S1RECVIEW
					 */
					if (!((EvChangeViewBean) event).getPlaybackSpeed().equals(0)) {
						if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {

							cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
							oldCurrentEvent = (SubsEventBean) currentEvent;
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(),
									TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "01");
						} else {
							cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "10");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(), event.getRefDate(), "01");
						}
					} else {
						// pausa
						if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
							cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "13");
							oldCurrentEvent = (SubsEventBean) currentEvent;
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(),
									TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "01");
						} else {
							cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "13");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(), event.getRefDate(), "01");
						}
					}

					currentState = State.S1RECVIEW;
				}
			}

		} else if (event instanceof EvVODPlaybackBean) {
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				/*
				 * Arrived evVODPlayback after asset end
				 * -> creation of fake event
				 * -> S2VODVIEW
				 */

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.NO, null, TimeUtils.long2String(((SubsEventBean) oldCurrentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "10");

				// real event
				currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
				currentState = State.S2VODVIEW;
			} else {
				/*
				 * Arrived evVODPlayback before or in the same time of asset end
				 * -> check if event arrived in the same time of asset end
				 * -> S2VODVIEW
				 */
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {

					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
					oldCurrentEvent = (SubsEventBean) currentEvent;
					currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "02");
				} else {
					cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "04");
					currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
				}
				currentState = State.S2VODVIEW;
			}
		} else if (event instanceof EvAdSmartBean) {
			LOG.debug(Source.ADSMART, "State: [S3], event: [(SubsEventBean)currentEvent], eventTime: [" + ((SubsEventBean)currentEvent).getRefDate()
					+ "], naturalEnd: [" + TimeUtils.long2String(((SubsEventBean)currentEvent).getNaturalEnd(), errorLogger) + "]");

			String lastTunerID = ((SubsEventBean)currentEvent).getTunerID() != null ? ((SubsEventBean)currentEvent).getTunerID() : null;
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				/*
				 * Arrived evAdSmart after asset end
				 * -> creation of fake event
				 * -> check of start offset to know where to go (S3ADLIVE or S4ADREC)
				 */
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.NO, null, TimeUtils.long2String(((SubsEventBean) oldCurrentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "03");

				// real event
				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);

				if (((EvAdSmartBean) event).getStartOffset() == 0) {

					currentState = State.S3ADLIVE;

					if ( sameChannel((EvAdSmartBean)event, oldCurrentEvent) == false ) {
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
					} else {
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, lastTunerID);
					}
				} else {
					initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
					currentState = State.S4ADREC;
					errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
				}

			} else {
				/*
				 * Arrived evAdSmart before or in the same time of asset end
				 * -> closure of opened subs event
				 * -> check of startOffset to know where to go (S3ADLIVE or S4ADREC)
				 */
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == (((SubsEventBean) currentEvent).getNaturalEnd()) - 1000) {

					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

					if (((EvAdSmartBean) event).getStartOffset() == 0) {

						if ( sameChannel((EvAdSmartBean) event, (SubsEventBean)currentEvent)==false ) {
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "03", 1, null);
							errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
							LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
						} else {
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "03", 1, lastTunerID);
						}

						currentState = State.S3ADLIVE;
					} else {
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), 
								TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "03", 1, null);
						currentState = State.S4ADREC;
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with unexpected startOffset different from zero. EventTime: " + event.getRefDate());
					}
				} else {

					cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "03");

					if (((EvAdSmartBean) event).getStartOffset() == 0) {

						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
						currentState = State.S3ADLIVE;
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart came before asset end with startOffset equals to 0. eventTime: " + event.getRefDate());
					} else {
						if ( sameChannel((EvAdSmartBean) event, (SubsEventBean) currentEvent)
								&& (((EvAdSmartBean)event).getCampaignID().equals((((SubsEventBean)currentEvent).getCmpID())))
								&& (((EvAdSmartBean)event).getStartOffset()*1000==(TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) 
								- (TimeUtils.UTCstring2long(currentEvent.getRefDate(), errorLogger)))) {
							LOG.debug(Source.ADSMART, "Possibile pausa");
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 0, lastTunerID);
						} else {
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
							errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
							LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
						}
						currentState = State.S4ADREC;
					}
				}
			}
		} else if (event instanceof EvStandbyInBean) {

			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				/*
				 * Arrived evStandbyIn after asset end
				 * -> creation of fake event
				 * -> S5STANDBY
				 */

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.NO, null, TimeUtils.long2String(((SubsEventBean) oldCurrentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "20");

			} else {
				/*
				 * Arrived evStandbyIn before or in the same time of asset end
				 * -> S5STANDBY
				 */
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "20");
				} else {
					cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "20");
				}
			}

			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}

	protected void processS4AdRecState(EventBean event) {
		boolean isDiscarded = false;
		SubsEventBean oldCurrentEvent;

		LOG.debug(Source.ADSMART, "Entered S4 state");

		SubsEventBean thisEvent = (SubsEventBean)currentEvent;

		LOG.debug(Source.ADSMART, "State: [S4], event: [(SubsEventBean)currentEvent], eventTime: [" + thisEvent.getRefDate()
		+ "], naturalEnd: [" + TimeUtils.long2String(thisEvent.getNaturalEnd(), errorLogger) + "]");

		//Initially checking if fsm is entered in S4 state from S3 state (with possible pause)
		if (thisEvent.getPayload().size() == 1 &&
				thisEvent.getPayload().get(thisEvent.getPayload().size()-1).getxSpeed() == 0) {
			if (event instanceof EvChangeViewBean &&
					(TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == TimeUtils.UTCstring2long(currentEvent.getRefDate(), errorLogger)
					|| TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == TimeUtils.UTCstring2long(currentEvent.getRefDate(), errorLogger) + 1000)) {

				EvChangeViewBean eventIn = (EvChangeViewBean)event;

				if (!eventIn.isLive()) {
					if ( sameChannel(eventIn, thisEvent) && eventIn.getPlaybackSpeed().equals(0)) {
						LOG.debug(Source.ADSMART, "Pause from S3ADLIVE confirmed");
					} else {
						thisEvent.getPayload().get(thisEvent.getPayload().size()-1).setxSpeed(1);
						thisEvent.setNaturalEnd((TimeUtils.UTCstring2long(thisEvent.getRefDate(), errorLogger) 
								+ (thisEvent.getCmpDur()*1000
										- thisEvent.getAdOffset()*1000)/(Math.abs(1))) + 1000);
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | Pause from S3ADLIVE not realized: different service key or playback speed not null");
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | Pause not realized. EventTime: " + currentEvent.getRefDate());
					}
				} else {
					thisEvent.getPayload().get(thisEvent.getPayload().size()-1).setxSpeed(1);
					thisEvent.setNaturalEnd((TimeUtils.UTCstring2long(thisEvent.getRefDate(), errorLogger) 
							+ (thisEvent.getCmpDur()*1000
									- thisEvent.getAdOffset()*1000)/(Math.abs(1))) + 1000);
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | Pause from S3ADLIVE not realized: change view not recorded");
					errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | Pause not realized. EventTime: " + currentEvent.getRefDate());
				}
			} else {
				thisEvent.getPayload().get(thisEvent.getPayload().size()-1).setxSpeed(1);
				thisEvent.setNaturalEnd((TimeUtils.UTCstring2long(thisEvent.getRefDate(), errorLogger) 
						+ (thisEvent.getCmpDur()*1000
								- thisEvent.getAdOffset()*1000)/(Math.abs(1))) + 1000);
				LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | Pause from S3ADLIVE not realized: closing event is not a change view or did not arrive in the right time");
				errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | Pause not realized. EventTime: " + currentEvent.getRefDate());
			}
		}

		if (event instanceof EvChangeViewBean) {

			EvChangeViewBean eventIn = (EvChangeViewBean)event;

			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= thisEvent.getNaturalEnd()) {
				if (eventIn.isLive()) {
					/*
					 *  Arrived evChangeView without playback speed and recorded time after asset end
					 *  -> creation of fake event with duration from naturalEnd to EventTime of the new event
					 *  -> S0LIVEVIEW
					 */
					cutEvent(thisEvent, event, TimeUtils.long2String(thisEvent.getNaturalEnd() - 1000, errorLogger), "04");

					// fake event
					oldCurrentEvent = thisEvent;
					currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
							TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "04");
					cutEvent((ViewChEventBean) currentEvent, event, "10");

					// real event
					currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
					currentState = State.S0LIVEVIEW;

				} else {
					/*
					 * evChangeView recorded on same channel (of the
					 * event before evAdSmart) after asset end Special
					 * case of transition #7 of S3ADREC state
					 */
					if ( sameChannel(eventIn, thisEvent) ) {							
						if (thisEvent.getPayload().get(thisEvent.getPayload().size()-1).getxSpeed() == eventIn.getPlaybackSpeed()) {
							stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
							errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
							isDiscarded = true;
							LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
							currentState = State.S4ADREC;
						} else {
							/*
							 *  Arrived evChangeView on the same channel
							 *  with playback speed and recorded time not null after asset end
							 *  -> creation of fake even
							 *  -> S1RECVIEW
							 */
							cutEvent(thisEvent, event, TimeUtils.long2String(thisEvent.getNaturalEnd() - 1000, errorLogger), "04");

							// saving infos that have to be retrieved from Subs event 
							oldCurrentEvent = thisEvent;
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							//fake event
							initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
									TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "04");

							EventPayloadBean lastPayload = ((ViewChEventBean) currentEvent).getPayload().get(((ViewChEventBean) currentEvent).getPayload().size() - 1);
							long startTsConverted = TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger);
							lastPayload.setEndTs(event.getRefDate());
							long endTsConverted = TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger);
							lastPayload.setDuration(endTsConverted - startTsConverted);

							EventPayloadBean payloadBean = new EventPayloadBean();
							payloadBean.setStartTs(event.getRefDate());

							payloadBean.setxSpeed(((EvChangeViewBean) event).getPlaybackSpeed());
							if (((EvChangeViewBean) event).getPlaybackSpeed() > 0) {
								lastPayload.setActionEnd("12");
								payloadBean.setActionStart("12");
							} else if (((EvChangeViewBean) event).getPlaybackSpeed() < 0) {
								lastPayload.setActionEnd("11");
								payloadBean.setActionStart("11");
							} else {
								lastPayload.setActionEnd("13");
								payloadBean.setActionStart("13");
							}

							((ViewChEventBean) currentEvent).getPayload().add(payloadBean);
							currentState = State.S1RECVIEW;
						}
					} else {
						/*
						 *  Arrived evChangeView on a different channel
						 *  with playback speed and recorded time not null after asset end
						 *  -> creation of fake event
						 *  -> S1RECVIEW
						 */
						cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
						// fake event
						oldCurrentEvent = (SubsEventBean) currentEvent;
						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((ViewChEventBean) currentEvent, (SubsEventBean) oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
								TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "04");
						cutEvent((ViewChEventBean) currentEvent, event, "10");
						// real event
						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(), event.getRefDate(), "01");
						currentState = State.S1RECVIEW;
					}
				}
			} else {
				if ((((EvChangeViewBean) event).getPlaybackSpeed() == null) && (((EvChangeViewBean) event).getRecordedTime() == null)) {
					/*
					 *  Arrived evChangeView without playback speed and recorded time
					 *  before or in the same time of asset end
					 *  -> check of EventTime of the newly arrived event
					 *  -> S0LIVEVIEW
					 */
					if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
						cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
						oldCurrentEvent = ((SubsEventBean)currentEvent);
						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						LOG.debug(Source.ADSMART, "S4 Exception EventTime: " + event.getRefDate());
						initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.NO, null,
								TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "00");
					} else {
						cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "10");
						currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.NO, null, event.getRefDate(), "00");
					}

					currentState = State.S0LIVEVIEW;
				} else {
					if ( sameChannel((EvChangeViewBean) event, (SubsEventBean) currentEvent) ) {
						/*
						 *  Arrived evChangeView on the same channel
						 *  with playback speed and recorded time not null before asset end
						 *  -> closure of last opened SUBS payload
						 *  -> update of adOffset
						 */
						EventPayloadBean lastPayload = ((SubsEventBean) currentEvent).getPayload().get(((SubsEventBean) currentEvent).getPayload().size() - 1);
						EventPayloadBean payloadBean = new EventPayloadBean();

						if (lastPayload.getxSpeed() != ((EvChangeViewBean)event).getPlaybackSpeed()) {

							long startTsConverted = TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger);

							if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
								lastPayload.setEndTs(TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger));
							} else {
								lastPayload.setEndTs(event.getRefDate());
								LOG.debug(Source.ADSMART, "ENDTS: " + lastPayload.getEndTs());
							}
							long endTsConverted = TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger);

							lastPayload.setDuration(endTsConverted - startTsConverted);

							if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
								payloadBean.setStartTs(TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger));
							} else {
								payloadBean.setStartTs(event.getRefDate());
							}	
							payloadBean.setxSpeed(((EvChangeViewBean) event).getPlaybackSpeed());
							LOG.debug(Source.ADSMART, "PLAYBACKSPEED: " + ((EvChangeViewBean)event).getPlaybackSpeed());

							if (((EvChangeViewBean) event).getPlaybackSpeed() > 0) {
								lastPayload.setActionEnd("12");
								payloadBean.setActionStart("12");
							} else if (((EvChangeViewBean) event).getPlaybackSpeed() < 0) {
								lastPayload.setActionEnd("11");
								payloadBean.setActionStart("11");
							} else {
								lastPayload.setActionEnd("13");
								payloadBean.setActionStart("13");
							}

							((SubsEventBean)currentEvent).getPayload().add(payloadBean);
							// adOffset update
							long newAdOffset = ((SubsEventBean) currentEvent).getAdOffset() + ((lastPayload.getDuration()/1000) * Math.abs(lastPayload.getxSpeed()));

							LOG.debug(Source.ADSMART, "NEWADOFFSETBEFORE: " + newAdOffset);

							/*
							 * If fsm is going in pause, there is no need to check the change of direction.
							 * It has to be checked only if fsm is not going in pause.
							 */
							if (payloadBean.getxSpeed() != 0) {
								if (lastPayload.getxSpeed() != 0) {
									// If direction changes, offset changes its meaning
									if (Math.signum(payloadBean.getxSpeed()) != Math.signum(lastPayload.getxSpeed())) {
										newAdOffset = (((SubsEventBean) currentEvent).getCmpDur()) - newAdOffset;
									}
								} else {
									/*
									 * Fsm is in pause, so it is needed to get (if exists) the payload
									 * before pause to see if direction is changing 
									 */

									if (((SubsEventBean) currentEvent).getPayload().size() > 2) {
										EventPayloadBean preLastPayload = ((SubsEventBean) currentEvent).getPayload().get(((SubsEventBean) currentEvent).getPayload().size() - 3);
										if (Math.signum(payloadBean.getxSpeed()) != Math.signum(preLastPayload.getxSpeed())) {
											newAdOffset = (((SubsEventBean) currentEvent).getCmpDur()) - newAdOffset;
										}
									}
								}
							}


							LOG.debug(Source.ADSMART, "NEWADOFFSETDOPO: " + newAdOffset);

							long naturalEnd = 0L;

							/*
							 * If fsm enter in pause (so, if xSpeed of the payload just created is equal to zero),
							 * then naturalEnd doesn't make any sense nomore, because I don't know how long the fsm
							 * will remain in pause. Consequently, if the aforementioned xSpeed is equal to zero,
							 * naturalEnd is setted to a very far date (00:00:00 - 01/01/2100)
							 */
							if (payloadBean.getxSpeed() == 0) {
								naturalEnd = 4102444800000L;
								LOG.debug(Source.ADSMART, "Far Natural End: [" + TimeUtils.long2String(naturalEnd, errorLogger) + "]");
							} else {
								naturalEnd = (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) + ((((((SubsEventBean) currentEvent).getCmpDur()*1000) - newAdOffset*1000))/(Math.abs(payloadBean.getxSpeed()))) + 1000);
								LOG.debug(Source.ADSMART, "NATURALEND: " + naturalEnd);
							}

							((SubsEventBean) currentEvent).setNaturalEnd(naturalEnd);
							((SubsEventBean) currentEvent).setAdOffset(newAdOffset);
						} else {
							if (((SubsEventBean) currentEvent).getPayload().size() == 1 && lastPayload.getxSpeed() == 0 &&
									((TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger) == TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) ||
									 (TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger) == TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)-1000))) {
								LOG.debug(Source.ADSMART, "Pause from S3ADLIVE");
								((SubsEventBean)currentEvent).setRecTime(((EvChangeViewBean) event).getRecordedTime());
								((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).setEndTs(eventIn.getRefDate());
								
								// Adding a fake payload to step over first check of state S4
								String fakeStartTs = ((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).getStartTs();
								String fakeEndTs = ((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).getEndTs();
								String fakeActStart = ((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).getActionStart();
								String fakeActEnd = ((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).getActionEnd();
								((SubsEventBean)currentEvent).getPayload().add(new EventPayloadBean(fakeStartTs, fakeEndTs, 0, 0, fakeActStart, fakeActEnd));
							} else {
								stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
								errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
								isDiscarded = true;
								LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
							}
						}

						currentState = State.S4ADREC;
					} else {
						/*
						 *  Arrived evChangeView on a different channel
						 *  with playback speed and recorded time not null before or in the same time of asset end
						 *  -> S1RECVIEW
						 */
						if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
							oldCurrentEvent = ((SubsEventBean)currentEvent);
							cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(),
									TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "01");
						} else {
							cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "10");
							currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((ViewChEventBean) currentEvent, (EvChangeViewBean) event, ConfigKeys.YES, ((EvChangeViewBean) event).getRecordedTime(), event.getRefDate(), "01");
						}

						currentState = State.S1RECVIEW;
					}
				}
			}
		} else if (event instanceof EvVODPlaybackBean) {
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				/*
				 * Arrived evVODPlayback after asset end
				 * -> creation of fake event
				 * -> S2VODVIEW
				 */
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, (SubsEventBean) oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
						TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "10");

				// real event
				currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
				currentState = State.S2VODVIEW;
			} else {
				/*
				 * Arrived evVODPlayback before or in the same time of asset end
				 * -> S2VODVIEW
				 */
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {

					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
					oldCurrentEvent = (SubsEventBean) currentEvent;
					currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "02");
				} else {
					cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "10");
					currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
					initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
				}
				currentState = State.S2VODVIEW;
			}
		} else if (event instanceof EvAdSmartBean) {
			String lastTunerID = ((SubsEventBean)currentEvent).getTunerID() != null ? ((SubsEventBean)currentEvent).getTunerID() : null;
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				/*
				 * Arrived evAdSmart after asset end
				 * -> creation of fake event
				 * -> check of startOffset of the last arrived event and playback speed of previous event
				 * 		to know if fsm has to go in S3 or in S4, or if it has to  treat the newly arrived event
				 * 		as an unexpected event
				 */
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, (SubsEventBean) oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
						TimeUtils.long2String(oldCurrentEvent.getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "03");

				int lastSpeed = ((ViewChEventBean)currentEvent).getPayload().get(((ViewChEventBean)currentEvent).getPayload().size()-1).getxSpeed();
				String lastRecTime = ((ViewChEventBean)currentEvent).getRecTime();

				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
				// real event
				if ( sameChannel((EvAdSmartBean)event, oldCurrentEvent) ) {

					if (((EvAdSmartBean)event).getStartOffset() == 0) {
						if (lastSpeed > 0) {
							initEvent((SubsEventBean)currentEvent, (EvAdSmartBean)event, ConfigKeys.YES, lastRecTime, event.getRefDate(), "03", lastSpeed, lastTunerID);
							currentState = State.S4ADREC;
						} else {			
							initEvent((SubsEventBean)currentEvent, (EvAdSmartBean)event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
							currentState = State.S3ADLIVE;
							errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
							LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
						}
					} else {
						if ((((EvAdSmartBean)event).getStartOffset() == ((EvAdSmartBean)event).getDuration()) && lastSpeed < 0) {
							initEvent((SubsEventBean)currentEvent, (EvAdSmartBean)event, ConfigKeys.YES, lastRecTime, event.getRefDate(), "03", lastSpeed, lastTunerID);
							currentState = State.S4ADREC;
						} else {
							initEvent((SubsEventBean)currentEvent, (EvAdSmartBean)event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
							currentState = State.S4ADREC;
							errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
							LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart with invalid startOffset. EventTime: " + event.getRefDate());
						}
					}
					initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, oldCurrentEvent.getRecTime(), event.getRefDate(), "03",
							oldCurrentEvent.getPayload().get(oldCurrentEvent.getPayload().size() - 1).getxSpeed(), lastTunerID);
					currentState = State.S4ADREC;
				} else {
					if (((EvAdSmartBean) event).getStartOffset() == 0) {
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
						currentState = State.S3ADLIVE;
					} else {
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
						currentState = State.S4ADREC;
					}

					errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. evAdSmart have to be on same channel. EventTime: " + event.getRefDate());
				}

			} else {
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {

					int lastSpeed = ((SubsEventBean)currentEvent).getPayload().get(((SubsEventBean)currentEvent).getPayload().size()-1).getxSpeed();

					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");
					if ( sameChannel((EvAdSmartBean) event, (SubsEventBean) currentEvent)
							&& ((lastSpeed > 0 && ((EvAdSmartBean)event).getStartOffset() == 0) || (lastSpeed < 0 && ((EvAdSmartBean)event).getStartOffset()
									== ((EvAdSmartBean)event).getDuration()))) {
						/*
						 * Back to back advertising substitution
						 */
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, ((SubsEventBean)currentEvent).getRecTime(), TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger),
								"03", lastSpeed, lastTunerID);
						LOG.debug(Source.ADSMART, "Back to back substitution");
						currentState = State.S4ADREC;
					} else {
						errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
						LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Back to back adSmart event unexpected. EventTime: " + event.getRefDate());
						if (((EvAdSmartBean) event).getStartOffset() == 0) {
							currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
							currentState = State.S3ADLIVE;
						} else {
							currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
							initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
							currentState = State.S4ADREC;
						}
					}
				} else {
					/*
					 * Arrived evAdSmart before asset end
					 * -> check of startOffset of the last arrived event
					 * 		to know if fsm has to go in S3 or in S4
					 * ALWAYS A NEGATIVE CASE
					 */
					errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
					LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. New ADSmart arrived before previous ADSmart naturalEnd. EventTime: " + event.getRefDate());
					cutEvent((SubsEventBean)currentEvent, event, event.getRefDate(), "03");
					if (((EvAdSmartBean) event).getStartOffset() == 0) {
						currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
						currentState = State.S3ADLIVE;
					} else {
						currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
						initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
						currentState = State.S4ADREC;
					}
				}
			}
		} else if (event instanceof EvStandbyInBean) {
			if ((TimeUtils.UTCstring2long(event.getRefDate(), errorLogger)) >= ((SubsEventBean) currentEvent).getNaturalEnd()) {
				/*
				 * Arrived evStandbyIn after asset end
				 * -> creation of fake event
				 * -> S5STANDBY
				 */
				cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "04");

				// fake event
				oldCurrentEvent = (SubsEventBean) currentEvent;
				currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((ViewChEventBean) currentEvent, oldCurrentEvent, ConfigKeys.YES, oldCurrentEvent.getRecTime(),
						TimeUtils.long2String(((SubsEventBean) oldCurrentEvent).getNaturalEnd() - 1000, errorLogger), "04");
				cutEvent((ViewChEventBean) currentEvent, event, "20");
			} else {
				/*
				 * Arrived evStandbyIn before asset end
				 * -> S5STANDBY
				 */
				if (TimeUtils.UTCstring2long(event.getRefDate(), errorLogger) == ((SubsEventBean) currentEvent).getNaturalEnd() - 1000) {
					cutEvent((SubsEventBean) currentEvent, event, TimeUtils.long2String(((SubsEventBean) currentEvent).getNaturalEnd() - 1000, errorLogger), "20");
				} else {
					cutEvent((SubsEventBean) currentEvent, event, event.getRefDate(), "20");
				}
			}

			currentState = State.S5STANDBY;
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}
	}

	protected void processS5StandByInState(EventBean event) {

		boolean isDiscarded = false;

		LOG.debug(Source.ADSMART, "Entered S5 state");

		if (event instanceof EvChangeViewBean) {

			EvChangeViewBean eventIn = (EvChangeViewBean) event;
			currentEvent = new ViewChEventBean(errorLogger, config, ConfigKeys.ADSMART);
			if (eventIn.isLive()) {
				initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.NO, null, event.getRefDate(), "00");
				currentState = State.S0LIVEVIEW;
			} else {
				LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | UNEXPECTED EVENT. evChangeView recorded unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());
				errorLogger.write(Source.ADSMART, ErrorCode.CV_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evChangeView recorded unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());
				initEvent((ViewChEventBean) currentEvent, eventIn, ConfigKeys.YES, eventIn.getRecordedTime(), event.getRefDate(), "01");
				currentState = State.S1RECVIEW;
			}

		} else if (event instanceof EvVODPlaybackBean) {

			LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | UNEXPECTED EVENT. evVODPlayback unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());
			errorLogger.write(Source.ADSMART, ErrorCode.VOD_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evVODPlayback unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());
			currentEvent = new ViewVODEventBean(errorLogger, config, ConfigKeys.ADSMART);
			initEvent((ViewVODEventBean) currentEvent, (EvVODPlaybackBean) event, event.getRefDate(), "02");
			currentState = State.S2VODVIEW;

		} else if (event instanceof EvAdSmartBean) {

			if (((EvAdSmartBean) event).getStartOffset() == 0) {
				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.NO, null, event.getRefDate(), "03", 1, null);
				currentState = State.S3ADLIVE;
			} else {
				currentEvent = new SubsEventBean(errorLogger, config, ConfigKeys.ADSMART);
				initEvent((SubsEventBean) currentEvent, (EvAdSmartBean) event, ConfigKeys.YES, event.getRefDate(), event.getRefDate(), "03", 1, null);
				currentState = State.S4ADREC;
			}

			LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | UNEXPECTED EVENT. evAdSmart unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());
			errorLogger.write(Source.ADSMART, ErrorCode.ADSMART_UNEXPECTED, "SmartcardID: "+ event.getCardID() + " | evAdSmart unexpected in S5StandbyIn state. EventTime: " + event.getRefDate());			

		} else if (event instanceof EvStandbyInBean) {
			// caso evento standby
			stats.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
			errorLogger.write(Source.ADSMART, ErrorCode.DUPLICATED_EVENT, "SmartcardID: "+ event.getCardID() + " | EventTime: " + event.getRefDate());
			isDiscarded = true;
			LOG.warn(Source.ADSMART, "SmartcardID: "+ event.getCardID() + " | NEGATIVE CASE. Duplicated event, discarded. EventTime: " + event.getRefDate());
		}

		if (!isDiscarded) {
			stats.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
		}

	}

	
	// Questi metodi indicano se due bean fanno riferimento allo stesso canale
	private boolean sameChannel(EvChangeViewBean eventIn, ViewChEventBean eventOut) {
		return eventIn.getServiceKey().equals(eventOut.getServiceKey())
				&& eventIn.getOriginalNetworkID().equals(eventOut.getOriginalNetworkID())
				&& eventIn.getTransportStreamID().equals(eventOut.getTransportStreamID())
				&& eventIn.getSiServiceID().equals(eventOut.getSiServiceID());
	}
	private boolean sameChannel(EvAdSmartBean eventIn, SubsEventBean eventOut) {
		return eventIn.getServiceKey().equals(eventOut.getServiceKey())
				&& eventIn.getOriginalNetworkID().equals(eventOut.getOriginalNetworkID())
				&& eventIn.getTransportStreamID().equals(eventOut.getTransportStreamID())
				&& eventIn.getSiServiceID().equals(eventOut.getSiServiceID());
	}
	private boolean sameChannel(EvChangeViewBean eventIn, SubsEventBean eventOut) {
		return eventIn.getServiceKey().equals(eventOut.getServiceKey())
				&& eventIn.getOriginalNetworkID().equals(eventOut.getOriginalNetworkID())
				&& eventIn.getTransportStreamID().equals(eventOut.getTransportStreamID())
				&& eventIn.getSiServiceID().equals(eventOut.getSiServiceID());
	}
	private boolean sameChannel(EvAdSmartBean eventIn, ViewChEventBean eventOut) {
		return eventIn.getServiceKey().equals(eventOut.getServiceKey())
				&& eventIn.getOriginalNetworkID().equals(eventOut.getOriginalNetworkID())
				&& eventIn.getTransportStreamID().equals(eventOut.getTransportStreamID())
				&& eventIn.getSiServiceID().equals(eventOut.getSiServiceID());
	}

	/**
	 * Fill in the missing fields of ViewChEvent.
	 * 
	 * @param eventOut
	 *            the last opened output VIEWCH structure
	 * @param eventIn
	 *            The new event coming from SAXParser containing all the
	 *            required field to cut event and to send it to the
	 *            FileBufferWriter.
	 * @param actionCodeEnd
	 *            the code that represents the motivation why a new output event
	 *            has been cut.
	 * 
	 */
	private void cutEvent(ViewChEventBean eventOut, EventBean eventIn, String actionCodeEnd) {

		long totalViewing = 0L;

		EventPayloadBean lastPayload = eventOut.getPayload().get(eventOut.getPayload().size() - 1);
		lastPayload.setEndTs(eventIn.getRefDate());
		lastPayload.setDuration((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger)));
		lastPayload.setActionEnd(actionCodeEnd);
		if (lastPayload.getxSpeed() != 0) {
			eventOut.setObsTime(((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(eventOut.getPayload().get(0).getStartTs(), errorLogger))) / 1000);
		} else {
			if (eventOut.getPayload().size() > 1) {
				eventOut.setObsTime(((TimeUtils.UTCstring2long(eventOut.getPayload().get(eventOut.getPayload().size() - 2).getEndTs(), errorLogger))
						- (TimeUtils.UTCstring2long(eventOut.getPayload().get(0).getStartTs(), errorLogger))) / 1000);
			}
		}

		for (int i = 0; i < eventOut.getPayload().size(); i++) {

			if (eventOut.getPayload().get(i).getxSpeed() != 0) {
				totalViewing += eventOut.getPayload().get(i).getDuration();
			}
		}
		eventOut.setTotalViewing(totalViewing);

		if (!(eventOut.getPayload().size() == 1 && lastPayload.getxSpeed() == 0)) {
			OutputMessage message = new EventPrinterHelper<ViewChEventBean>(eventOut)
					.createOutputMessage(ViewChEventBean.EVENT_NAME);
			eventsToWrite.add(message);
			viewChCounter++;
		}
	}

	/**
	 * Fill in the missing fields of ViewVODEvent.
	 * 
	 * @param eventOut
	 *            the last opened output VIEWVOD structure
	 * @param eventIn
	 *            The new event coming from SAXParser containing all the
	 *            required field to cut event and to send it to the
	 *            FileBufferWriter.
	 * @param actionCodeEnd
	 *            the code that represents the motivation why a new output event
	 *            has been cut.
	 */
	private void cutEvent(ViewVODEventBean eventOut, EventBean eventIn, String actionCodeEnd) {

		long totalViewing = 0L;
		EventPayloadBean lastPayload = eventOut.getPayload().get(eventOut.getPayload().size() - 1);

		lastPayload.setEndTs(eventIn.getRefDate());
		lastPayload.setDuration((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger)));
		lastPayload.setActionEnd(actionCodeEnd);
		eventOut.setObsTime(((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(eventOut.getPayload().get(0).getStartTs(), errorLogger))) / 1000);

		for (int i = 0; i < eventOut.getPayload().size(); i++) {

			if (eventOut.getPayload().get(i).getxSpeed() != 0) {
				totalViewing += eventOut.getPayload().get(i).getDuration();
			}
		}
		eventOut.setTotalViewing(totalViewing);

		OutputMessage message = new EventPrinterHelper<ViewVODEventBean>(eventOut)
				.createOutputMessage(ViewVODEventBean.EVENT_NAME);
		eventsToWrite.add(message);
		viewVODCounter++;
	}

	/**
	 * Fill in the missing fields of SubsEvent.
	 * 
	 * @param eventOut
	 *            the last opened output SUBS structure
	 * @param eventIn
	 *            The new event coming from SAXParser containing all the
	 *            required field to cut event and to send it to the
	 *            FileBufferWriter.
	 * @param timestampEnd
	 *            represents the end of an asset (equals timestampStart +
	 *            assetDuration)
	 * @param actionCodeEnd
	 *            the code that represents the motivation why a new output event
	 *            has been cut.
	 */
	private void cutEvent(SubsEventBean eventOut, EventBean eventIn, String timestampEnd, String actionCodeEnd) {

		long totalViewing = 0L;

		EventPayloadBean lastPayload = eventOut.getPayload().get(eventOut.getPayload().size() - 1);
		lastPayload.setEndTs(timestampEnd);
		lastPayload.setDuration((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(lastPayload.getStartTs(), errorLogger)));
		lastPayload.setActionEnd(actionCodeEnd);

		if (lastPayload.getxSpeed() != 0) {
			eventOut.setObsTime(((TimeUtils.UTCstring2long(lastPayload.getEndTs(), errorLogger)) - (TimeUtils.UTCstring2long(eventOut.getPayload().get(0).getStartTs(), errorLogger))) / 1000);
		} else {
			if (eventOut.getPayload().size() > 1) {
				eventOut.setObsTime(((TimeUtils.UTCstring2long(eventOut.getPayload().get(eventOut.getPayload().size() - 2).getEndTs(), errorLogger))
						- (TimeUtils.UTCstring2long(eventOut.getPayload().get(0).getStartTs(), errorLogger))) / 1000);
			}
		}

		for (int i = 0; i < eventOut.getPayload().size(); i++) {

			if (eventOut.getPayload().get(i).getxSpeed() != 0) {
				totalViewing += eventOut.getPayload().get(i).getDuration();
			}
		}

		eventOut.setCmpView(totalViewing);

		// counter for payloads with speed equals to zero
		int zeroSpeedPayloadCounter = 0;
		for (int i = 0; i < eventOut.getPayload().size(); i++) {
			if (eventOut.getPayload().get(i).getxSpeed() == 0) {
				zeroSpeedPayloadCounter++;
			}
		}

		if (zeroSpeedPayloadCounter != eventOut.getPayload().size()) {
			if (!(eventOut.getPayload().size() == 1 && lastPayload.getxSpeed() == 0)) {
				OutputMessage message = new EventPrinterHelper<SubsEventBean>(eventOut)
						.createOutputMessage(SubsEventBean.EVENT_NAME);
				eventsToWrite.add(message);
				subsCounter++;
			}
		}
	}

	/**
	 * Initialize the new created output ViewChEvent filling in the available
	 * fields.
	 * 
	 *
	 * @param eventOut
	 *            the output event
	 * @param eventIn
	 *            the upcoming input event
	 * @param type
	 *            the output event type (VIEWCH, VIEWVOD, SUBS)
	 * @param pbflg
	 *            the playback flag
	 * @param recTime
	 *            the recorded time if playbackflag is setted to Y If not it
	 *            will not appear in the output event
	 * @param timeStampStart
	 *            the time of the input event who generated the output event
	 * @param actionCodeStart
	 *            the code that represents the motivation why a new output event
	 *            has been created
	 */
	private void initEvent(ViewChEventBean eventOut, EvChangeViewBean eventIn, String pbflg, String recTime, String timeStampStart, String actionCodeStart) {

		eventOut.setCardID(eventIn.getCardID());
		eventOut.setSubscriberID(eventIn.getSubscriberID());
		eventOut.setPanelID(eventIn.getPanelID());
		eventOut.setReportID(eventIn.getReportID());
		eventOut.setTunerID(eventIn.getTunerID());

		eventOut.setPayload(new ArrayList<EventPayloadBean>());
		EventPayloadBean payload = new EventPayloadBean();
		payload.setStartTs(timeStampStart);

		eventOut.setChType(((EvChangeViewBean) eventIn).getChType());
		if (((EvChangeViewBean) eventIn).getPlaybackSpeed() != null) {
			payload.setxSpeed(((EvChangeViewBean) eventIn).getPlaybackSpeed());
		} else {
			payload.setxSpeed(1);
		}
		eventOut.setOriginalNetworkID(((EvChangeViewBean) eventIn).getOriginalNetworkID());
		eventOut.setTransportStreamID(((EvChangeViewBean) eventIn).getTransportStreamID());
		eventOut.setSiServiceID(((EvChangeViewBean) eventIn).getSiServiceID());
		eventOut.setServiceKey(((EvChangeViewBean) eventIn).getServiceKey());

		payload.setActionStart(actionCodeStart);
		eventOut.getPayload().add(payload);

		eventOut.setRefDate(eventOut.getPayload().get(0).getStartTs());
		eventOut.setPbflg(pbflg);
		eventOut.setRecTime(((recTime == null) ? "NA" : recTime));

	}

	private void initEvent(ViewChEventBean eventOut, SubsEventBean eventIn, String pbflg, String recTime, String timeStampStart, String actionCodeStart) {

		eventOut.setCardID(eventIn.getCardID());
		eventOut.setSubscriberID(eventIn.getSubscriberID());
		eventOut.setPanelID(eventIn.getPanelID());
		eventOut.setReportID(eventIn.getReportID());

		eventOut.setPayload(new ArrayList<EventPayloadBean>());
		EventPayloadBean payload = new EventPayloadBean();
		payload.setStartTs(timeStampStart);

		eventOut.setChType(eventIn.getChType());
		if (eventIn.getPayload().get(eventIn.getPayload().size() - 1) != null) {
			payload.setxSpeed(eventIn.getPayload().get(eventIn.getPayload().size() - 1).getxSpeed());
		} else {
			payload.setxSpeed(1);
		}
		eventOut.setOriginalNetworkID(eventIn.getOriginalNetworkID());
		eventOut.setTransportStreamID(eventIn.getTransportStreamID());
		eventOut.setSiServiceID(eventIn.getSiServiceID());
		eventOut.setServiceKey(eventIn.getServiceKey());
		
		if (eventIn.getTunerID() != null) {
			eventOut.setTunerID(eventIn.getTunerID());
		}

		payload.setActionStart(actionCodeStart);
		eventOut.getPayload().add(payload);

		eventOut.setRefDate(eventOut.getPayload().get(0).getStartTs());
		eventOut.setPbflg(pbflg);
		eventOut.setRecTime(((recTime == null) ? "NA" : recTime));
	}

	/**
	 * Initialize the new created output ViewVODEvent filling in the available
	 * fields.
	 * 
	 * @param eventOut
	 *            the output event
	 * @param eventIn
	 *            the upcoming input event
	 * @param type
	 *            the output event type (VIEWCH, VIEWVOD, SUBS)
	 * @param pbflg
	 *            the playback flag
	 * @param recTime
	 *            the recorded time if playbackflag is setted to Y If not it
	 *            will not appear in the output event
	 * @param timeStampStart
	 *            the time of the input event who generated the output event
	 * @param actionCodeStart
	 *            the code that represents the motivation why a new output event
	 *            has been created
	 */
	private void initEvent(ViewVODEventBean eventOut, EvVODPlaybackBean eventIn, String timeStampStart, String actionCodeStart) {

		eventOut.setCardID(eventIn.getCardID());
		eventOut.setSubscriberID(eventIn.getSubscriberID());
		eventOut.setPanelID(eventIn.getPanelID());
		eventOut.setReportID(eventIn.getReportID());

		eventOut.setVodID(eventIn.getVodAssetID());
		eventOut.setOffset(eventIn.getStartOffset());
		eventOut.setPayload(new ArrayList<EventPayloadBean>());
		EventPayloadBean payload = new EventPayloadBean();
		payload.setStartTs(eventIn.getRefDate());
		payload.setxSpeed(eventIn.getPlaybackSpeed());
		payload.setActionStart(actionCodeStart);
		eventOut.getPayload().add(payload);

		eventOut.setRefDate(eventOut.getPayload().get(0).getStartTs());
	}


	/**
	 * Initialize the new created output SubsEvent filling in the available
	 * fields.
	 * 
	 * @param eventOut
	 *            the output event
	 * @param eventIn
	 *            the upcoming input event
	 * @param type
	 *            the output event type (VIEWCH, VIEWVOD, SUBS)
	 * @param pbflg
	 *            the playback flag
	 * @param recTime
	 *            the recorded time if playbackflag is setted to Y If not it
	 *            will not appear in the output event
	 * @param timeStampStart
	 *            the time of the input event who generated the output event
	 * @param actionCodeStart
	 *            the code that represents the motivation why a new output event
	 *            has been created
	 */
	private void initEvent(SubsEventBean eventOut, EvAdSmartBean eventIn, String pbflg, String recTime, String timeStampStart, String actionCodeStart, int speed, String tunerID) {

		eventOut.setCardID(eventIn.getCardID());
		eventOut.setSubscriberID(eventIn.getSubscriberID());
		eventOut.setPanelID(eventIn.getPanelID());
		eventOut.setReportID(eventIn.getReportID());

		eventOut.setOriginalNetworkID(eventIn.getOriginalNetworkID());
		eventOut.setTransportStreamID(eventIn.getTransportStreamID());
		eventOut.setSiServiceID(eventIn.getSiServiceID());
		eventOut.setServiceKey(eventIn.getServiceKey());
		eventOut.setCmpID((eventIn.getCampaignID()));
		eventOut.setStartOffset(eventIn.getStartOffset());
		
		// Kept here to use it in fake events
		eventOut.setTunerID(tunerID);

		if (speed < 0) {
			((SubsEventBean)currentEvent).setAdOffset(eventIn.getDuration() - eventIn.getStartOffset());
		} else {
			((SubsEventBean)currentEvent).setAdOffset(eventIn.getStartOffset());
		}

		((SubsEventBean) currentEvent).setChType(eventIn.getChType());
		((SubsEventBean) currentEvent).setCmpUpID(eventIn.getContentInstanceID());
		((SubsEventBean) currentEvent).setCmpDur(eventIn.getDuration());
		((SubsEventBean) currentEvent).setSlotID(eventIn.getAvailId());
		((SubsEventBean) currentEvent).setPbflg(pbflg);
		if (pbflg.equals(ConfigKeys.YES)) {
			if (eventIn.getStartOffset() == 0) {
				((SubsEventBean) currentEvent).setBflg(ConfigKeys.YES);
			} else {
				((SubsEventBean) currentEvent).setBflg(ConfigKeys.NO);
			}
		}
		((SubsEventBean) currentEvent).setRecTime(((recTime == null) ? "NA" : recTime));
		((SubsEventBean) currentEvent).setPayload(new ArrayList<EventPayloadBean>());
		EventPayloadBean payload = new EventPayloadBean();
		payload.setStartTs(eventIn.getRefDate());
		payload.setxSpeed(speed);
		payload.setActionStart(actionCodeStart);
		((SubsEventBean) currentEvent).getPayload().add(payload);

		eventOut.setRefDate(eventOut.getPayload().get(0).getStartTs());

		long naturalEnd = 0L;

		if (speed == 0) {
			naturalEnd = 4102444800000L;
			LOG.debug(Source.ADSMART, "Far Natural End: [" + TimeUtils.long2String(naturalEnd, errorLogger) + "]");
		} else {

			naturalEnd = 1000*(((TimeUtils.UTCstring2long(((SubsEventBean) currentEvent).getRefDate(), errorLogger) 
					+ (((SubsEventBean) currentEvent).getCmpDur()*1000
							- ((SubsEventBean) currentEvent).getAdOffset()*1000)/(Math.abs(speed))) + 1000)/1000);
		}

		eventOut.setNaturalEnd(naturalEnd);
	}


	public int getViewChCounter() {
		return viewChCounter;
	}

	public void setViewChCounter(int viewChCounter) {
		this.viewChCounter = viewChCounter;
	}

	public int getSubsCounter() {
		return subsCounter;
	}

	public void setSubsCounter(int subsCounter) {
		this.subsCounter = subsCounter;
	}

	public int getViewVODCounter() {
		return viewVODCounter;
	}

	public void setViewVODCounter(int viewVODCounter) {
		this.viewVODCounter = viewVODCounter;
	}

	public EventBean getCurrentEvent() {
		return currentEvent;
	}

	public void setCurrentEvent(EventBean currentEvent) {
		this.currentEvent = currentEvent;
	}
}
