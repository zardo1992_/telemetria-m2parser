package com.nttdata.admeter.parser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.nttdata.admeter.utils.LOG;

public class SAXParserWrapper {

	final
	private SAXParserFactory	parserFactory;
	
	final
	private SchemaFactory		schemaFactory;
	
	
	public SAXParserWrapper() {
		this(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	}
	
	public SAXParserWrapper(String language) {
		this.parserFactory = SAXParserFactory.newInstance();
		this.parserFactory.setNamespaceAware(true);
		
		this.schemaFactory = SchemaFactory.newInstance(language);
	}
	
	public void setSchemaFile(File file) throws SAXException {
		Schema schema = this.schemaFactory.newSchema(
			new Source[] { new StreamSource(file) }
		);
		this.parserFactory.setSchema(schema);
	}
	
	public boolean isValidXMLString(String xmlString) {
		
		Source source = new StreamSource(new StringReader(xmlString));
		
		Validator validator = this.parserFactory.getSchema().newValidator();
		try {
			validator.validate(source);
			return true;
		} catch (SAXException | IOException e) {
			LOG.error("validation exception: " + e.getMessage());
			return false;
		}
	}
	
	public void parse(String xmlString, DefaultHandler handler) throws Exception {
		SAXParser parser = this.parserFactory.newSAXParser();	
		parser.parse(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)), handler);
	}
	
}
