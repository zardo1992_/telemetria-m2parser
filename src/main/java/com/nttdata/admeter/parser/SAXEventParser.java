package com.nttdata.admeter.parser;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.nttdata.admeter.channel.ChannelBean;
import com.nttdata.admeter.comparator.LongComparator;
import com.nttdata.admeter.config.check.ConfigKeys;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constants.M2Statistics;
import com.nttdata.admeter.couchbase.DButils;
import com.nttdata.admeter.event.input.EvAdSmartBean;
import com.nttdata.admeter.event.input.EvAdSmartNoSubsBean;
import com.nttdata.admeter.event.input.EvChangeViewBean;
import com.nttdata.admeter.event.input.EvStandbyInBean;
import com.nttdata.admeter.event.input.EvStandbyOutBean;
import com.nttdata.admeter.event.input.EvSurfBean;
import com.nttdata.admeter.event.input.EvTunerActivityBean;
import com.nttdata.admeter.event.input.EvVODPlaybackBean;
import com.nttdata.admeter.event.input.EventBean;
import com.nttdata.admeter.event.output.EventPrinterHelper;
import com.nttdata.admeter.event.output.STBReportBean;
import com.nttdata.admeter.event.output.StandByEventBean;
import com.nttdata.admeter.event.output.SubsEventBean;
import com.nttdata.admeter.event.output.SurfBean;
import com.nttdata.admeter.event.output.ViewChEventBean;
import com.nttdata.admeter.event.output.ViewVODEventBean;
import com.nttdata.admeter.event.payload.STBPayloadBean;
import com.nttdata.admeter.fsm.FiniteStateMachine;
import com.nttdata.admeter.fsm.FiniteStateMachine.State;
import com.nttdata.admeter.fsm.FiniteStateMachineAdSmart;
import com.nttdata.admeter.fsm.FiniteStateMachineEthan;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.output.FileBufferWriter;
import com.nttdata.admeter.output.OutputMessage;
import com.nttdata.admeter.parser.interfaces.IChannelable;
import com.nttdata.admeter.runnable.StatParserRunnable;
import com.nttdata.admeter.runnable.StatusRunnable;
import com.nttdata.admeter.utils.ChannelDateComparator;
import com.nttdata.admeter.utils.JsonUtils;
import com.nttdata.admeter.utils.LOG;
import com.nttdata.admeter.utils.TimeUtils;

public class SAXEventParser extends DefaultHandler {

	final
	private String	source;
	
	protected ErrorLogger errorLogger;
	protected IConfig config;
	
	protected StatParserRunnable statisticRunnable;
	protected StatusRunnable statusRunnable;

	private FileBufferWriter fbw;

	private StringBuffer sBuffer = new StringBuffer();

	private EventBean inputEvent;
	private EventBean stbEvent;

	private String cardID;
	private String subscriberID;
	private String deviceID;
	private String deviceType;
	
	private String panelID;
	
	private String dateXML;
	private String dateLog;
	private long lastEventTime = 0L;

	private long startProcessing;

	private FiniteStateMachine currentFSM;

	private ArrayList<Long> changeViewList = new ArrayList<>();
	private ArrayList<Long> adSmartList = new ArrayList<>();
	private ArrayList<Long> noSubsList = new ArrayList<>();
	private ArrayList<Long> vodPlaybackList = new ArrayList<>();
	private ArrayList<Long> standbyInList = new ArrayList<>();
	private ArrayList<Long> standbyOutList = new ArrayList<>();
	private ArrayList<Long> tunerActivityList = new ArrayList<>();
	private ArrayList<Long> surfList = new ArrayList<>();

	private long receivedDate;
	private int noSubsOutCounter = 0;
	private int tunerActivityCounter = 0;
//	private int surfCounter = 0;

	protected List<OutputMessage> eventsToWrite = new ArrayList<>();

	/**
	 * A SAXParser
	 * @param logger
	 * @param errorLogger
	 * @param config
	 * @param fbw
	 * @param statisticRunnable
	 * @param statusRunnable
	 * @param receivedDate
	 */
	public SAXEventParser(String source, ErrorLogger errorLogger,
			IConfig config, FileBufferWriter fbw,
			StatParserRunnable statisticRunnable,
			StatusRunnable statusRunnable, long receivedDate) {
		super();
		this.source = source;
		this.errorLogger = errorLogger;
		this.config = config;
		this.fbw = fbw;
		this.statisticRunnable = statisticRunnable;
		this.statusRunnable = statusRunnable;
		this.receivedDate = receivedDate;
		this.currentFSM = getFiniteStateMachine();

	}

	protected FiniteStateMachine getFiniteStateMachine() {
		return this.source.equalsIgnoreCase(ConfigKeys.ADSMART) ?
				new FiniteStateMachineAdSmart(errorLogger, config, statisticRunnable, eventsToWrite) :
				new FiniteStateMachineEthan(errorLogger, config, statisticRunnable, eventsToWrite);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		switch (qName) {
		case "Subscriber":
			onSubscriberStart(attributes);
			break;

		case "SkyAdSmartPanel":
		case "SkyPanel":
			onSkyAdSmartPanelStart(attributes);
			break;

		case "OriginalNetworkID":
		case "TransportStreamID":
		case "SIServiceID":
		case "ServiceKey":
		case "campaignId":
		case "contentInstanceId":
		case "availId":
		case "startOffset":
		case "reason":
		case "RecordedTime":
		case "PlaybackSpeed":
		case "VODAssetID":
		case "TunerID":
			onClearStringBuffer();
			break;

		case "DVBTriplet":
			onDVBTriplet(attributes);
			break;
			
		case "evChangeView":
			onEvChangeViewStart(attributes);
			break;

		case "evAdSmartNoSubstitution":
			onEvAdSmartNoSubstitutionStart(attributes);
			break;

		case "evAdSmart":
			onEvAdSmartStart(attributes);
			break;

		case "evVODPlayback":
			onEvVODPlaybackStart(attributes);
			break;
			
		case "evSurf":
			onEvSurfStart(attributes);
			break;
		
		case "evStandbyIn":
			onEvStandbyInStart(attributes);
			break;
		
		case "evStandbyOut":
			onEvStandbyOutStart(attributes);
			break;

		case "evTunerActivity":
			onEvTunerActivityStart(attributes);
			break;
			
		case "evEPGAction":	
		case "evLinearRecord":
		case "evABREvent":
		case "evAppDataEvent":
		case "evAppStartEvent":
		case "evAppExitEvent":			
		case "PrivateData":
			onEventToLogAndDiscard(qName);
			break;
			
		default:
			break;
		}
	}
	
	protected void onDVBTriplet(Attributes attributes) {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setOriginalNetworkID(attributes.getValue("OriginalNetworkID"));
			((EvAdSmartBean) inputEvent).setTransportStreamID(attributes.getValue("TransportStreamID"));
			((EvAdSmartBean) inputEvent).setSiServiceID(attributes.getValue("SIServiceID"));
		} else if (inputEvent instanceof EvAdSmartNoSubsBean) {
			((EvAdSmartNoSubsBean) inputEvent).setOriginalNetworkID(attributes.getValue("OriginalNetworkID"));
			((EvAdSmartNoSubsBean) inputEvent).setTransportStreamID(attributes.getValue("TransportStreamID"));
			((EvAdSmartNoSubsBean) inputEvent).setSiServiceID(attributes.getValue("SIServiceID"));
		} else if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent).setOriginalNetworkID(attributes.getValue("OriginalNetworkID"));
			((EvChangeViewBean) inputEvent).setTransportStreamID(attributes.getValue("TransportStreamID"));
			((EvChangeViewBean) inputEvent).setSiServiceID(attributes.getValue("SIServiceID"));
		} else if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setOriginalNetworkID(attributes.getValue("OriginalNetworkID"));
			((EvTunerActivityBean) inputEvent).setTransportStreamID(attributes.getValue("TransportStreamID"));
			((EvTunerActivityBean) inputEvent).setSiServiceID(attributes.getValue("SIServiceID"));
		}
	}

	protected void onEvStandbyInStart(Attributes attributes) {
		inputEvent = new EvStandbyInBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		standbyInList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}
	
	protected void onEvStandbyOutStart(Attributes attributes) {
		inputEvent = new EvStandbyOutBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		standbyOutList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}
	
	protected void onEvSurfStart(Attributes attributes) {
		inputEvent = new EvSurfBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		surfList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}

	protected void onEvVODPlaybackStart(Attributes attributes) {
		inputEvent = new EvVODPlaybackBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		vodPlaybackList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}

	protected void onEvAdSmartStart(Attributes attributes) {
		inputEvent = new EvAdSmartBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		adSmartList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}

	protected void onEvAdSmartNoSubstitutionStart(Attributes attributes) {
		inputEvent = new EvAdSmartNoSubsBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		noSubsList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}

	protected void onEvChangeViewStart(Attributes attributes) {
		inputEvent = new EvChangeViewBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		changeViewList.add(TimeUtils.UTCstring2long(
				attributes.getValue("EventTime"), errorLogger));
	}
	
	protected void onEvTunerActivityStart(Attributes attributes) {
		inputEvent = new EvTunerActivityBean(errorLogger, config, source);
		initEvent(inputEvent, attributes);
		checkLastEventTime(attributes);
		tunerActivityList.add(TimeUtils.UTCstring2long(attributes.getValue("EventTime"), errorLogger));
	}

	protected void onClearStringBuffer() {
		sBuffer = new StringBuffer();
	}

	private void onSkyAdSmartPanelStart(Attributes attributes) {
		panelID = attributes.getValue("PanelID");
		dateXML = TimeUtils.UTC2LocalTime(
				attributes.getValue("DocumentCreationDate"), "CET",
				errorLogger);
		dateLog = TimeUtils.UTC2LocalTime(
				attributes.getValue("STBLogCreationDate"), "CET",
				errorLogger);
	}

	protected void onSubscriberStart(Attributes attributes) throws SAXException {
		// Retrieving cardID and subscriberID
		cardID = attributes.getValue("CardId");
		subscriberID = attributes.getValue("SubscriberID");
		deviceID = attributes.getValue("DeviceID");
		
		startProcessing = System.currentTimeMillis();

		// STBReportEvent initialization
		stbEvent = new STBReportBean(errorLogger, config, source);
		initEvent(stbEvent, attributes);

		// Retrieving last event structure saved in Couchbase (if exists and
		// if statusRecovery
		// parameter is setted to "ON")
		if (config.getString(ConfigKeys.RUNTIME.STATUS_RECOVERY)
				.equalsIgnoreCase(ConfigKeys.YES)) {
			JsonDocument dbValue;
			JsonObject content = null;
			String couchbaseID = "";
			if (source.equals(ConfigKeys.ADSMART)) {
				couchbaseID = subscriberID + "-" + cardID;
			} else {
				couchbaseID = subscriberID + "-" + cardID + "-" + deviceID;
			}
			try {
				dbValue = DButils.readState(couchbaseID, source, config, errorLogger);
				content = dbValue.content();
				LOG.info(source, "ID: " + couchbaseID + " found and correctly retrieved from Couchbase.");

				currentFSM.setCurrentState(
						State.valueOf(content.getString("state")));
				
				if (content.getString("event") != null) {
					if (content.getString("event").equals("VIEWCH")) {
						ViewChEventBean viewChToRead = new ViewChEventBean(errorLogger, config, source);
						viewChToRead.readFromJson(content);
						currentFSM.setCurrentEvent(viewChToRead);
					} else if (content.getString("event").equals("SUBS")) {
						SubsEventBean subsToRead = new SubsEventBean(errorLogger, config, source);
						subsToRead.readFromJson(content);
						currentFSM.setCurrentEvent(subsToRead);
					} else if (content.getString("event").equals("VIEWVOD")) {
						ViewVODEventBean viewVodToRead = new ViewVODEventBean(errorLogger, config, source);
						viewVodToRead.readFromJson(content);
						currentFSM.setCurrentEvent(viewVodToRead);
					} else if (content.getString("event").equals("STANDBY")) {
						StandByEventBean standbyToRead = new StandByEventBean(errorLogger, config, source);
						standbyToRead.readFromJson(content);
						currentFSM.setCurrentEvent(standbyToRead);
					} else if (content.getString("event").equals("SURF")) {
						SurfBean surfToRead = new SurfBean(errorLogger, config, source);
						surfToRead.readFromJson(content);
						currentFSM.setCurrentEvent(surfToRead);
					}
					
					// Retrieving EventTime of the event came before the
					// upcoming event.
					// If the last event has no payloads I set the event's
					// timestamp as
					// lastEventTime.
					JsonArray lastPayloadArray = null;
					lastPayloadArray = content.getArray("payload").getArray(content.getArray("payload").size() - 1);
					if (lastPayloadArray.getString(0) != null) {
						lastEventTime = TimeUtils.UTCstring2long(lastPayloadArray.getString(0), errorLogger);
					} else {
						LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Payload not found on couchbase" :
																		 "DeviceID: " + deviceID + " | Payload not found on couchbase");
					}
				}
			} catch (NullPointerException npe) {
				// the NPE is thrown when the searched id is not found
				currentFSM.setCurrentState(State.START);
				LOG.warn(source, "ID: " + couchbaseID + " not found in Couchbase. FSM will start in the START state.");
			} catch (Exception e) {
			    throw new SAXException(e);
			}

		} else {
			currentFSM.setCurrentState(State.START);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		switch (qName) {

		case "Subscriber":
			// Setting end processing time and start processing time
			onSubscriberEnd();
			break;

		case "evAdSmart":
		case "evAdSmartNoSubstitution":
		case "evTunerActivity":
		case "evChangeView":
			channelDataRetrieve((IChannelable)inputEvent);
			break;

		case "evStandbyIn":
			onEvStandbyInEnd();
			break;
		
		case "evStandbyOut":
			onEvStandbyOutEnd();
			break;
			
		case "evSurf":
			onEvSurfEnd();
			break;

		case "evVODPlayback":
			onEvVODPlaybackEnd();
			break;

		case "OriginalNetworkID":
			onOriginalNetworkIDEnd();
			break;

		case "TransportStreamID":
			onTransportStreamIDEnd();
			break;

		case "SIServiceID":
			onSIServiceIDEnd();
			break;

		case "ServiceKey":
			onServiceKeyEnd();
			break;

		case "campaignId":
			onCampaignIdEnd();
			break;

		case "contentInstanceId":
			onContentInstanceIdEnd();
			break;

		case "availId":
			onAvailIdEnd();
			break;

		case "startOffset":
			onStartOffsetEnd();
			break;

		case "reason":
			onReasonEnd();
			break;

		case "RecordedTime":
			onRecordedTimeEnd();
			break;

		case "PlaybackSpeed":
			onPlaybackSpeedEnd();
			break;

		case "VODAssetID":
			onVODAssetIDEnd();
			break;

		case "TunerID":
			onTunerIDEnd();
			break;
			
		default:
			break;
		}
	}
	
	protected void onEventToLogAndDiscard(String eventType) {
		LOG.info(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Received and discarded event of type: " + eventType :
														 "DeviceID: " + deviceID + " | Received and discarded event of type: " + eventType);
	}

	protected void onVODAssetIDEnd() {
		((EvVODPlaybackBean) inputEvent).setVodAssetID(sBuffer.toString());
	}
	
	protected void onTunerIDEnd() {
		if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setTunerID(sBuffer.toString());
		} else if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent).setTunerID(sBuffer.toString());
		}
	}

	protected void onPlaybackSpeedEnd() {
		if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent)
					.setPlaybackSpeed(retrieveSpeed(sBuffer.toString()));
		} else if (inputEvent instanceof EvVODPlaybackBean) {
			((EvVODPlaybackBean) inputEvent)
					.setPlaybackSpeed(retrieveSpeed(sBuffer.toString()));
		}
	}

	protected void onRecordedTimeEnd() {
		((EvChangeViewBean) inputEvent).setRecordedTime(sBuffer.toString());
	}

	protected void onReasonEnd() {
		((EvAdSmartNoSubsBean) inputEvent)
				.setReason(sBuffer.toString());
	}

	protected void onStartOffsetEnd() {
		if (inputEvent instanceof EvVODPlaybackBean) {
			((EvVODPlaybackBean) inputEvent)
					.setStartOffset(Integer.parseInt(sBuffer.toString()));
		} else if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent)
					.setStartOffset(Integer.parseInt(sBuffer.toString()));
		}
	}

	protected void onAvailIdEnd() {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setAvailId(sBuffer.toString());
		} else if (inputEvent instanceof EvAdSmartNoSubsBean)
			((EvAdSmartNoSubsBean) inputEvent).setAvailID(sBuffer.toString());
	}

	protected void onContentInstanceIdEnd() {
		((EvAdSmartBean) inputEvent).setContentInstanceID(sBuffer.toString());
	}

	protected void onCampaignIdEnd() {
		((EvAdSmartBean) inputEvent).setCampaignID(sBuffer.toString());
		if (DButils.isCampaignExistent(String.valueOf(((EvAdSmartBean) inputEvent).getCampaignID()), source)) {
			((EvAdSmartBean) inputEvent)
			.setDuration(DButils.getDurationFromCampaign(String.valueOf(
					((EvAdSmartBean) inputEvent).getCampaignID()), source));
			LOG.info(source, "CampaignID: ["
					+ ((EvAdSmartBean) inputEvent).getCampaignID()
					+ "], Duration: "
					+ ((EvAdSmartBean) inputEvent).getDuration());
		}
	}

	protected void onServiceKeyEnd() {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setServiceKey(sBuffer.toString());
		} else if (inputEvent instanceof EvAdSmartNoSubsBean) {
			((EvAdSmartNoSubsBean) inputEvent).setServiceKey(sBuffer.toString());
		} else if (inputEvent instanceof EvChangeViewBean) {
			if (source.equals(ConfigKeys.ETHAN)) {
				if (sBuffer.toString() != null &&  !sBuffer.toString().equals("0") && !sBuffer.toString().equals("-1")) {
					((EvChangeViewBean) inputEvent).setServiceKey(sBuffer.toString());
				} else {
					LOG.info(source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Received Invalid ServiceKey (null, 0 or -1) inside ChangeView event" :
															 "DeviceID: " + deviceID + " | Received Invalid ServiceKey (null, 0 or -1) inside ChangeView event");
				}
			} else {
				((EvChangeViewBean) inputEvent).setServiceKey(sBuffer.toString());
			}
		} else if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setServiceKey(sBuffer.toString());
		}
	}

	protected void onSIServiceIDEnd() {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setSiServiceID(sBuffer.toString());
		} else if (inputEvent instanceof EvAdSmartNoSubsBean) {
			((EvAdSmartNoSubsBean) inputEvent).setSiServiceID(sBuffer.toString());
		} else if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent).setSiServiceID(sBuffer.toString());
		} else if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setSiServiceID(sBuffer.toString());
		}
	}

	protected void onTransportStreamIDEnd() {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setTransportStreamID(sBuffer.toString());
		} else if (inputEvent instanceof EvAdSmartNoSubsBean) {
			((EvAdSmartNoSubsBean) inputEvent).setTransportStreamID(sBuffer.toString());
		} else if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent).setTransportStreamID(sBuffer.toString());
		} else if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setTransportStreamID(sBuffer.toString());
		}
	}

	protected void onOriginalNetworkIDEnd() {
		if (inputEvent instanceof EvAdSmartBean) {
			((EvAdSmartBean) inputEvent).setOriginalNetworkID(sBuffer.toString());
		} else if (inputEvent instanceof EvAdSmartNoSubsBean) {
			((EvAdSmartNoSubsBean) inputEvent).setOriginalNetworkID(sBuffer.toString());
		} else if (inputEvent instanceof EvChangeViewBean) {
			((EvChangeViewBean) inputEvent).setOriginalNetworkID(sBuffer.toString());
		} else if (inputEvent instanceof EvTunerActivityBean) {
			((EvTunerActivityBean) inputEvent).setOriginalNetworkID(sBuffer.toString());
		}
	}

	protected void onEvVODPlaybackEnd() {
		currentFSM.doAction(inputEvent);
	}

	protected void onEvStandbyInEnd() {
		currentFSM.doAction(inputEvent);
	}
	
	protected void onEvStandbyOutEnd() {
		currentFSM.doAction(inputEvent);
	}
	
	protected void onEvSurfEnd() {
		currentFSM.doAction(inputEvent);
	}

	protected void onSubscriberEnd() throws SAXException {
		
		((STBReportBean) stbEvent)
				.setTs(TimeUtils.long2String(errorLogger));
		((STBReportBean) stbEvent).setProcessingTime(
				System.currentTimeMillis() - startProcessing);

		List<STBPayloadBean> stbPayloadIn = new ArrayList<>();
		List<STBPayloadBean> stbPayloadOut = new ArrayList<>();

		Collections.sort(changeViewList, new LongComparator());
		Collections.sort(adSmartList, new LongComparator());
		Collections.sort(noSubsList, new LongComparator());
		Collections.sort(vodPlaybackList, new LongComparator());
		Collections.sort(standbyInList, new LongComparator());
		Collections.sort(standbyOutList, new LongComparator());
		Collections.sort(tunerActivityList, new LongComparator());
		Collections.sort(surfList, new LongComparator());

		if (changeViewList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					EvChangeViewBean.EVENT_NAME,
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(changeViewList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(changeViewList.get(changeViewList.size() - 1), errorLogger), "CET", errorLogger),
					changeViewList.size()));
		}
		if (adSmartList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					EvAdSmartBean.EVENT_NAME,
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(adSmartList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(adSmartList.get(adSmartList.size() - 1), errorLogger), "CET", errorLogger),
					adSmartList.size()));

		}
		if (noSubsList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					EvAdSmartNoSubsBean.INPUT_EVENT_NAME,
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(noSubsList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(noSubsList.get(noSubsList.size() - 1), errorLogger), "CET", errorLogger),
					noSubsList.size()));
		}
		if (vodPlaybackList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					EvVODPlaybackBean.EVENT_NAME,
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(vodPlaybackList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(vodPlaybackList.get(vodPlaybackList.size() - 1), errorLogger), "CET", errorLogger),
					vodPlaybackList.size()));
		}
		if (standbyInList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					"evStandbyIn",
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(standbyInList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(standbyInList.get(standbyInList.size() - 1), errorLogger), "CET", errorLogger),
					standbyInList.size()));
		}
		if (standbyOutList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					"evStandbyOut",
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(standbyOutList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(standbyOutList.get(standbyOutList.size() - 1), errorLogger), "CET", errorLogger),
					standbyOutList.size()));
		}
		if (tunerActivityList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					EvTunerActivityBean.INPUT_EVENT_NAME,
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(tunerActivityList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(tunerActivityList.get(tunerActivityList.size() - 1), errorLogger), "CET", errorLogger),
					tunerActivityList.size()));
		}
		
		if (surfList.size() > 0) {
			stbPayloadIn.add(new STBPayloadBean(
					"evSurf",
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(surfList.get(0), errorLogger), "CET", errorLogger),
					TimeUtils.UTC2LocalTime(TimeUtils.long2String(surfList.get(surfList.size() - 1), errorLogger), "CET", errorLogger),
					surfList.size()));
		}

		if (currentFSM.getViewChCounter() > 0) {
			stbPayloadOut.add(new STBPayloadBean(
					ViewChEventBean.EVENT_NAME,
					currentFSM.getViewChCounter()));
		}
		if (currentFSM.getSubsCounter() > 0) {
			stbPayloadOut.add(new STBPayloadBean(
					SubsEventBean.EVENT_NAME,
					currentFSM.getSubsCounter()));
		}
		if (noSubsOutCounter > 0) {
			stbPayloadOut.add(new STBPayloadBean(
					EvAdSmartNoSubsBean.OUTPUT_EVENT_NAME,
					noSubsOutCounter));
		}
		if (currentFSM.getViewVODCounter() > 0) {
			stbPayloadOut
			.add(new STBPayloadBean(
					ViewVODEventBean.EVENT_NAME,
					currentFSM.getViewVODCounter()));
		}
		if (tunerActivityCounter > 0) {
			stbPayloadOut.add(new STBPayloadBean(
					EvTunerActivityBean.OUTPUT_EVENT_NAME, 
					tunerActivityCounter));
		}
		if (currentFSM instanceof FiniteStateMachineEthan) {
			if (((FiniteStateMachineEthan) currentFSM).getStandByCounter() > 0) {
				stbPayloadOut.add(new STBPayloadBean("STANDBY", ((FiniteStateMachineEthan) currentFSM).getStandByCounter()));
			}
			if (((FiniteStateMachineEthan) currentFSM).getSurfCounter() > 0) {
				stbPayloadOut.add(new STBPayloadBean("SURF", ((FiniteStateMachineEthan) currentFSM).getSurfCounter()));
			}
		}

		((STBReportBean) stbEvent).setPayloadIn(stbPayloadIn);
		((STBReportBean) stbEvent).setPayloadOut(stbPayloadOut);

		OutputMessage message = new EventPrinterHelper<STBReportBean>((STBReportBean) stbEvent)
				.createOutputMessage(STBReportBean.EVENT_NAME);
		eventsToWrite.add(message);

		changeViewList.clear();
		adSmartList.clear();
		noSubsList.clear();
		vodPlaybackList.clear();
		standbyInList.clear();
		standbyOutList.clear();
		tunerActivityList.clear();
		currentFSM.setViewChCounter(0);
		currentFSM.setSubsCounter(0);
		noSubsOutCounter = 0;
		tunerActivityCounter = 0;
		currentFSM.setViewVODCounter(0);
		if (currentFSM instanceof FiniteStateMachineEthan) {
			((FiniteStateMachineEthan) currentFSM).setStandByCounter(0);
			((FiniteStateMachineEthan) currentFSM).setSurfCounter(0);
		}

		// Saving last event structure with needed informations to
		// CouchbaseDB
		// (if statusRecovery parameter is setted to "ON")
		JsonObject jsonToWrite = null;
		if (config.getString(ConfigKeys.RUNTIME.STATUS_RECOVERY).equalsIgnoreCase(ConfigKeys.YES)) {
			if (currentFSM.getCurrentState().equals(State.S6STANDBYOUT)) {
				jsonToWrite = JsonObject.create();
				jsonToWrite.put("state", currentFSM.getCurrentState().toString());
			} else if (currentFSM.getCurrentState().equals(State.S5STANDBY)) {
				if (source.equals(ConfigKeys.ADSMART)) {
					jsonToWrite = JsonObject.create();
					jsonToWrite.put("state", currentFSM.getCurrentState().toString());
				} else {
					StandByEventBean standbyToWrite = (StandByEventBean)currentFSM.getCurrentEvent();
					OutputMessage om = new EventPrinterHelper<StandByEventBean>(standbyToWrite).createOutputMessage(StandByEventBean.EVENT_NAME);
					JSONObject eventToWriteObj = om.getJson();
					LOG.debug(source, "eventToWrite: " + eventToWriteObj);
					jsonToWrite = JsonObject.fromJson(standbyToWrite.addMissingParameters(eventToWriteObj, standbyToWrite.getPayload(), currentFSM.getCurrentState()).toString());
				}
			} else {
				if (currentFSM.getCurrentEvent() instanceof ViewChEventBean) {
					ViewChEventBean viewChToWrite = (ViewChEventBean) (currentFSM.getCurrentEvent());
					LOG.debug(source, "viewChToWrite: " + viewChToWrite.getRefDate());	
					OutputMessage om = new EventPrinterHelper<ViewChEventBean>(viewChToWrite).createOutputMessage(ViewChEventBean.EVENT_NAME);
					JSONObject eventToWriteObj = om.getJson();
					LOG.debug(source, "eventToWrite: " + eventToWriteObj);
					jsonToWrite = JsonObject.fromJson(viewChToWrite.addMissingParameters(eventToWriteObj, viewChToWrite.getPayload(), currentFSM.getCurrentState()).toString());
				} else if (currentFSM.getCurrentEvent() instanceof SubsEventBean) {
					SubsEventBean subsToWrite = ((SubsEventBean) currentFSM.getCurrentEvent());
					LOG.debug(source, "subsToWrite: " + subsToWrite.getRefDate());
					OutputMessage om = new EventPrinterHelper<SubsEventBean>(subsToWrite).createOutputMessage(SubsEventBean.EVENT_NAME);
					JSONObject eventToWriteObj = om.getJson();
					jsonToWrite = JsonObject.fromJson(subsToWrite.addMissingParameters(eventToWriteObj, subsToWrite.getPayload(), currentFSM.getCurrentState()).toString());
				} else if (currentFSM.getCurrentEvent() instanceof ViewVODEventBean) {
					ViewVODEventBean vodToWrite = ((ViewVODEventBean) currentFSM.getCurrentEvent());
					LOG.debug(source, "vodToWrite: " + vodToWrite.getRefDate());
					OutputMessage om = new EventPrinterHelper<ViewVODEventBean>(vodToWrite).createOutputMessage(ViewVODEventBean.EVENT_NAME);
					JSONObject eventToWriteObj = om.getJson();
					jsonToWrite = JsonObject.fromJson(vodToWrite.addMissingParameters(eventToWriteObj, vodToWrite.getPayload(), currentFSM.getCurrentState()).toString());
				} else if (currentFSM.getCurrentEvent() instanceof SurfBean) {
					SurfBean surfToWrite = (SurfBean)currentFSM.getCurrentEvent();
					OutputMessage om = new EventPrinterHelper<SurfBean>(surfToWrite).createOutputMessage(SurfBean.EVENT_NAME);
					JSONObject eventToWriteObj = om.getJson();
					LOG.debug(source, "eventToWrite: " + eventToWriteObj);
					jsonToWrite = JsonObject.fromJson(surfToWrite.addMissingParameters(eventToWriteObj, surfToWrite.getPayload(), currentFSM.getCurrentState()).toString());
				}
			}

			String couchbaseID = "";
			if (source.equals(ConfigKeys.ADSMART)) {
				couchbaseID = subscriberID + "-" + cardID;
			} else {
				couchbaseID = subscriberID + "-" + cardID + "-" + deviceID;
			}
			
			JsonDocument jsonDocument = JsonDocument.create(couchbaseID, jsonToWrite);
			try {
                DButils.writeState(jsonDocument, source, config, errorLogger);
            } catch (Exception e) {
                throw new SAXException(e);
            }
		}
		
		for (int i = 0; i < eventsToWrite.size(); i++) {
			fbw.writeByteArray(eventsToWrite.get(i).toByteArray());
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String temp = new String(ch, start, length);
		sBuffer.append(temp);
	}

	public String getSubscriberID() {
		return subscriberID;
	}

	public String getCardId() {
		return cardID;
	}
	
	public String getDeviceID() {
		return deviceID;
	}

	public String getPanelID() {
		return panelID;
	}

	public String getDateXML() {
		return dateXML;
	}

	public String getDateLog() {
		return dateLog;
	}

	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * Initializes the event with attributes taken from SkyAdSmartPanel and
	 * Subscriber tags. Before that, the method checks if the last arrived event
	 * has an anticipated event time (before the last saved event time).
	 * 
	 * @param event
	 *            the last arrived input event
	 * @param attributes
	 *            attributes of the tag from where the method is called
	 *        lastEventTime
	 *    	  	  eventTime of the last arrived event
	 */
	protected void initEvent(EventBean event, Attributes attributes) {

		
		//added check for input event of kind STBRep
		if (attributes.getValue("EventTime") != null) {
			event.setRefDate(TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger));
		}
		event.setCardID(cardID);
		event.setDeviceID(deviceID);
		event.setSubscriberID(subscriberID);
		event.setDeviceType(deviceType);
		event.setPanelID(panelID);
		event.setLogDate(dateLog);
		event.setXmlDate(dateXML);
		event.setReportID(computeMD5Hash());
		if (event instanceof STBReportBean) {
			((STBReportBean) event).setReceivedDate(TimeUtils.long2String(receivedDate, errorLogger));
			event.setRefDate(TimeUtils.long2String(errorLogger));
		}
	}

	private void checkLastEventTime(Attributes attributes) {
		if (lastEventTime != 0L) {
			LOG.debug(source, "New EventTime: ["
					+ TimeUtils.UTCstring2long(
							attributes.getValue("EventTime"), errorLogger)
					+ "]" + ", Last EventTime: [" + lastEventTime + "]");

			if ((TimeUtils.UTCstring2long(
					TimeUtils.UTC2LocalTime(
							attributes.getValue("EventTime"), "CET",
							errorLogger),
					errorLogger) < lastEventTime)) {
				LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Anticipated event. Last event time: " + TimeUtils.long2String(lastEventTime, errorLogger) + ", New event time: " + TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger) + ". Reinitializing finite state machine..." : 
																 "DeviceID: " + deviceID + " | Anticipated event. Last event time: " + TimeUtils.long2String(lastEventTime, errorLogger) + ", New event time: " + TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger) + ". Reinitializing finite state machine...");
				errorLogger.write(source, ErrorCode.ANTICIPATED_EVENT, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Last event time: " + TimeUtils.long2String(lastEventTime, errorLogger) + ", New event time: " + TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger) + ". Finite State Machine will be reinitialized." :
																									   "DeviceID: " + deviceID + " | Last event time: " + TimeUtils.long2String(lastEventTime, errorLogger) + ", New event time: " + TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger) + ". Finite State Machine will be reinitialized.");
				FiniteStateMachine lastFSM = currentFSM;
				currentFSM = getFiniteStateMachine();
				currentFSM.setViewChCounter(lastFSM.getViewChCounter());
				currentFSM.setViewVODCounter(lastFSM.getViewVODCounter());
				currentFSM.setSubsCounter(lastFSM.getSubsCounter());
			}
		}

		lastEventTime = TimeUtils.UTCstring2long(TimeUtils.UTC2LocalTime(attributes.getValue("EventTime"), "CET", errorLogger), errorLogger);
	}

	private HashMap<String, String> getSpeeds() {
		HashMap<String, String> speedsMap = new HashMap<>();
		String filenameKey = this.source.equalsIgnoreCase(ConfigKeys.ADSMART) ?
				
				(config.getString(ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_DIR)
				+ config.getString(ConfigKeys.FIXED.ADSMART_CFG.TBL_SPEED_FILE_NAME)) :
					
				(config.getString(ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_DIR)
				+ config.getString(ConfigKeys.FIXED.ETHAN_CFG.TBL_SPEED_FILE_NAME));
								
		JSONObject speedJson = JsonUtils.getJson(filenameKey);
		JSONArray speedMappingArray = speedJson.getJSONArray("speed_mapping");
		for (int i = 0; i < speedMappingArray.length(); i++) {
			JSONObject singleLine = speedMappingArray.getJSONObject(i);
			speedsMap.put(singleLine.getString("reported"),
					singleLine.getString("touse"));

		}
		return speedsMap;
	}

	/**
	 * Retrieves speed from the proper file containing the speed map
	 * 
	 * @param speedReceived
	 *            the event speed arrived from XML document
	 * @return the speed retrieved from the speed map
	 */
	private int retrieveSpeed(String speedReceived) {

		int result = 0;
		boolean found = false;
		for (Map.Entry<String, String> singleLine : getSpeeds().entrySet()) {

			if (speedReceived.equals(singleLine.getKey())) {
				result = Integer.parseInt(singleLine.getValue());
				found = true;
			}
		}

		if (!found) {
			result = Integer.parseInt(getSpeeds().get("other"));
			LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Speed: [" + speedReceived + "] unknown" :
															 "DeviceID: " + deviceID + " | Speed: [" + speedReceived + "] unknown");
			if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART))
				statisticRunnable.incrementInt(M2Statistics.UNKNOWN_SPEED_ADSMART);
			else 
				statisticRunnable.incrementInt(M2Statistics.UNKNOWN_SPEED_ETHAN);
			
			errorLogger.write(source, ErrorCode.UNKNOWN_SPEED, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Event with EventTime: [" + inputEvent.getRefDate() + " with speed not known: [" + speedReceived + "]" :
																							   "DeviceID: " + deviceID + " | Event with EventTime: [" + inputEvent.getRefDate() + " with speed not known: [" + speedReceived + "]");
		}
		return result;
	}

	private ChannelBean retrieveChannelBySK(String eventSK, long eventDate) {
		boolean foundValid = false;
		Iterator<ChannelBean> chIterator = DButils.getChannelFromSkey(eventSK, source).iterator();
		List<ChannelBean> possibleChannels = new ArrayList<>();
		ChannelBean chBeanToReturn = new ChannelBean();
		while (chIterator.hasNext() && foundValid == false) {
			ChannelBean channelBean = (ChannelBean) chIterator.next();
			if (eventDate >= channelBean.getFromDate()
					&& eventDate < channelBean.getToDate()) {
				chBeanToReturn = channelBean;
				foundValid = true;
			} else {
				if ((channelBean.getToDate() >= (eventDate
						- config.getInteger(ConfigKeys.RUNTIME.TBL_CHANNEL_READ_SINCE_DAY)))
						&& (channelBean.getToDate() < eventDate)) {
					possibleChannels.add(channelBean);
				}
			}
		}

		if (!foundValid)

		{

			if (possibleChannels.size() != 0) {
				errorLogger.write(source, ErrorCode.VALID_RANGE_NOT_FOUND, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + "SK: [\"" + eventSK + "\"] will be matched with the most recent DVB. [module:Parser, event with EventTime: " + inputEvent.getRefDate() + "]" : 
																										   "DeviceID: " + deviceID + "SK: [\"" + eventSK + "\"] will be matched with the most recent DVB. [module:Parser, event with EventTime: " + inputEvent.getRefDate() + "]");
				Collections.sort(possibleChannels, new ChannelDateComparator());
				chBeanToReturn = possibleChannels.get(0);
			} else {
				chBeanToReturn = new ChannelBean("NA", "NA", "NA", "NA");
				LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Unknown Channel: " + inputEvent.getRefDate() :
																 "DeviceID: " + deviceID + " | Unknown Channel: " + inputEvent.getRefDate());
				errorLogger.write(source, ErrorCode.CHANNEL_NOT_FOUND, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | ServiceKey: [\"" + eventSK + "\"] cannot be matched." : 
																									   "DeviceID: " + deviceID + " | ServiceKey: [\"" + eventSK + "\"] cannot be matched.");
				if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART))
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CHANNEL_ADSMART);
				else
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CHANNEL_ETHAN);
			}

		}

		return chBeanToReturn;
	}

	private ChannelBean retrieveChannelByDVB(String eventONID, String eventTSID, String eventSSID, long eventDate) {
		boolean foundValid = false;
		Iterator<ChannelBean> chIterator = DButils.getChannelFromIDs(eventONID, eventTSID, eventSSID, source).iterator();
		List<ChannelBean> possibleChannels = new ArrayList<>();
		ChannelBean chBeanToReturn = new ChannelBean();
		while (chIterator.hasNext() && foundValid == false) {
			ChannelBean channelBean = (ChannelBean) chIterator.next();
			LOG.debug(source, "DBUtils.getChannelFromIDs: [" + channelBean.toString() + "]");
			if (eventDate >= channelBean.getFromDate()
					&& eventDate < channelBean.getToDate()) {
				foundValid = true;
				chBeanToReturn = channelBean;
			} else {
				if ((channelBean.getToDate() >= eventDate
						- config.getInteger(ConfigKeys.RUNTIME.TBL_CHANNEL_READ_SINCE_DAY))
						&& (channelBean.getToDate() < eventDate)) {
					possibleChannels.add(channelBean);
				}
			}
		}

		if (!foundValid) {
			if (possibleChannels.size() != 0) {
				errorLogger.write(source, ErrorCode.VALID_RANGE_NOT_FOUND, "DVB: [\"" + eventONID + "\",\"" + eventSSID + "\",\"" + eventTSID
						+ "\"] will be matched with the most recent SK. [module:Parser, event with EventTime: " + inputEvent.getRefDate() + "]");
				Collections.sort(possibleChannels, new ChannelDateComparator());
				chBeanToReturn = possibleChannels.get(0);
			} else {
				LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Unknown Channel: " + inputEvent.getRefDate() : 
																 "DeviceID: " + deviceID + " | Unknown Channel: " + inputEvent.getRefDate());
				errorLogger.write(source, ErrorCode.CHANNEL_NOT_FOUND, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | DVBTriplet: [\"" + eventONID + "\",\"" + eventTSID + "\",\"" + eventSSID + "\"] cannot be matched." : 
																									   "DeviceID: " + deviceID + " | DVBTriplet: [\"" + eventONID + "\",\"" + eventTSID + "\",\"" + eventSSID + "\"] cannot be matched.");
				if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART))
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CHANNEL_ADSMART);
				else
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CHANNEL_ETHAN);
				
				chBeanToReturn = new ChannelBean("NA", "NA");
			}
		}

		return chBeanToReturn;
	}

	private void channelDataRetrieve(IChannelable event) {
		ChannelBean tempChannelBean;
		
		boolean hasTDVB = !(event.getOriginalNetworkID().equals("NA"));
		hasTDVB = hasTDVB && !(event.getTransportStreamID().equals("NA"));
		hasTDVB = hasTDVB && !(event.getSiServiceID().equals("NA"));
		boolean hasSK = !(event.getServiceKey().equals("NA"));
		
		LOG.debug("HASSK: " + hasSK + ", HASTDVB: " + hasTDVB);
		LOG.debug("SK: " + event.getServiceKey() + ", ONID: " + event.getOriginalNetworkID() + ", TSID: " + event.getTransportStreamID() + ", SSID: " + event.getSiServiceID());
		
		event.setChType("SAT");
		if (!hasSK) {
			tempChannelBean = retrieveChannelByDVB(
					event.getOriginalNetworkID(),
					event.getTransportStreamID(),
					event.getSiServiceID(),
					TimeUtils.UTCstring2long(((EventBean)event).getRefDate(),
							errorLogger));

			event.setServiceKey(tempChannelBean.getServiceKey());
			event.setChType(tempChannelBean.getType());
		} else if (!hasTDVB){
			tempChannelBean = retrieveChannelBySK(
					event.getServiceKey(),
					TimeUtils.UTCstring2long(((EventBean)event).getRefDate(),
							errorLogger));
			event.setOriginalNetworkID(
					tempChannelBean.getOriginalNetworkID());
			event.setTransportStreamID(
					tempChannelBean.getTransportStreamID());
			event.setSiServiceID(tempChannelBean.getSiServiceID());
			event.setChType(tempChannelBean.getType());
		}

		if (event instanceof EvAdSmartBean) {
			EvAdSmartBean bean = (EvAdSmartBean)event;
				
			// Campaign not existent
			if (!DButils.isCampaignExistent(bean.getCampaignID(), source)) {
				errorLogger.write(source, ErrorCode.UNKNOWN_CAMPAIGN, "Campaign ID: [" + bean.getCampaignID() + "] not found.");
				LOG.error(source, "UNKNOWN CAMPAIGN: campaign ID: [" + bean.getCampaignID() + "] not found.");
					
				if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CAMPAIGN_ADSMART);
					statisticRunnable.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
				} else {
					statisticRunnable.incrementInt(M2Statistics.UNKNOWN_CAMPAIGN_ETHAN);
					statisticRunnable.incrementInt(M2Statistics.EVENTS_DISCARDED_ETHAN);	
				}
			} else {
				if (!hasSK && !hasTDVB) { 
					LOG.warn(source, source.equals(Source.ADSMART) ? "SmartcardID: " + cardID + " | Arrived evAdSmart without service key and dvb triplet: it will be discarded." :
																	 "DeviceID: " + deviceID + " | Arrived evAdSmart without service key and dvb triplet: it will be discarded.");
					if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART)) {
						statisticRunnable.incrementInt(M2Statistics.EVENTS_DISCARDED_ADSMART);
					} else {
						statisticRunnable.incrementInt(M2Statistics.EVENTS_DISCARDED_ETHAN);
					}
				} else {
					if (bean.getChType().equals("NA")) {
						bean.setChType("SAT");
					}
					currentFSM.doAction(bean);
				}
			}
		}
		
		else if (event instanceof EvAdSmartNoSubsBean) {
			
			// Event of type evAdSmartNoSubstitution has not to be sent to the fsm,
			// so it is printed on the output of parser directly here
			noSubsOutCounter++;
			if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART))
				statisticRunnable.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
			else
				statisticRunnable.incrementInt(M2Statistics.EVENTS_PROCESSED_ETHAN);
			OutputMessage message = new EventPrinterHelper<EvAdSmartNoSubsBean>((EvAdSmartNoSubsBean) inputEvent)
					.createOutputMessage(EvAdSmartNoSubsBean.OUTPUT_EVENT_NAME);
			eventsToWrite.add(message);
			
			LOG.debug(source, "Printed new EvAdSmartNoSubsBean");
		}
		
		else if (event instanceof EvTunerActivityBean) {
			// Event of type evAdSmartNoSubstitution has not to be sent to the fsm,
			// so it is printed on the output of parser directly here
			tunerActivityCounter++;
			if (this.source.equalsIgnoreCase(ConfigKeys.ADSMART))
				statisticRunnable.incrementInt(M2Statistics.EVENTS_PROCESSED_ADSMART);
			else
				statisticRunnable.incrementInt(M2Statistics.EVENTS_PROCESSED_ETHAN);
			
			if (hasSK && hasTDVB) {
				event.setChType(retrieveChannelBySK(event.getServiceKey(), TimeUtils.UTCstring2long(((EvTunerActivityBean) event).getRefDate(), errorLogger)).getType());
			}
			
			OutputMessage message = new EventPrinterHelper<EvTunerActivityBean>((EvTunerActivityBean) inputEvent)
					.createOutputMessage(EvTunerActivityBean.OUTPUT_EVENT_NAME);
			eventsToWrite.add(message);
			
			LOG.debug(source, "Printed new TUNER event");
		}
		
		else if (event instanceof EvChangeViewBean) {
			if (hasSK && hasTDVB) {
				event.setChType(retrieveChannelBySK(event.getServiceKey(), TimeUtils.UTCstring2long(((EvChangeViewBean) event).getRefDate(), errorLogger)).getType());
			}
			currentFSM.doAction((EventBean)event);
		}
	}
	
	private String computeMD5Hash() {
		String s = subscriberID + "-" + dateXML + "-" + dateLog;
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Handle exception
			e.printStackTrace();
		}
		m.update(s.getBytes(), 0, s.length());
		BigInteger hashValue = new BigInteger(1, m.digest());
		return hashValue.toString(16);
	}
}
