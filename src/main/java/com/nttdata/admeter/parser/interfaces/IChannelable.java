package com.nttdata.admeter.parser.interfaces;

public interface IChannelable {

	String getOriginalNetworkID();

	void setOriginalNetworkID(String originalNetworkID);

	String getTransportStreamID();

	void setTransportStreamID(String transportStreamID);

	String getSiServiceID();

	void setSiServiceID(String siServiceID);

	String getServiceKey();

	void setServiceKey(String serviceKey);

	String getChType();
	
	void setChType(String chType);
	
}
