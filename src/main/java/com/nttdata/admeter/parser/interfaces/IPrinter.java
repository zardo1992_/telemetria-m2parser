package com.nttdata.admeter.parser.interfaces;

import org.json.JSONObject;

public interface IPrinter {

	JSONObject toJSONObject();
	
}
